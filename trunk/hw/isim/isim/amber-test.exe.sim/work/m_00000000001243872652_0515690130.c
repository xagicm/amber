/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/ethmac/eth_register.v";
static unsigned int ng1[] = {100U, 0U};



static void Always_94_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;

LAB0:    t1 = (t0 + 1868U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(94, ng0);
    t2 = (t0 + 2048);
    *((int *)t2) = 1;
    t3 = (t0 + 1892);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(95, ng0);

LAB5:    xsi_set_current_line(96, ng0);
    t4 = (t0 + 1036U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:    xsi_set_current_line(99, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t3);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB9;

LAB10:    xsi_set_current_line(102, ng0);
    t2 = (t0 + 852U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t3);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB12;

LAB13:
LAB14:
LAB11:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(97, ng0);
    t11 = ((char*)((ng1)));
    t12 = (t0 + 1356);
    xsi_vlogvar_wait_assign_value(t12, t11, 0, 0, 8, 1000LL);
    goto LAB8;

LAB9:    xsi_set_current_line(100, ng0);
    t4 = ((char*)((ng1)));
    t5 = (t0 + 1356);
    xsi_vlogvar_wait_assign_value(t5, t4, 0, 0, 8, 1000LL);
    goto LAB11;

LAB12:    xsi_set_current_line(103, ng0);
    t4 = (t0 + 760U);
    t5 = *((char **)t4);
    t4 = (t0 + 1356);
    xsi_vlogvar_wait_assign_value(t4, t5, 0, 0, 8, 1000LL);
    goto LAB14;

}


extern void work_m_00000000001243872652_0515690130_init()
{
	static char *pe[] = {(void *)Always_94_0};
	xsi_register_didat("work_m_00000000001243872652_0515690130", "isim/amber-test.exe.sim/work/m_00000000001243872652_0515690130.didat");
	xsi_register_executes(pe);
}
