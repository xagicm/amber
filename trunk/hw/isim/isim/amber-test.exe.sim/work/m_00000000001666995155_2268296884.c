/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/lib/generic_sram_byte_en.v";
static unsigned int ng1[] = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U};
static int ng2[] = {0, 0};
static int ng3[] = {8, 0};
static int ng4[] = {1, 0};
static int ng5[] = {2, 0};
static int ng6[] = {3, 0};
static int ng7[] = {4, 0};
static int ng8[] = {5, 0};
static int ng9[] = {6, 0};
static int ng10[] = {7, 0};



static void Always_61_0(char *t0)
{
    char t4[32];
    char t5[8];
    char t26[32];
    char t35[8];
    char t36[8];
    char t37[8];
    char t38[8];
    char t40[8];
    char t47[8];
    char t49[8];
    char t65[8];
    char t72[8];
    char t74[8];
    char t76[8];
    char t77[8];
    char t86[8];
    char t93[8];
    char t95[8];
    char *t1;
    char *t2;
    char *t3;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    unsigned int t39;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t48;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    char *t60;
    char *t61;
    char *t62;
    char *t63;
    char *t64;
    char *t66;
    char *t67;
    char *t68;
    char *t69;
    char *t70;
    char *t71;
    char *t73;
    char *t75;
    char *t78;
    char *t79;
    char *t80;
    char *t81;
    char *t82;
    char *t83;
    char *t84;
    char *t85;
    char *t87;
    char *t88;
    char *t89;
    char *t90;
    char *t91;
    char *t92;
    char *t94;
    char *t96;
    unsigned int t97;
    int t98;
    char *t99;
    unsigned int t100;
    int t101;
    int t102;
    char *t103;
    unsigned int t104;
    int t105;
    int t106;
    unsigned int t107;
    unsigned int t108;
    int t109;

LAB0:    t1 = (t0 + 2052U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 2232);
    *((int *)t2) = 1;
    t3 = (t0 + 2076);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(62, ng0);

LAB5:    xsi_set_current_line(64, ng0);
    t6 = (t0 + 944U);
    t7 = *((char **)t6);
    memset(t5, 0, 8);
    t6 = (t7 + 4);
    t8 = *((unsigned int *)t6);
    t9 = (~(t8));
    t10 = *((unsigned int *)t7);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB6;

LAB7:    if (*((unsigned int *)t6) != 0)
        goto LAB8;

LAB9:    t14 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t14);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB10;

LAB11:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t14);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t14) > 0)
        goto LAB14;

LAB15:    if (*((unsigned int *)t5) > 0)
        goto LAB16;

LAB17:    memcpy(t4, t26, 32);

LAB18:    t33 = (t0 + 1356);
    xsi_vlogvar_wait_assign_value(t33, t4, 0, 0, 128, 0LL);
    xsi_set_current_line(67, ng0);
    t2 = (t0 + 944U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t8 = *((unsigned int *)t2);
    t9 = (~(t8));
    t10 = *((unsigned int *)t3);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB19;

LAB20:
LAB21:    goto LAB2;

LAB6:    *((unsigned int *)t5) = 1;
    goto LAB9;

LAB8:    t13 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t13) = 1;
    goto LAB9;

LAB10:    t18 = ((char*)((ng1)));
    goto LAB11;

LAB12:    t23 = (t0 + 1448);
    t24 = (t23 + 36U);
    t25 = *((char **)t24);
    t27 = (t0 + 1448);
    t28 = (t27 + 44U);
    t29 = *((char **)t28);
    t30 = (t0 + 1448);
    t31 = (t30 + 40U);
    t32 = *((char **)t31);
    t33 = (t0 + 1036U);
    t34 = *((char **)t33);
    xsi_vlog_generic_get_array_select_value(t26, 128, t25, t29, t32, 2, 1, t34, 8, 2);
    goto LAB13;

LAB14:    xsi_vlog_unsigned_bit_combine(t4, 128, t18, 128, t26, 128);
    goto LAB18;

LAB16:    memcpy(t4, t18, 32);
    goto LAB18;

LAB19:    xsi_set_current_line(68, ng0);
    xsi_set_current_line(68, ng0);
    t6 = ((char*)((ng2)));
    t7 = (t0 + 1540);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 32);

LAB22:    t2 = (t0 + 1540);
    t3 = (t2 + 36U);
    t6 = *((char **)t3);
    t7 = (t0 + 264);
    t13 = *((char **)t7);
    t7 = ((char*)((ng3)));
    memset(t5, 0, 8);
    xsi_vlog_signed_divide(t5, 32, t13, 32, t7, 32);
    memset(t35, 0, 8);
    xsi_vlog_signed_less(t35, 32, t6, 32, t5, 32);
    t14 = (t35 + 4);
    t8 = *((unsigned int *)t14);
    t9 = (~(t8));
    t10 = *((unsigned int *)t35);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB23;

LAB24:    goto LAB21;

LAB23:    xsi_set_current_line(69, ng0);

LAB25:    xsi_set_current_line(70, ng0);
    t18 = (t0 + 1128U);
    t23 = *((char **)t18);
    t18 = (t0 + 1104U);
    t24 = (t18 + 44U);
    t25 = *((char **)t24);
    t27 = (t0 + 1540);
    t28 = (t27 + 36U);
    t29 = *((char **)t28);
    xsi_vlog_generic_get_index_select_value(t38, 1, t23, t25, 2, t29, 32, 1);
    memset(t37, 0, 8);
    t30 = (t38 + 4);
    t15 = *((unsigned int *)t30);
    t16 = (~(t15));
    t17 = *((unsigned int *)t38);
    t19 = (t17 & t16);
    t20 = (t19 & 1U);
    if (t20 != 0)
        goto LAB26;

LAB27:    if (*((unsigned int *)t30) != 0)
        goto LAB28;

LAB29:    t32 = (t37 + 4);
    t21 = *((unsigned int *)t37);
    t22 = *((unsigned int *)t32);
    t39 = (t21 || t22);
    if (t39 > 0)
        goto LAB30;

LAB31:    t50 = *((unsigned int *)t37);
    t51 = (~(t50));
    t52 = *((unsigned int *)t32);
    t53 = (t51 || t52);
    if (t53 > 0)
        goto LAB32;

LAB33:    if (*((unsigned int *)t32) > 0)
        goto LAB34;

LAB35:    if (*((unsigned int *)t37) > 0)
        goto LAB36;

LAB37:    memcpy(t36, t65, 8);

LAB38:    t75 = (t0 + 1448);
    t78 = (t0 + 1448);
    t79 = (t78 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1448);
    t82 = (t81 + 40U);
    t83 = *((char **)t82);
    t84 = (t0 + 1036U);
    t85 = *((char **)t84);
    xsi_vlog_generic_convert_array_indices(t76, t77, t80, t83, 2, 1, t85, 8, 2);
    t84 = (t0 + 1448);
    t87 = (t84 + 44U);
    t88 = *((char **)t87);
    t89 = (t0 + 1540);
    t90 = (t89 + 36U);
    t91 = *((char **)t90);
    t92 = ((char*)((ng3)));
    memset(t93, 0, 8);
    xsi_vlog_signed_multiply(t93, 32, t91, 32, t92, 32);
    t94 = ((char*)((ng2)));
    memset(t95, 0, 8);
    xsi_vlog_signed_add(t95, 32, t93, 32, t94, 32);
    xsi_vlog_generic_convert_bit_index(t86, t88, 2, t95, 32, 1);
    t96 = (t76 + 4);
    t97 = *((unsigned int *)t96);
    t98 = (!(t97));
    t99 = (t77 + 4);
    t100 = *((unsigned int *)t99);
    t101 = (!(t100));
    t102 = (t98 && t101);
    t103 = (t86 + 4);
    t104 = *((unsigned int *)t103);
    t105 = (!(t104));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB39;

LAB40:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB41;

LAB42:    if (*((unsigned int *)t23) != 0)
        goto LAB43;

LAB44:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB45;

LAB46:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB47;

LAB48:    if (*((unsigned int *)t25) > 0)
        goto LAB49;

LAB50:    if (*((unsigned int *)t35) > 0)
        goto LAB51;

LAB52:    memcpy(t5, t47, 8);

LAB53:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng4)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB54;

LAB55:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB56;

LAB57:    if (*((unsigned int *)t23) != 0)
        goto LAB58;

LAB59:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB60;

LAB61:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB62;

LAB63:    if (*((unsigned int *)t25) > 0)
        goto LAB64;

LAB65:    if (*((unsigned int *)t35) > 0)
        goto LAB66;

LAB67:    memcpy(t5, t47, 8);

LAB68:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng5)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB69;

LAB70:    xsi_set_current_line(73, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB71;

LAB72:    if (*((unsigned int *)t23) != 0)
        goto LAB73;

LAB74:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB75;

LAB76:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB77;

LAB78:    if (*((unsigned int *)t25) > 0)
        goto LAB79;

LAB80:    if (*((unsigned int *)t35) > 0)
        goto LAB81;

LAB82:    memcpy(t5, t47, 8);

LAB83:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng6)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB84;

LAB85:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB86;

LAB87:    if (*((unsigned int *)t23) != 0)
        goto LAB88;

LAB89:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB90;

LAB91:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB92;

LAB93:    if (*((unsigned int *)t25) > 0)
        goto LAB94;

LAB95:    if (*((unsigned int *)t35) > 0)
        goto LAB96;

LAB97:    memcpy(t5, t47, 8);

LAB98:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng7)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB99;

LAB100:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB101;

LAB102:    if (*((unsigned int *)t23) != 0)
        goto LAB103;

LAB104:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB105;

LAB106:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB107;

LAB108:    if (*((unsigned int *)t25) > 0)
        goto LAB109;

LAB110:    if (*((unsigned int *)t35) > 0)
        goto LAB111;

LAB112:    memcpy(t5, t47, 8);

LAB113:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng8)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB114;

LAB115:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB116;

LAB117:    if (*((unsigned int *)t23) != 0)
        goto LAB118;

LAB119:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB120;

LAB121:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB122;

LAB123:    if (*((unsigned int *)t25) > 0)
        goto LAB124;

LAB125:    if (*((unsigned int *)t35) > 0)
        goto LAB126;

LAB127:    memcpy(t5, t47, 8);

LAB128:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng9)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB129;

LAB130:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t36, 1, t3, t7, 2, t18, 32, 1);
    memset(t35, 0, 8);
    t23 = (t36 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t36);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB131;

LAB132:    if (*((unsigned int *)t23) != 0)
        goto LAB133;

LAB134:    t25 = (t35 + 4);
    t15 = *((unsigned int *)t35);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB135;

LAB136:    t19 = *((unsigned int *)t35);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB137;

LAB138:    if (*((unsigned int *)t25) > 0)
        goto LAB139;

LAB140:    if (*((unsigned int *)t35) > 0)
        goto LAB141;

LAB142:    memcpy(t5, t47, 8);

LAB143:    t67 = (t0 + 1448);
    t68 = (t0 + 1448);
    t69 = (t68 + 44U);
    t70 = *((char **)t69);
    t71 = (t0 + 1448);
    t73 = (t71 + 40U);
    t75 = *((char **)t73);
    t78 = (t0 + 1036U);
    t79 = *((char **)t78);
    xsi_vlog_generic_convert_array_indices(t72, t74, t70, t75, 2, 1, t79, 8, 2);
    t78 = (t0 + 1448);
    t80 = (t78 + 44U);
    t81 = *((char **)t80);
    t82 = (t0 + 1540);
    t83 = (t82 + 36U);
    t84 = *((char **)t83);
    t85 = ((char*)((ng3)));
    memset(t77, 0, 8);
    xsi_vlog_signed_multiply(t77, 32, t84, 32, t85, 32);
    t87 = ((char*)((ng10)));
    memset(t86, 0, 8);
    xsi_vlog_signed_add(t86, 32, t77, 32, t87, 32);
    xsi_vlog_generic_convert_bit_index(t76, t81, 2, t86, 32, 1);
    t88 = (t72 + 4);
    t39 = *((unsigned int *)t88);
    t98 = (!(t39));
    t89 = (t74 + 4);
    t50 = *((unsigned int *)t89);
    t101 = (!(t50));
    t102 = (t98 && t101);
    t90 = (t76 + 4);
    t51 = *((unsigned int *)t90);
    t105 = (!(t51));
    t106 = (t102 && t105);
    if (t106 == 1)
        goto LAB144;

LAB145:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 1540);
    t3 = (t2 + 36U);
    t6 = *((char **)t3);
    t7 = ((char*)((ng4)));
    memset(t5, 0, 8);
    xsi_vlog_signed_add(t5, 32, t6, 32, t7, 32);
    t13 = (t0 + 1540);
    xsi_vlogvar_assign_value(t13, t5, 0, 0, 32);
    goto LAB22;

LAB26:    *((unsigned int *)t37) = 1;
    goto LAB29;

LAB28:    t31 = (t37 + 4);
    *((unsigned int *)t37) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB29;

LAB30:    t33 = (t0 + 852U);
    t34 = *((char **)t33);
    t33 = (t0 + 828U);
    t41 = (t33 + 44U);
    t42 = *((char **)t41);
    t43 = (t0 + 1540);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    t46 = ((char*)((ng3)));
    memset(t47, 0, 8);
    xsi_vlog_signed_multiply(t47, 32, t45, 32, t46, 32);
    t48 = ((char*)((ng2)));
    memset(t49, 0, 8);
    xsi_vlog_signed_add(t49, 32, t47, 32, t48, 32);
    xsi_vlog_generic_get_index_select_value(t40, 1, t34, t42, 2, t49, 32, 1);
    goto LAB31;

LAB32:    t54 = (t0 + 1448);
    t55 = (t54 + 36U);
    t56 = *((char **)t55);
    t57 = (t0 + 1448);
    t58 = (t57 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1448);
    t61 = (t60 + 40U);
    t62 = *((char **)t61);
    t63 = (t0 + 1036U);
    t64 = *((char **)t63);
    xsi_vlog_generic_get_array_select_value(t4, 128, t56, t59, t62, 2, 1, t64, 8, 2);
    t63 = (t0 + 1448);
    t66 = (t63 + 44U);
    t67 = *((char **)t66);
    t68 = (t0 + 1540);
    t69 = (t68 + 36U);
    t70 = *((char **)t69);
    t71 = ((char*)((ng3)));
    memset(t72, 0, 8);
    xsi_vlog_signed_multiply(t72, 32, t70, 32, t71, 32);
    t73 = ((char*)((ng2)));
    memset(t74, 0, 8);
    xsi_vlog_signed_add(t74, 32, t72, 32, t73, 32);
    xsi_vlog_generic_get_index_select_value(t65, 1, t4, t67, 2, t74, 32, 1);
    goto LAB33;

LAB34:    xsi_vlog_unsigned_bit_combine(t36, 1, t40, 1, t65, 1);
    goto LAB38;

LAB36:    memcpy(t36, t40, 8);
    goto LAB38;

LAB39:    t107 = *((unsigned int *)t77);
    t108 = *((unsigned int *)t86);
    t109 = (t107 + t108);
    xsi_vlogvar_wait_assign_value(t75, t36, 0, t109, 1, 0LL);
    goto LAB40;

LAB41:    *((unsigned int *)t35) = 1;
    goto LAB44;

LAB43:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB44;

LAB45:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng4)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB46;

LAB47:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng4)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB48;

LAB49:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB53;

LAB51:    memcpy(t5, t37, 8);
    goto LAB53;

LAB54:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB55;

LAB56:    *((unsigned int *)t35) = 1;
    goto LAB59;

LAB58:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB59;

LAB60:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng5)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB61;

LAB62:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng5)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB63;

LAB64:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB68;

LAB66:    memcpy(t5, t37, 8);
    goto LAB68;

LAB69:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB70;

LAB71:    *((unsigned int *)t35) = 1;
    goto LAB74;

LAB73:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB74;

LAB75:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng6)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB76;

LAB77:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng6)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB78;

LAB79:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB83;

LAB81:    memcpy(t5, t37, 8);
    goto LAB83;

LAB84:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB85;

LAB86:    *((unsigned int *)t35) = 1;
    goto LAB89;

LAB88:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB89;

LAB90:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng7)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB91;

LAB92:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng7)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB93;

LAB94:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB98;

LAB96:    memcpy(t5, t37, 8);
    goto LAB98;

LAB99:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB100;

LAB101:    *((unsigned int *)t35) = 1;
    goto LAB104;

LAB103:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB104;

LAB105:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng8)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB106;

LAB107:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng8)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB108;

LAB109:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB113;

LAB111:    memcpy(t5, t37, 8);
    goto LAB113;

LAB114:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB115;

LAB116:    *((unsigned int *)t35) = 1;
    goto LAB119;

LAB118:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB119;

LAB120:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng9)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB121;

LAB122:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng9)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB123;

LAB124:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB128;

LAB126:    memcpy(t5, t37, 8);
    goto LAB128;

LAB129:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB130;

LAB131:    *((unsigned int *)t35) = 1;
    goto LAB134;

LAB133:    t24 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB134;

LAB135:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t38, 0, 8);
    xsi_vlog_signed_multiply(t38, 32, t33, 32, t34, 32);
    t41 = ((char*)((ng10)));
    memset(t40, 0, 8);
    xsi_vlog_signed_add(t40, 32, t38, 32, t41, 32);
    xsi_vlog_generic_get_index_select_value(t37, 1, t28, t30, 2, t40, 32, 1);
    goto LAB136;

LAB137:    t42 = (t0 + 1448);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    t45 = (t0 + 1448);
    t46 = (t45 + 44U);
    t48 = *((char **)t46);
    t54 = (t0 + 1448);
    t55 = (t54 + 40U);
    t56 = *((char **)t55);
    t57 = (t0 + 1036U);
    t58 = *((char **)t57);
    xsi_vlog_generic_get_array_select_value(t4, 128, t44, t48, t56, 2, 1, t58, 8, 2);
    t57 = (t0 + 1448);
    t59 = (t57 + 44U);
    t60 = *((char **)t59);
    t61 = (t0 + 1540);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    t64 = ((char*)((ng3)));
    memset(t49, 0, 8);
    xsi_vlog_signed_multiply(t49, 32, t63, 32, t64, 32);
    t66 = ((char*)((ng10)));
    memset(t65, 0, 8);
    xsi_vlog_signed_add(t65, 32, t49, 32, t66, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t4, t60, 2, t65, 32, 1);
    goto LAB138;

LAB139:    xsi_vlog_unsigned_bit_combine(t5, 1, t37, 1, t47, 1);
    goto LAB143;

LAB141:    memcpy(t5, t37, 8);
    goto LAB143;

LAB144:    t52 = *((unsigned int *)t74);
    t53 = *((unsigned int *)t76);
    t109 = (t52 + t53);
    xsi_vlogvar_wait_assign_value(t67, t5, 0, t109, 1, 0LL);
    goto LAB145;

}


extern void work_m_00000000001666995155_2268296884_init()
{
	static char *pe[] = {(void *)Always_61_0};
	xsi_register_didat("work_m_00000000001666995155_2268296884", "isim/amber-test.exe.sim/work/m_00000000001666995155_2268296884.didat");
	xsi_register_executes(pe);
}
