/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/lib/generic_sram_byte_en.v";
static unsigned int ng1[] = {0U, 0U};
static int ng2[] = {0, 0};
static int ng3[] = {8, 0};
static int ng4[] = {1, 0};
static int ng5[] = {2, 0};
static int ng6[] = {3, 0};
static int ng7[] = {4, 0};
static int ng8[] = {5, 0};
static int ng9[] = {6, 0};
static int ng10[] = {7, 0};



static void Always_61_0(char *t0)
{
    char t4[8];
    char t5[8];
    char t26[8];
    char t35[8];
    char t36[8];
    char t38[8];
    char t45[8];
    char t47[8];
    char t55[8];
    char t64[8];
    char t71[8];
    char t73[8];
    char t75[8];
    char t76[8];
    char t85[8];
    char t92[8];
    char t94[8];
    char *t1;
    char *t2;
    char *t3;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    unsigned int t37;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t46;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    char *t52;
    char *t53;
    char *t54;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    char *t60;
    char *t61;
    char *t62;
    char *t63;
    char *t65;
    char *t66;
    char *t67;
    char *t68;
    char *t69;
    char *t70;
    char *t72;
    char *t74;
    char *t77;
    char *t78;
    char *t79;
    char *t80;
    char *t81;
    char *t82;
    char *t83;
    char *t84;
    char *t86;
    char *t87;
    char *t88;
    char *t89;
    char *t90;
    char *t91;
    char *t93;
    char *t95;
    unsigned int t96;
    int t97;
    char *t98;
    unsigned int t99;
    int t100;
    int t101;
    char *t102;
    unsigned int t103;
    int t104;
    int t105;
    unsigned int t106;
    unsigned int t107;
    int t108;

LAB0:    t1 = (t0 + 2052U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 2232);
    *((int *)t2) = 1;
    t3 = (t0 + 2076);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(62, ng0);

LAB5:    xsi_set_current_line(64, ng0);
    t6 = (t0 + 944U);
    t7 = *((char **)t6);
    memset(t5, 0, 8);
    t6 = (t7 + 4);
    t8 = *((unsigned int *)t6);
    t9 = (~(t8));
    t10 = *((unsigned int *)t7);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB6;

LAB7:    if (*((unsigned int *)t6) != 0)
        goto LAB8;

LAB9:    t14 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t14);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB10;

LAB11:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t14);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t14) > 0)
        goto LAB14;

LAB15:    if (*((unsigned int *)t5) > 0)
        goto LAB16;

LAB17:    memcpy(t4, t26, 8);

LAB18:    t33 = (t0 + 1356);
    xsi_vlogvar_wait_assign_value(t33, t4, 0, 0, 32, 0LL);
    xsi_set_current_line(67, ng0);
    t2 = (t0 + 944U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t8 = *((unsigned int *)t2);
    t9 = (~(t8));
    t10 = *((unsigned int *)t3);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB19;

LAB20:
LAB21:    goto LAB2;

LAB6:    *((unsigned int *)t5) = 1;
    goto LAB9;

LAB8:    t13 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t13) = 1;
    goto LAB9;

LAB10:    t18 = ((char*)((ng1)));
    goto LAB11;

LAB12:    t23 = (t0 + 1448);
    t24 = (t23 + 36U);
    t25 = *((char **)t24);
    t27 = (t0 + 1448);
    t28 = (t27 + 44U);
    t29 = *((char **)t28);
    t30 = (t0 + 1448);
    t31 = (t30 + 40U);
    t32 = *((char **)t31);
    t33 = (t0 + 1036U);
    t34 = *((char **)t33);
    xsi_vlog_generic_get_array_select_value(t26, 32, t25, t29, t32, 2, 1, t34, 8, 2);
    goto LAB13;

LAB14:    xsi_vlog_unsigned_bit_combine(t4, 32, t18, 32, t26, 32);
    goto LAB18;

LAB16:    memcpy(t4, t18, 8);
    goto LAB18;

LAB19:    xsi_set_current_line(68, ng0);
    xsi_set_current_line(68, ng0);
    t6 = ((char*)((ng2)));
    t7 = (t0 + 1540);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 32);

LAB22:    t2 = (t0 + 1540);
    t3 = (t2 + 36U);
    t6 = *((char **)t3);
    t7 = (t0 + 264);
    t13 = *((char **)t7);
    t7 = ((char*)((ng3)));
    memset(t4, 0, 8);
    xsi_vlog_signed_divide(t4, 32, t13, 32, t7, 32);
    memset(t5, 0, 8);
    xsi_vlog_signed_less(t5, 32, t6, 32, t4, 32);
    t14 = (t5 + 4);
    t8 = *((unsigned int *)t14);
    t9 = (~(t8));
    t10 = *((unsigned int *)t5);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB23;

LAB24:    goto LAB21;

LAB23:    xsi_set_current_line(69, ng0);

LAB25:    xsi_set_current_line(70, ng0);
    t18 = (t0 + 1128U);
    t23 = *((char **)t18);
    t18 = (t0 + 1104U);
    t24 = (t18 + 44U);
    t25 = *((char **)t24);
    t27 = (t0 + 1540);
    t28 = (t27 + 36U);
    t29 = *((char **)t28);
    xsi_vlog_generic_get_index_select_value(t36, 1, t23, t25, 2, t29, 32, 1);
    memset(t35, 0, 8);
    t30 = (t36 + 4);
    t15 = *((unsigned int *)t30);
    t16 = (~(t15));
    t17 = *((unsigned int *)t36);
    t19 = (t17 & t16);
    t20 = (t19 & 1U);
    if (t20 != 0)
        goto LAB26;

LAB27:    if (*((unsigned int *)t30) != 0)
        goto LAB28;

LAB29:    t32 = (t35 + 4);
    t21 = *((unsigned int *)t35);
    t22 = *((unsigned int *)t32);
    t37 = (t21 || t22);
    if (t37 > 0)
        goto LAB30;

LAB31:    t48 = *((unsigned int *)t35);
    t49 = (~(t48));
    t50 = *((unsigned int *)t32);
    t51 = (t49 || t50);
    if (t51 > 0)
        goto LAB32;

LAB33:    if (*((unsigned int *)t32) > 0)
        goto LAB34;

LAB35:    if (*((unsigned int *)t35) > 0)
        goto LAB36;

LAB37:    memcpy(t26, t64, 8);

LAB38:    t74 = (t0 + 1448);
    t77 = (t0 + 1448);
    t78 = (t77 + 44U);
    t79 = *((char **)t78);
    t80 = (t0 + 1448);
    t81 = (t80 + 40U);
    t82 = *((char **)t81);
    t83 = (t0 + 1036U);
    t84 = *((char **)t83);
    xsi_vlog_generic_convert_array_indices(t75, t76, t79, t82, 2, 1, t84, 8, 2);
    t83 = (t0 + 1448);
    t86 = (t83 + 44U);
    t87 = *((char **)t86);
    t88 = (t0 + 1540);
    t89 = (t88 + 36U);
    t90 = *((char **)t89);
    t91 = ((char*)((ng3)));
    memset(t92, 0, 8);
    xsi_vlog_signed_multiply(t92, 32, t90, 32, t91, 32);
    t93 = ((char*)((ng2)));
    memset(t94, 0, 8);
    xsi_vlog_signed_add(t94, 32, t92, 32, t93, 32);
    xsi_vlog_generic_convert_bit_index(t85, t87, 2, t94, 32, 1);
    t95 = (t75 + 4);
    t96 = *((unsigned int *)t95);
    t97 = (!(t96));
    t98 = (t76 + 4);
    t99 = *((unsigned int *)t98);
    t100 = (!(t99));
    t101 = (t97 && t100);
    t102 = (t85 + 4);
    t103 = *((unsigned int *)t102);
    t104 = (!(t103));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB39;

LAB40:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB41;

LAB42:    if (*((unsigned int *)t23) != 0)
        goto LAB43;

LAB44:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB45;

LAB46:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB47;

LAB48:    if (*((unsigned int *)t25) > 0)
        goto LAB49;

LAB50:    if (*((unsigned int *)t5) > 0)
        goto LAB51;

LAB52:    memcpy(t4, t47, 8);

LAB53:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng4)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB54;

LAB55:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB56;

LAB57:    if (*((unsigned int *)t23) != 0)
        goto LAB58;

LAB59:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB60;

LAB61:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB62;

LAB63:    if (*((unsigned int *)t25) > 0)
        goto LAB64;

LAB65:    if (*((unsigned int *)t5) > 0)
        goto LAB66;

LAB67:    memcpy(t4, t47, 8);

LAB68:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng5)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB69;

LAB70:    xsi_set_current_line(73, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB71;

LAB72:    if (*((unsigned int *)t23) != 0)
        goto LAB73;

LAB74:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB75;

LAB76:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB77;

LAB78:    if (*((unsigned int *)t25) > 0)
        goto LAB79;

LAB80:    if (*((unsigned int *)t5) > 0)
        goto LAB81;

LAB82:    memcpy(t4, t47, 8);

LAB83:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng6)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB84;

LAB85:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB86;

LAB87:    if (*((unsigned int *)t23) != 0)
        goto LAB88;

LAB89:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB90;

LAB91:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB92;

LAB93:    if (*((unsigned int *)t25) > 0)
        goto LAB94;

LAB95:    if (*((unsigned int *)t5) > 0)
        goto LAB96;

LAB97:    memcpy(t4, t47, 8);

LAB98:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng7)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB99;

LAB100:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB101;

LAB102:    if (*((unsigned int *)t23) != 0)
        goto LAB103;

LAB104:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB105;

LAB106:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB107;

LAB108:    if (*((unsigned int *)t25) > 0)
        goto LAB109;

LAB110:    if (*((unsigned int *)t5) > 0)
        goto LAB111;

LAB112:    memcpy(t4, t47, 8);

LAB113:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng8)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB114;

LAB115:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB116;

LAB117:    if (*((unsigned int *)t23) != 0)
        goto LAB118;

LAB119:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB120;

LAB121:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB122;

LAB123:    if (*((unsigned int *)t25) > 0)
        goto LAB124;

LAB125:    if (*((unsigned int *)t5) > 0)
        goto LAB126;

LAB127:    memcpy(t4, t47, 8);

LAB128:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng9)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB129;

LAB130:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 1128U);
    t3 = *((char **)t2);
    t2 = (t0 + 1104U);
    t6 = (t2 + 44U);
    t7 = *((char **)t6);
    t13 = (t0 + 1540);
    t14 = (t13 + 36U);
    t18 = *((char **)t14);
    xsi_vlog_generic_get_index_select_value(t26, 1, t3, t7, 2, t18, 32, 1);
    memset(t5, 0, 8);
    t23 = (t26 + 4);
    t8 = *((unsigned int *)t23);
    t9 = (~(t8));
    t10 = *((unsigned int *)t26);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB131;

LAB132:    if (*((unsigned int *)t23) != 0)
        goto LAB133;

LAB134:    t25 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t25);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB135;

LAB136:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t25);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB137;

LAB138:    if (*((unsigned int *)t25) > 0)
        goto LAB139;

LAB140:    if (*((unsigned int *)t5) > 0)
        goto LAB141;

LAB142:    memcpy(t4, t47, 8);

LAB143:    t66 = (t0 + 1448);
    t67 = (t0 + 1448);
    t68 = (t67 + 44U);
    t69 = *((char **)t68);
    t70 = (t0 + 1448);
    t72 = (t70 + 40U);
    t74 = *((char **)t72);
    t77 = (t0 + 1036U);
    t78 = *((char **)t77);
    xsi_vlog_generic_convert_array_indices(t71, t73, t69, t74, 2, 1, t78, 8, 2);
    t77 = (t0 + 1448);
    t79 = (t77 + 44U);
    t80 = *((char **)t79);
    t81 = (t0 + 1540);
    t82 = (t81 + 36U);
    t83 = *((char **)t82);
    t84 = ((char*)((ng3)));
    memset(t76, 0, 8);
    xsi_vlog_signed_multiply(t76, 32, t83, 32, t84, 32);
    t86 = ((char*)((ng10)));
    memset(t85, 0, 8);
    xsi_vlog_signed_add(t85, 32, t76, 32, t86, 32);
    xsi_vlog_generic_convert_bit_index(t75, t80, 2, t85, 32, 1);
    t87 = (t71 + 4);
    t37 = *((unsigned int *)t87);
    t97 = (!(t37));
    t88 = (t73 + 4);
    t48 = *((unsigned int *)t88);
    t100 = (!(t48));
    t101 = (t97 && t100);
    t89 = (t75 + 4);
    t49 = *((unsigned int *)t89);
    t104 = (!(t49));
    t105 = (t101 && t104);
    if (t105 == 1)
        goto LAB144;

LAB145:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 1540);
    t3 = (t2 + 36U);
    t6 = *((char **)t3);
    t7 = ((char*)((ng4)));
    memset(t4, 0, 8);
    xsi_vlog_signed_add(t4, 32, t6, 32, t7, 32);
    t13 = (t0 + 1540);
    xsi_vlogvar_assign_value(t13, t4, 0, 0, 32);
    goto LAB22;

LAB26:    *((unsigned int *)t35) = 1;
    goto LAB29;

LAB28:    t31 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB29;

LAB30:    t33 = (t0 + 852U);
    t34 = *((char **)t33);
    t33 = (t0 + 828U);
    t39 = (t33 + 44U);
    t40 = *((char **)t39);
    t41 = (t0 + 1540);
    t42 = (t41 + 36U);
    t43 = *((char **)t42);
    t44 = ((char*)((ng3)));
    memset(t45, 0, 8);
    xsi_vlog_signed_multiply(t45, 32, t43, 32, t44, 32);
    t46 = ((char*)((ng2)));
    memset(t47, 0, 8);
    xsi_vlog_signed_add(t47, 32, t45, 32, t46, 32);
    xsi_vlog_generic_get_index_select_value(t38, 1, t34, t40, 2, t47, 32, 1);
    goto LAB31;

LAB32:    t52 = (t0 + 1448);
    t53 = (t52 + 36U);
    t54 = *((char **)t53);
    t56 = (t0 + 1448);
    t57 = (t56 + 44U);
    t58 = *((char **)t57);
    t59 = (t0 + 1448);
    t60 = (t59 + 40U);
    t61 = *((char **)t60);
    t62 = (t0 + 1036U);
    t63 = *((char **)t62);
    xsi_vlog_generic_get_array_select_value(t55, 32, t54, t58, t61, 2, 1, t63, 8, 2);
    t62 = (t0 + 1448);
    t65 = (t62 + 44U);
    t66 = *((char **)t65);
    t67 = (t0 + 1540);
    t68 = (t67 + 36U);
    t69 = *((char **)t68);
    t70 = ((char*)((ng3)));
    memset(t71, 0, 8);
    xsi_vlog_signed_multiply(t71, 32, t69, 32, t70, 32);
    t72 = ((char*)((ng2)));
    memset(t73, 0, 8);
    xsi_vlog_signed_add(t73, 32, t71, 32, t72, 32);
    xsi_vlog_generic_get_index_select_value(t64, 1, t55, t66, 2, t73, 32, 1);
    goto LAB33;

LAB34:    xsi_vlog_unsigned_bit_combine(t26, 1, t38, 1, t64, 1);
    goto LAB38;

LAB36:    memcpy(t26, t38, 8);
    goto LAB38;

LAB39:    t106 = *((unsigned int *)t76);
    t107 = *((unsigned int *)t85);
    t108 = (t106 + t107);
    xsi_vlogvar_wait_assign_value(t74, t26, 0, t108, 1, 0LL);
    goto LAB40;

LAB41:    *((unsigned int *)t5) = 1;
    goto LAB44;

LAB43:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB44;

LAB45:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng4)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB46;

LAB47:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng4)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB48;

LAB49:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB53;

LAB51:    memcpy(t4, t35, 8);
    goto LAB53;

LAB54:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB55;

LAB56:    *((unsigned int *)t5) = 1;
    goto LAB59;

LAB58:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB59;

LAB60:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng5)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB61;

LAB62:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng5)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB63;

LAB64:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB68;

LAB66:    memcpy(t4, t35, 8);
    goto LAB68;

LAB69:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB70;

LAB71:    *((unsigned int *)t5) = 1;
    goto LAB74;

LAB73:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB74;

LAB75:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng6)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB76;

LAB77:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng6)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB78;

LAB79:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB83;

LAB81:    memcpy(t4, t35, 8);
    goto LAB83;

LAB84:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB85;

LAB86:    *((unsigned int *)t5) = 1;
    goto LAB89;

LAB88:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB89;

LAB90:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng7)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB91;

LAB92:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng7)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB93;

LAB94:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB98;

LAB96:    memcpy(t4, t35, 8);
    goto LAB98;

LAB99:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB100;

LAB101:    *((unsigned int *)t5) = 1;
    goto LAB104;

LAB103:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB104;

LAB105:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng8)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB106;

LAB107:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng8)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB108;

LAB109:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB113;

LAB111:    memcpy(t4, t35, 8);
    goto LAB113;

LAB114:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB115;

LAB116:    *((unsigned int *)t5) = 1;
    goto LAB119;

LAB118:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB119;

LAB120:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng9)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB121;

LAB122:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng9)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB123;

LAB124:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB128;

LAB126:    memcpy(t4, t35, 8);
    goto LAB128;

LAB129:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB130;

LAB131:    *((unsigned int *)t5) = 1;
    goto LAB134;

LAB133:    t24 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t24) = 1;
    goto LAB134;

LAB135:    t27 = (t0 + 852U);
    t28 = *((char **)t27);
    t27 = (t0 + 828U);
    t29 = (t27 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1540);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    t34 = ((char*)((ng3)));
    memset(t36, 0, 8);
    xsi_vlog_signed_multiply(t36, 32, t33, 32, t34, 32);
    t39 = ((char*)((ng10)));
    memset(t38, 0, 8);
    xsi_vlog_signed_add(t38, 32, t36, 32, t39, 32);
    xsi_vlog_generic_get_index_select_value(t35, 1, t28, t30, 2, t38, 32, 1);
    goto LAB136;

LAB137:    t40 = (t0 + 1448);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t0 + 1448);
    t44 = (t43 + 44U);
    t46 = *((char **)t44);
    t52 = (t0 + 1448);
    t53 = (t52 + 40U);
    t54 = *((char **)t53);
    t56 = (t0 + 1036U);
    t57 = *((char **)t56);
    xsi_vlog_generic_get_array_select_value(t45, 32, t42, t46, t54, 2, 1, t57, 8, 2);
    t56 = (t0 + 1448);
    t58 = (t56 + 44U);
    t59 = *((char **)t58);
    t60 = (t0 + 1540);
    t61 = (t60 + 36U);
    t62 = *((char **)t61);
    t63 = ((char*)((ng3)));
    memset(t55, 0, 8);
    xsi_vlog_signed_multiply(t55, 32, t62, 32, t63, 32);
    t65 = ((char*)((ng10)));
    memset(t64, 0, 8);
    xsi_vlog_signed_add(t64, 32, t55, 32, t65, 32);
    xsi_vlog_generic_get_index_select_value(t47, 1, t45, t59, 2, t64, 32, 1);
    goto LAB138;

LAB139:    xsi_vlog_unsigned_bit_combine(t4, 1, t35, 1, t47, 1);
    goto LAB143;

LAB141:    memcpy(t4, t35, 8);
    goto LAB143;

LAB144:    t50 = *((unsigned int *)t73);
    t51 = *((unsigned int *)t75);
    t108 = (t50 + t51);
    xsi_vlogvar_wait_assign_value(t66, t4, 0, t108, 1, 0LL);
    goto LAB145;

}


extern void work_m_00000000001666995155_3262520233_init()
{
	static char *pe[] = {(void *)Always_61_0};
	xsi_register_didat("work_m_00000000001666995155_3262520233", "isim/amber-test.exe.sim/work/m_00000000001666995155_3262520233.didat");
	xsi_register_executes(pe);
}
