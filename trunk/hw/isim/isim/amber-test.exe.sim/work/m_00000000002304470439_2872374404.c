/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/system/clocks_resets.v";
static int ng1[] = {4, 0, 0, 0};
static unsigned int ng2[] = {1U, 0U};
static int ng3[] = {2, 0, 0, 0};
static unsigned int ng4[] = {0U, 0U};
static int ng5[] = {20, 0};
static int ng6[] = {2, 0};
static int ng7[] = {1, 0};
static int ng8[] = {0, 0};



static void Cont_64_0(char *t0)
{
    char t4[8];
    char t16[8];
    char t25[8];
    char t33[8];
    char *t1;
    char *t2;
    char *t3;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    char *t37;
    char *t38;
    char *t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    char *t61;
    char *t62;
    char *t63;
    char *t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;
    unsigned int t69;
    unsigned int t70;
    char *t71;
    unsigned int t72;
    unsigned int t73;
    char *t74;

LAB0:    t1 = (t0 + 2536U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 1336U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t3 + 4);
    t5 = *((unsigned int *)t2);
    t6 = (~(t5));
    t7 = *((unsigned int *)t3);
    t8 = (t7 & t6);
    t9 = (t8 & 1U);
    if (t9 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t11 = (t4 + 4);
    t12 = *((unsigned int *)t4);
    t13 = (!(t12));
    t14 = *((unsigned int *)t11);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    memcpy(t33, t4, 8);

LAB10:    t61 = (t0 + 3768);
    t62 = (t61 + 32U);
    t63 = *((char **)t62);
    t64 = (t63 + 32U);
    t65 = *((char **)t64);
    memset(t65, 0, 8);
    t66 = 1U;
    t67 = t66;
    t68 = (t33 + 4);
    t69 = *((unsigned int *)t33);
    t66 = (t66 & t69);
    t70 = *((unsigned int *)t68);
    t67 = (t67 & t70);
    t71 = (t65 + 4);
    t72 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t72 | t66);
    t73 = *((unsigned int *)t71);
    *((unsigned int *)t71) = (t73 | t67);
    xsi_driver_vfirst_trans(t61, 0, 0);
    t74 = (t0 + 3668);
    *((int *)t74) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t10 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB7;

LAB8:    t17 = (t0 + 1244U);
    t18 = *((char **)t17);
    memset(t16, 0, 8);
    t17 = (t18 + 4);
    t19 = *((unsigned int *)t17);
    t20 = (~(t19));
    t21 = *((unsigned int *)t18);
    t22 = (t21 & t20);
    t23 = (t22 & 1U);
    if (t23 != 0)
        goto LAB14;

LAB12:    if (*((unsigned int *)t17) == 0)
        goto LAB11;

LAB13:    t24 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t24) = 1;

LAB14:    memset(t25, 0, 8);
    t26 = (t16 + 4);
    t27 = *((unsigned int *)t26);
    t28 = (~(t27));
    t29 = *((unsigned int *)t16);
    t30 = (t29 & t28);
    t31 = (t30 & 1U);
    if (t31 != 0)
        goto LAB15;

LAB16:    if (*((unsigned int *)t26) != 0)
        goto LAB17;

LAB18:    t34 = *((unsigned int *)t4);
    t35 = *((unsigned int *)t25);
    t36 = (t34 | t35);
    *((unsigned int *)t33) = t36;
    t37 = (t4 + 4);
    t38 = (t25 + 4);
    t39 = (t33 + 4);
    t40 = *((unsigned int *)t37);
    t41 = *((unsigned int *)t38);
    t42 = (t40 | t41);
    *((unsigned int *)t39) = t42;
    t43 = *((unsigned int *)t39);
    t44 = (t43 != 0);
    if (t44 == 1)
        goto LAB19;

LAB20:
LAB21:    goto LAB10;

LAB11:    *((unsigned int *)t16) = 1;
    goto LAB14;

LAB15:    *((unsigned int *)t25) = 1;
    goto LAB18;

LAB17:    t32 = (t25 + 4);
    *((unsigned int *)t25) = 1;
    *((unsigned int *)t32) = 1;
    goto LAB18;

LAB19:    t45 = *((unsigned int *)t33);
    t46 = *((unsigned int *)t39);
    *((unsigned int *)t33) = (t45 | t46);
    t47 = (t4 + 4);
    t48 = (t25 + 4);
    t49 = *((unsigned int *)t47);
    t50 = (~(t49));
    t51 = *((unsigned int *)t4);
    t52 = (t51 & t50);
    t53 = *((unsigned int *)t48);
    t54 = (~(t53));
    t55 = *((unsigned int *)t25);
    t56 = (t55 & t54);
    t57 = (~(t52));
    t58 = (~(t56));
    t59 = *((unsigned int *)t39);
    *((unsigned int *)t39) = (t59 & t57);
    t60 = *((unsigned int *)t39);
    *((unsigned int *)t39) = (t60 & t58);
    goto LAB21;

}

static void Initial_252_1(char *t0)
{
    char t4[16];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    double t6;
    char *t7;
    char *t8;
    char *t9;
    double t10;
    double t11;
    char *t12;

LAB0:    t1 = (t0 + 2672U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(253, ng0);

LAB4:    xsi_set_current_line(254, ng0);
    t2 = (t0 + 3676);
    *((int *)t2) = 1;
    t3 = (t0 + 2696);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB5;

LAB1:    return;
LAB5:    xsi_set_current_line(255, ng0);
    t5 = xsi_vlog_time(t4, 1.0000000000000000, 1.0000000000000000);
    t6 = xsi_vlog_convert_to_real(t4, 64, 2);
    t7 = (t0 + 1748);
    xsi_vlogvar_assign_value_double(t7, t6, 0);
    xsi_set_current_line(256, ng0);
    t2 = (t0 + 3684);
    *((int *)t2) = 1;
    t3 = (t0 + 2696);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB6;
    goto LAB1;

LAB6:    xsi_set_current_line(257, ng0);
    t5 = xsi_vlog_time(t4, 1.0000000000000000, 1.0000000000000000);
    t6 = xsi_vlog_convert_to_real(t4, 64, 2);
    t7 = (t0 + 1748);
    t8 = (t7 + 36U);
    t9 = *((char **)t8);
    t10 = *((double *)t9);
    t11 = (t6 - t10);
    t12 = (t0 + 1564);
    xsi_vlogvar_assign_value_double(t12, t11, 0);
    xsi_set_current_line(258, ng0);
    t2 = (t0 + 1564);
    t3 = (t2 + 36U);
    t5 = *((char **)t3);
    t6 = *((double *)t5);
    t7 = ((char*)((ng1)));
    t10 = xsi_vlog_convert_to_real(t7, 32, 1);
    t11 = (t6 / t10);
    t8 = (t0 + 1656);
    xsi_vlogvar_assign_value_double(t8, t11, 0);
    goto LAB1;

}

static void Always_262_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    double t6;
    double t7;
    double t8;
    double t9;
    char *t10;
    char *t11;
    char *t12;

LAB0:    t1 = (t0 + 2808U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(262, ng0);
    t2 = (t0 + 3692);
    *((int *)t2) = 1;
    t3 = (t0 + 2832);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(263, ng0);

LAB5:    xsi_set_current_line(264, ng0);
    t4 = ((char*)((ng2)));
    t5 = (t0 + 1840);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 1);
    xsi_set_current_line(265, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB6;

LAB7:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB8:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB9;
    goto LAB1;

LAB6:    t8 = 0.0000000000000000;
    goto LAB8;

LAB9:    xsi_set_current_line(266, ng0);
    t11 = ((char*)((ng4)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(267, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB10;

LAB11:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB12:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB13;
    goto LAB1;

LAB10:    t8 = 0.0000000000000000;
    goto LAB12;

LAB13:    xsi_set_current_line(269, ng0);
    t11 = ((char*)((ng2)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(270, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB14;

LAB15:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB16:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB17;
    goto LAB1;

LAB14:    t8 = 0.0000000000000000;
    goto LAB16;

LAB17:    xsi_set_current_line(271, ng0);
    t11 = ((char*)((ng4)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(272, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB18;

LAB19:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB20:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB21;
    goto LAB1;

LAB18:    t8 = 0.0000000000000000;
    goto LAB20;

LAB21:    xsi_set_current_line(274, ng0);
    t11 = ((char*)((ng2)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(275, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB22;

LAB23:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB24:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB25;
    goto LAB1;

LAB22:    t8 = 0.0000000000000000;
    goto LAB24;

LAB25:    xsi_set_current_line(276, ng0);
    t11 = ((char*)((ng4)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(277, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB26;

LAB27:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB28:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB29;
    goto LAB1;

LAB26:    t8 = 0.0000000000000000;
    goto LAB28;

LAB29:    xsi_set_current_line(279, ng0);
    t11 = ((char*)((ng2)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    xsi_set_current_line(280, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = *((double *)t4);
    t5 = ((char*)((ng3)));
    t7 = xsi_vlog_convert_to_real(t5, 32, 1);
    t8 = (t6 / t7);
    t9 = (t8 < 0.0000000000000000);
    if (t9 == 1)
        goto LAB30;

LAB31:    t8 = (t8 + 0.50000000000000000);
    t8 = ((int64)(t8));

LAB32:    t8 = (t8 * 1.0000000000000000);
    t10 = (t0 + 2708);
    xsi_process_wait(t10, t8);
    *((char **)t1) = &&LAB33;
    goto LAB1;

LAB30:    t8 = 0.0000000000000000;
    goto LAB32;

LAB33:    xsi_set_current_line(281, ng0);
    t11 = ((char*)((ng4)));
    t12 = (t0 + 1840);
    xsi_vlogvar_assign_value(t12, t11, 0, 0, 1);
    goto LAB2;

}

static void Always_286_3(char *t0)
{
    char t9[8];
    char t11[8];
    char t12[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t10;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    char *t20;

LAB0:    t1 = (t0 + 2944U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(286, ng0);
    t2 = (t0 + 3700);
    *((int *)t2) = 1;
    t3 = (t0 + 2968);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(287, ng0);

LAB5:    xsi_set_current_line(288, ng0);
    t4 = (t0 + 2024);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    t7 = ((char*)((ng5)));
    t8 = ((char*)((ng6)));
    memset(t9, 0, 8);
    xsi_vlog_signed_multiply(t9, 32, t7, 32, t8, 32);
    t10 = ((char*)((ng7)));
    memset(t11, 0, 8);
    xsi_vlog_signed_minus(t11, 32, t9, 32, t10, 32);
    memset(t12, 0, 8);
    xsi_vlog_signed_equal(t12, 32, t6, 32, t11, 32);
    t13 = (t12 + 4);
    t14 = *((unsigned int *)t13);
    t15 = (~(t14));
    t16 = *((unsigned int *)t12);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB6;

LAB7:    xsi_set_current_line(293, ng0);
    t2 = (t0 + 2024);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng2)));
    memset(t9, 0, 8);
    xsi_vlog_unsigned_add(t9, 32, t4, 32, t5, 32);
    t6 = (t0 + 2024);
    xsi_vlogvar_wait_assign_value(t6, t9, 0, 0, 32, 0LL);

LAB8:    xsi_set_current_line(295, ng0);
    t2 = (t0 + 2024);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng8)));
    memset(t9, 0, 8);
    xsi_vlog_signed_equal(t9, 32, t4, 32, t5, 32);
    t6 = (t9 + 4);
    t14 = *((unsigned int *)t6);
    t15 = (~(t14));
    t16 = *((unsigned int *)t9);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB9;

LAB10:    xsi_set_current_line(297, ng0);
    t2 = (t0 + 2024);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng5)));
    memset(t9, 0, 8);
    xsi_vlog_signed_equal(t9, 32, t4, 32, t5, 32);
    t6 = (t9 + 4);
    t14 = *((unsigned int *)t6);
    t15 = (~(t14));
    t16 = *((unsigned int *)t9);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB12;

LAB13:
LAB14:
LAB11:    goto LAB2;

LAB6:    xsi_set_current_line(291, ng0);
    t19 = ((char*)((ng4)));
    t20 = (t0 + 2024);
    xsi_vlogvar_wait_assign_value(t20, t19, 0, 0, 32, 0LL);
    goto LAB8;

LAB9:    xsi_set_current_line(296, ng0);
    t7 = ((char*)((ng2)));
    t8 = (t0 + 1932);
    xsi_vlogvar_assign_value(t8, t7, 0, 0, 1);
    goto LAB11;

LAB12:    xsi_set_current_line(300, ng0);
    t7 = ((char*)((ng4)));
    t8 = (t0 + 1932);
    xsi_vlogvar_assign_value(t8, t7, 0, 0, 1);
    goto LAB14;

}

static void Cont_303_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;

LAB0:    t1 = (t0 + 3080U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(303, ng0);
    t2 = (t0 + 1932);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 3804);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    memset(t9, 0, 8);
    t10 = 1U;
    t11 = t10;
    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t10 = (t10 & t13);
    t14 = *((unsigned int *)t12);
    t11 = (t11 & t14);
    t15 = (t9 + 4);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 | t10);
    t17 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t17 | t11);
    xsi_driver_vfirst_trans(t5, 0, 0);
    t18 = (t0 + 3708);
    *((int *)t18) = 1;

LAB1:    return;
}

static void Cont_304_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;

LAB0:    t1 = (t0 + 3216U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(304, ng0);
    t2 = (t0 + 600U);
    t3 = *((char **)t2);
    t2 = (t0 + 3840);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 1U;
    t9 = t8;
    t10 = (t3 + 4);
    t11 = *((unsigned int *)t3);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t2, 0, 0);
    t16 = (t0 + 3716);
    *((int *)t16) = 1;

LAB1:    return;
}

static void Cont_305_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;

LAB0:    t1 = (t0 + 3352U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(305, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3876);
    t4 = (t3 + 32U);
    t5 = *((char **)t4);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 1U;
    t9 = t8;
    t10 = (t2 + 4);
    t11 = *((unsigned int *)t2);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t3, 0, 0);

LAB1:    return;
}

static void Cont_306_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;

LAB0:    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(306, ng0);
    t2 = (t0 + 784U);
    t3 = *((char **)t2);
    t2 = (t0 + 3912);
    t4 = (t2 + 32U);
    t5 = *((char **)t4);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 1U;
    t9 = t8;
    t10 = (t3 + 4);
    t11 = *((unsigned int *)t3);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t2, 0, 0);
    t16 = (t0 + 3724);
    *((int *)t16) = 1;

LAB1:    return;
}


extern void work_m_00000000002304470439_2872374404_init()
{
	static char *pe[] = {(void *)Cont_64_0,(void *)Initial_252_1,(void *)Always_262_2,(void *)Always_286_3,(void *)Cont_303_4,(void *)Cont_304_5,(void *)Cont_305_6,(void *)Cont_306_7};
	xsi_register_didat("work_m_00000000002304470439_2872374404", "isim/amber-test.exe.sim/work/m_00000000002304470439_2872374404.didat");
	xsi_register_executes(pe);
}
