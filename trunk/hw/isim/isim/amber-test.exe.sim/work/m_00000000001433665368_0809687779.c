/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/lib/generic_sram_line_en.v";
static int ng1[] = {0, 0};
static int ng2[] = {2, 0};
static unsigned int ng3[] = {8U, 0U};
static unsigned int ng4[] = {0U, 0U};
static int ng5[] = {1, 0};



static void Initial_62_0(char *t0)
{
    char t6[8];
    char t7[8];
    char t16[8];
    char t17[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t15;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    unsigned int t28;
    int t29;
    char *t30;
    unsigned int t31;
    int t32;
    int t33;
    unsigned int t34;
    unsigned int t35;
    int t36;
    int t37;

LAB0:    xsi_set_current_line(63, ng0);

LAB2:    xsi_set_current_line(64, ng0);
    xsi_set_current_line(64, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 1528);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 32);

LAB3:    t1 = (t0 + 1528);
    t2 = (t1 + 36U);
    t3 = *((char **)t2);
    t4 = ((char*)((ng2)));
    t5 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_signed_power(t6, 32, t4, 32, t5, 32, 0);
    memset(t7, 0, 8);
    xsi_vlog_signed_less(t7, 32, t3, 32, t6, 32);
    t8 = (t7 + 4);
    t9 = *((unsigned int *)t8);
    t10 = (~(t9));
    t11 = *((unsigned int *)t7);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB4;

LAB5:
LAB1:    return;
LAB4:    xsi_set_current_line(65, ng0);
    t14 = ((char*)((ng4)));
    t15 = (t0 + 1436);
    t18 = (t0 + 1436);
    t19 = (t18 + 44U);
    t20 = *((char **)t19);
    t21 = (t0 + 1436);
    t22 = (t21 + 40U);
    t23 = *((char **)t22);
    t24 = (t0 + 1528);
    t25 = (t24 + 36U);
    t26 = *((char **)t25);
    xsi_vlog_generic_convert_array_indices(t16, t17, t20, t23, 2, 1, t26, 32, 1);
    t27 = (t16 + 4);
    t28 = *((unsigned int *)t27);
    t29 = (!(t28));
    t30 = (t17 + 4);
    t31 = *((unsigned int *)t30);
    t32 = (!(t31));
    t33 = (t29 && t32);
    if (t33 == 1)
        goto LAB6;

LAB7:    xsi_set_current_line(64, ng0);
    t1 = (t0 + 1528);
    t2 = (t1 + 36U);
    t3 = *((char **)t2);
    t4 = ((char*)((ng5)));
    memset(t6, 0, 8);
    xsi_vlog_signed_add(t6, 32, t3, 32, t4, 32);
    t5 = (t0 + 1528);
    xsi_vlogvar_assign_value(t5, t6, 0, 0, 32);
    goto LAB3;

LAB6:    t34 = *((unsigned int *)t16);
    t35 = *((unsigned int *)t17);
    t36 = (t34 - t35);
    t37 = (t36 + 1);
    xsi_vlogvar_wait_assign_value(t15, t14, 0, *((unsigned int *)t17), t37, 0LL);
    goto LAB7;

}

static void Always_71_1(char *t0)
{
    char t4[8];
    char t5[8];
    char t26[8];
    char *t1;
    char *t2;
    char *t3;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    int t35;
    int t36;
    int t37;
    int t38;
    int t39;

LAB0:    t1 = (t0 + 2176U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 2356);
    *((int *)t2) = 1;
    t3 = (t0 + 2200);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(72, ng0);

LAB5:    xsi_set_current_line(74, ng0);
    t6 = (t0 + 1024U);
    t7 = *((char **)t6);
    memset(t5, 0, 8);
    t6 = (t7 + 4);
    t8 = *((unsigned int *)t6);
    t9 = (~(t8));
    t10 = *((unsigned int *)t7);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB6;

LAB7:    if (*((unsigned int *)t6) != 0)
        goto LAB8;

LAB9:    t14 = (t5 + 4);
    t15 = *((unsigned int *)t5);
    t16 = *((unsigned int *)t14);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB10;

LAB11:    t19 = *((unsigned int *)t5);
    t20 = (~(t19));
    t21 = *((unsigned int *)t14);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t14) > 0)
        goto LAB14;

LAB15:    if (*((unsigned int *)t5) > 0)
        goto LAB16;

LAB17:    memcpy(t4, t26, 8);

LAB18:    t33 = (t0 + 1344);
    xsi_vlogvar_wait_assign_value(t33, t4, 0, 0, 21, 0LL);
    xsi_set_current_line(77, ng0);
    t2 = (t0 + 1024U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t8 = *((unsigned int *)t2);
    t9 = (~(t8));
    t10 = *((unsigned int *)t3);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB19;

LAB20:
LAB21:    goto LAB2;

LAB6:    *((unsigned int *)t5) = 1;
    goto LAB9;

LAB8:    t13 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t13) = 1;
    goto LAB9;

LAB10:    t18 = ((char*)((ng4)));
    goto LAB11;

LAB12:    t23 = (t0 + 1436);
    t24 = (t23 + 36U);
    t25 = *((char **)t24);
    t27 = (t0 + 1436);
    t28 = (t27 + 44U);
    t29 = *((char **)t28);
    t30 = (t0 + 1436);
    t31 = (t30 + 40U);
    t32 = *((char **)t31);
    t33 = (t0 + 1116U);
    t34 = *((char **)t33);
    xsi_vlog_generic_get_array_select_value(t26, 21, t25, t29, t32, 2, 1, t34, 8, 2);
    goto LAB13;

LAB14:    xsi_vlog_unsigned_bit_combine(t4, 21, t18, 21, t26, 21);
    goto LAB18;

LAB16:    memcpy(t4, t18, 8);
    goto LAB18;

LAB19:    xsi_set_current_line(78, ng0);
    t6 = (t0 + 932U);
    t7 = *((char **)t6);
    t6 = (t0 + 1436);
    t13 = (t0 + 1436);
    t14 = (t13 + 44U);
    t18 = *((char **)t14);
    t23 = (t0 + 1436);
    t24 = (t23 + 40U);
    t25 = *((char **)t24);
    t27 = (t0 + 1116U);
    t28 = *((char **)t27);
    xsi_vlog_generic_convert_array_indices(t4, t5, t18, t25, 2, 1, t28, 8, 2);
    t27 = (t4 + 4);
    t15 = *((unsigned int *)t27);
    t35 = (!(t15));
    t29 = (t5 + 4);
    t16 = *((unsigned int *)t29);
    t36 = (!(t16));
    t37 = (t35 && t36);
    if (t37 == 1)
        goto LAB22;

LAB23:    goto LAB21;

LAB22:    t17 = *((unsigned int *)t4);
    t19 = *((unsigned int *)t5);
    t38 = (t17 - t19);
    t39 = (t38 + 1);
    xsi_vlogvar_wait_assign_value(t6, t7, 0, *((unsigned int *)t5), t39, 0LL);
    goto LAB23;

}


extern void work_m_00000000001433665368_0809687779_init()
{
	static char *pe[] = {(void *)Initial_62_0,(void *)Always_71_1};
	xsi_register_didat("work_m_00000000001433665368_0809687779", "isim/amber-test.exe.sim/work/m_00000000001433665368_0809687779.didat");
	xsi_register_executes(pe);
}
