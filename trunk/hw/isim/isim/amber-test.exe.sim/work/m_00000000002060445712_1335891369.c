/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/ethmac/eth_shiftreg.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1U, 0U};
static unsigned int ng3[] = {2U, 0U};
static unsigned int ng4[] = {4U, 0U};
static unsigned int ng5[] = {8U, 0U};
static int ng6[] = {7, 0};
static int ng7[] = {0, 0};
static int ng8[] = {15, 0};
static int ng9[] = {8, 0};



static void Always_107_0(char *t0)
{
    char t13[8];
    char t24[8];
    char t36[8];
    char t37[8];
    char t49[8];
    char t69[8];
    char t70[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t25;
    char *t26;
    char *t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    char *t34;
    int t35;
    char *t38;
    char *t39;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    char *t47;
    char *t48;
    char *t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    char *t56;
    char *t57;
    char *t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    char *t67;
    char *t68;
    int t71;
    int t72;
    int t73;
    int t74;
    int t75;
    int t76;
    int t77;

LAB0:    t1 = (t0 + 2524U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(107, ng0);
    t2 = (t0 + 2840);
    *((int *)t2) = 1;
    t3 = (t0 + 2548);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(108, ng0);

LAB5:    xsi_set_current_line(109, ng0);
    t4 = (t0 + 772U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:    xsi_set_current_line(116, ng0);

LAB10:    xsi_set_current_line(117, ng0);
    t2 = (t0 + 864U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t3);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB11;

LAB12:
LAB13:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(110, ng0);

LAB9:    xsi_set_current_line(111, ng0);
    t11 = ((char*)((ng1)));
    t12 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t12, t11, 0, 0, 8, 1000LL);
    xsi_set_current_line(112, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 1828);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 16, 1000LL);
    xsi_set_current_line(113, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 1920);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 1000LL);
    goto LAB8;

LAB11:    xsi_set_current_line(118, ng0);

LAB14:    xsi_set_current_line(119, ng0);
    t4 = (t0 + 1416U);
    t5 = *((char **)t4);
    memset(t13, 0, 8);
    t4 = (t5 + 4);
    t14 = *((unsigned int *)t4);
    t15 = (~(t14));
    t16 = *((unsigned int *)t5);
    t17 = (t16 & t15);
    t18 = (t17 & 15U);
    if (t18 != 0)
        goto LAB15;

LAB16:    if (*((unsigned int *)t4) != 0)
        goto LAB17;

LAB18:    t12 = (t13 + 4);
    t19 = *((unsigned int *)t12);
    t20 = (~(t19));
    t21 = *((unsigned int *)t13);
    t22 = (t21 & t20);
    t23 = (t22 != 0);
    if (t23 > 0)
        goto LAB19;

LAB20:    xsi_set_current_line(129, ng0);

LAB39:    xsi_set_current_line(130, ng0);
    t2 = (t0 + 956U);
    t3 = *((char **)t2);
    t2 = (t0 + 2012);
    t4 = (t2 + 36U);
    t5 = *((char **)t4);
    memset(t36, 0, 8);
    t11 = (t36 + 4);
    t12 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 0);
    *((unsigned int *)t36) = t7;
    t8 = *((unsigned int *)t12);
    t9 = (t8 >> 0);
    *((unsigned int *)t11) = t9;
    t10 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t10 & 127U);
    t14 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t14 & 127U);
    xsi_vlogtype_concat(t13, 8, 8, 2U, t36, 7, t3, 1);
    t25 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t25, t13, 0, 0, 8, 1000LL);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 1508U);
    t3 = *((char **)t2);
    memset(t13, 0, 8);
    t2 = (t13 + 4);
    t4 = (t3 + 4);
    t6 = *((unsigned int *)t3);
    t7 = (t6 >> 0);
    t8 = (t7 & 1);
    *((unsigned int *)t13) = t8;
    t9 = *((unsigned int *)t4);
    t10 = (t9 >> 0);
    t14 = (t10 & 1);
    *((unsigned int *)t2) = t14;
    t5 = (t13 + 4);
    t15 = *((unsigned int *)t5);
    t16 = (~(t15));
    t17 = *((unsigned int *)t13);
    t18 = (t17 & t16);
    t19 = (t18 != 0);
    if (t19 > 0)
        goto LAB40;

LAB41:    xsi_set_current_line(138, ng0);

LAB59:    xsi_set_current_line(139, ng0);
    t2 = (t0 + 1508U);
    t3 = *((char **)t2);
    memset(t13, 0, 8);
    t2 = (t13 + 4);
    t4 = (t3 + 4);
    t6 = *((unsigned int *)t3);
    t7 = (t6 >> 1);
    t8 = (t7 & 1);
    *((unsigned int *)t13) = t8;
    t9 = *((unsigned int *)t4);
    t10 = (t9 >> 1);
    t14 = (t10 & 1);
    *((unsigned int *)t2) = t14;
    t5 = (t13 + 4);
    t15 = *((unsigned int *)t5);
    t16 = (~(t15));
    t17 = *((unsigned int *)t13);
    t18 = (t17 & t16);
    t19 = (t18 != 0);
    if (t19 > 0)
        goto LAB60;

LAB61:
LAB62:
LAB42:
LAB21:    goto LAB13;

LAB15:    *((unsigned int *)t13) = 1;
    goto LAB18;

LAB17:    t11 = (t13 + 4);
    *((unsigned int *)t13) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB18;

LAB19:    xsi_set_current_line(120, ng0);

LAB22:    xsi_set_current_line(121, ng0);
    t25 = (t0 + 1416U);
    t26 = *((char **)t25);
    memset(t24, 0, 8);
    t25 = (t24 + 4);
    t27 = (t26 + 4);
    t28 = *((unsigned int *)t26);
    t29 = (t28 >> 0);
    *((unsigned int *)t24) = t29;
    t30 = *((unsigned int *)t27);
    t31 = (t30 >> 0);
    *((unsigned int *)t25) = t31;
    t32 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t32 & 15U);
    t33 = *((unsigned int *)t25);
    *((unsigned int *)t25) = (t33 & 15U);

LAB23:    t34 = ((char*)((ng2)));
    t35 = xsi_vlog_unsigned_case_compare(t24, 4, t34, 4);
    if (t35 == 1)
        goto LAB24;

LAB25:    t2 = ((char*)((ng3)));
    t35 = xsi_vlog_unsigned_case_compare(t24, 4, t2, 4);
    if (t35 == 1)
        goto LAB26;

LAB27:    t2 = ((char*)((ng4)));
    t35 = xsi_vlog_unsigned_case_compare(t24, 4, t2, 4);
    if (t35 == 1)
        goto LAB28;

LAB29:    t2 = ((char*)((ng5)));
    t35 = xsi_vlog_unsigned_case_compare(t24, 4, t2, 4);
    if (t35 == 1)
        goto LAB30;

LAB31:
LAB32:    goto LAB21;

LAB24:    xsi_set_current_line(122, ng0);
    t38 = (t0 + 1048U);
    t39 = *((char **)t38);
    memset(t37, 0, 8);
    t38 = (t37 + 4);
    t40 = (t39 + 4);
    t41 = *((unsigned int *)t39);
    t42 = (t41 >> 1);
    *((unsigned int *)t37) = t42;
    t43 = *((unsigned int *)t40);
    t44 = (t43 >> 1);
    *((unsigned int *)t38) = t44;
    t45 = *((unsigned int *)t37);
    *((unsigned int *)t37) = (t45 & 15U);
    t46 = *((unsigned int *)t38);
    *((unsigned int *)t38) = (t46 & 15U);
    t47 = (t0 + 1324U);
    t48 = *((char **)t47);
    t47 = (t0 + 1324U);
    t50 = *((char **)t47);
    memset(t49, 0, 8);
    t47 = (t50 + 4);
    t51 = *((unsigned int *)t47);
    t52 = (~(t51));
    t53 = *((unsigned int *)t50);
    t54 = (t53 & t52);
    t55 = (t54 & 1U);
    if (t55 != 0)
        goto LAB36;

LAB34:    if (*((unsigned int *)t47) == 0)
        goto LAB33;

LAB35:    t56 = (t49 + 4);
    *((unsigned int *)t49) = 1;
    *((unsigned int *)t56) = 1;

LAB36:    t57 = (t49 + 4);
    t58 = (t50 + 4);
    t59 = *((unsigned int *)t50);
    t60 = (~(t59));
    *((unsigned int *)t49) = t60;
    *((unsigned int *)t57) = 0;
    if (*((unsigned int *)t58) != 0)
        goto LAB38;

LAB37:    t65 = *((unsigned int *)t49);
    *((unsigned int *)t49) = (t65 & 1U);
    t66 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t66 & 1U);
    t67 = ((char*)((ng2)));
    xsi_vlogtype_concat(t36, 8, 8, 4U, t67, 2, t49, 1, t48, 1, t37, 4);
    t68 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t68, t36, 0, 0, 8, 1000LL);
    goto LAB32;

LAB26:    xsi_set_current_line(123, ng0);
    t3 = ((char*)((ng3)));
    t4 = (t0 + 1140U);
    t5 = *((char **)t4);
    memset(t36, 0, 8);
    t4 = (t36 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 0);
    *((unsigned int *)t36) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 0);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t10 & 31U);
    t14 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t14 & 31U);
    t12 = (t0 + 1048U);
    t25 = *((char **)t12);
    memset(t37, 0, 8);
    t12 = (t37 + 4);
    t26 = (t25 + 4);
    t15 = *((unsigned int *)t25);
    t16 = (t15 >> 0);
    t17 = (t16 & 1);
    *((unsigned int *)t37) = t17;
    t18 = *((unsigned int *)t26);
    t19 = (t18 >> 0);
    t20 = (t19 & 1);
    *((unsigned int *)t12) = t20;
    xsi_vlogtype_concat(t13, 8, 8, 3U, t37, 1, t36, 5, t3, 2);
    t27 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t27, t13, 0, 0, 8, 1000LL);
    goto LAB32;

LAB28:    xsi_set_current_line(124, ng0);
    t3 = (t0 + 1232U);
    t4 = *((char **)t3);
    memset(t13, 0, 8);
    t3 = (t13 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 8);
    *((unsigned int *)t13) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 8);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t10 & 255U);
    t14 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t14 & 255U);
    t11 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t11, t13, 0, 0, 8, 1000LL);
    goto LAB32;

LAB30:    xsi_set_current_line(125, ng0);
    t3 = (t0 + 1232U);
    t4 = *((char **)t3);
    memset(t13, 0, 8);
    t3 = (t13 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 0);
    *((unsigned int *)t13) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 0);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t10 & 255U);
    t14 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t14 & 255U);
    t11 = (t0 + 2012);
    xsi_vlogvar_wait_assign_value(t11, t13, 0, 0, 8, 1000LL);
    goto LAB32;

LAB33:    *((unsigned int *)t49) = 1;
    goto LAB36;

LAB38:    t61 = *((unsigned int *)t49);
    t62 = *((unsigned int *)t58);
    *((unsigned int *)t49) = (t61 | t62);
    t63 = *((unsigned int *)t57);
    t64 = *((unsigned int *)t58);
    *((unsigned int *)t57) = (t63 | t64);
    goto LAB37;

LAB40:    xsi_set_current_line(132, ng0);

LAB43:    xsi_set_current_line(133, ng0);
    t11 = (t0 + 956U);
    t12 = *((char **)t11);
    t11 = (t0 + 2012);
    t25 = (t11 + 36U);
    t26 = *((char **)t25);
    memset(t37, 0, 8);
    t27 = (t37 + 4);
    t34 = (t26 + 4);
    t20 = *((unsigned int *)t26);
    t21 = (t20 >> 0);
    *((unsigned int *)t37) = t21;
    t22 = *((unsigned int *)t34);
    t23 = (t22 >> 0);
    *((unsigned int *)t27) = t23;
    t28 = *((unsigned int *)t37);
    *((unsigned int *)t37) = (t28 & 127U);
    t29 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t29 & 127U);
    xsi_vlogtype_concat(t36, 8, 8, 2U, t37, 7, t12, 1);
    t38 = (t0 + 1828);
    t39 = (t0 + 1828);
    t40 = (t39 + 44U);
    t47 = *((char **)t40);
    t48 = ((char*)((ng6)));
    t50 = ((char*)((ng7)));
    xsi_vlog_convert_partindices(t49, t69, t70, ((int*)(t47)), 2, t48, 32, 1, t50, 32, 1);
    t56 = (t49 + 4);
    t30 = *((unsigned int *)t56);
    t35 = (!(t30));
    t57 = (t69 + 4);
    t31 = *((unsigned int *)t57);
    t71 = (!(t31));
    t72 = (t35 && t71);
    t58 = (t70 + 4);
    t32 = *((unsigned int *)t58);
    t73 = (!(t32));
    t74 = (t72 && t73);
    if (t74 == 1)
        goto LAB44;

LAB45:    xsi_set_current_line(134, ng0);
    t2 = (t0 + 1140U);
    t3 = *((char **)t2);
    t2 = ((char*)((ng2)));
    memset(t13, 0, 8);
    t4 = (t3 + 4);
    t5 = (t2 + 4);
    t6 = *((unsigned int *)t3);
    t7 = *((unsigned int *)t2);
    t8 = (t6 ^ t7);
    t9 = *((unsigned int *)t4);
    t10 = *((unsigned int *)t5);
    t14 = (t9 ^ t10);
    t15 = (t8 | t14);
    t16 = *((unsigned int *)t4);
    t17 = *((unsigned int *)t5);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB49;

LAB46:    if (t18 != 0)
        goto LAB48;

LAB47:    *((unsigned int *)t13) = 1;

LAB49:    t12 = (t13 + 4);
    t21 = *((unsigned int *)t12);
    t22 = (~(t21));
    t23 = *((unsigned int *)t13);
    t28 = (t23 & t22);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB50;

LAB51:
LAB52:    goto LAB42;

LAB44:    t33 = *((unsigned int *)t70);
    t75 = (t33 + 0);
    t41 = *((unsigned int *)t49);
    t42 = *((unsigned int *)t69);
    t76 = (t41 - t42);
    t77 = (t76 + 1);
    xsi_vlogvar_wait_assign_value(t38, t36, t75, *((unsigned int *)t69), t77, 1000LL);
    goto LAB45;

LAB48:    t11 = (t13 + 4);
    *((unsigned int *)t13) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB49;

LAB50:    xsi_set_current_line(135, ng0);
    t25 = (t0 + 2012);
    t26 = (t25 + 36U);
    t27 = *((char **)t26);
    memset(t37, 0, 8);
    t34 = (t37 + 4);
    t38 = (t27 + 4);
    t30 = *((unsigned int *)t27);
    t31 = (t30 >> 1);
    t32 = (t31 & 1);
    *((unsigned int *)t37) = t32;
    t33 = *((unsigned int *)t38);
    t41 = (t33 >> 1);
    t42 = (t41 & 1);
    *((unsigned int *)t34) = t42;
    memset(t36, 0, 8);
    t39 = (t37 + 4);
    t43 = *((unsigned int *)t39);
    t44 = (~(t43));
    t45 = *((unsigned int *)t37);
    t46 = (t45 & t44);
    t51 = (t46 & 1U);
    if (t51 != 0)
        goto LAB56;

LAB54:    if (*((unsigned int *)t39) == 0)
        goto LAB53;

LAB55:    t40 = (t36 + 4);
    *((unsigned int *)t36) = 1;
    *((unsigned int *)t40) = 1;

LAB56:    t47 = (t36 + 4);
    t48 = (t37 + 4);
    t52 = *((unsigned int *)t37);
    t53 = (~(t52));
    *((unsigned int *)t36) = t53;
    *((unsigned int *)t47) = 0;
    if (*((unsigned int *)t48) != 0)
        goto LAB58;

LAB57:    t61 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t61 & 1U);
    t62 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t62 & 1U);
    t50 = (t0 + 1920);
    xsi_vlogvar_wait_assign_value(t50, t36, 0, 0, 1, 1000LL);
    goto LAB52;

LAB53:    *((unsigned int *)t36) = 1;
    goto LAB56;

LAB58:    t54 = *((unsigned int *)t36);
    t55 = *((unsigned int *)t48);
    *((unsigned int *)t36) = (t54 | t55);
    t59 = *((unsigned int *)t47);
    t60 = *((unsigned int *)t48);
    *((unsigned int *)t47) = (t59 | t60);
    goto LAB57;

LAB60:    xsi_set_current_line(140, ng0);
    t11 = (t0 + 956U);
    t12 = *((char **)t11);
    t11 = (t0 + 2012);
    t25 = (t11 + 36U);
    t26 = *((char **)t25);
    memset(t37, 0, 8);
    t27 = (t37 + 4);
    t34 = (t26 + 4);
    t20 = *((unsigned int *)t26);
    t21 = (t20 >> 0);
    *((unsigned int *)t37) = t21;
    t22 = *((unsigned int *)t34);
    t23 = (t22 >> 0);
    *((unsigned int *)t27) = t23;
    t28 = *((unsigned int *)t37);
    *((unsigned int *)t37) = (t28 & 127U);
    t29 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t29 & 127U);
    xsi_vlogtype_concat(t36, 8, 8, 2U, t37, 7, t12, 1);
    t38 = (t0 + 1828);
    t39 = (t0 + 1828);
    t40 = (t39 + 44U);
    t47 = *((char **)t40);
    t48 = ((char*)((ng8)));
    t50 = ((char*)((ng9)));
    xsi_vlog_convert_partindices(t49, t69, t70, ((int*)(t47)), 2, t48, 32, 1, t50, 32, 1);
    t56 = (t49 + 4);
    t30 = *((unsigned int *)t56);
    t35 = (!(t30));
    t57 = (t69 + 4);
    t31 = *((unsigned int *)t57);
    t71 = (!(t31));
    t72 = (t35 && t71);
    t58 = (t70 + 4);
    t32 = *((unsigned int *)t58);
    t73 = (!(t32));
    t74 = (t72 && t73);
    if (t74 == 1)
        goto LAB63;

LAB64:    goto LAB62;

LAB63:    t33 = *((unsigned int *)t70);
    t75 = (t33 + 0);
    t41 = *((unsigned int *)t49);
    t42 = *((unsigned int *)t69);
    t76 = (t41 - t42);
    t77 = (t76 + 1);
    xsi_vlogvar_wait_assign_value(t38, t36, t75, *((unsigned int *)t69), t77, 1000LL);
    goto LAB64;

}

static void Cont_148_1(char *t0)
{
    char t5[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    unsigned int t25;
    unsigned int t26;
    char *t27;

LAB0:    t1 = (t0 + 2660U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(148, ng0);
    t2 = (t0 + 2012);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    memset(t5, 0, 8);
    t6 = (t5 + 4);
    t7 = (t4 + 4);
    t8 = *((unsigned int *)t4);
    t9 = (t8 >> 7);
    t10 = (t9 & 1);
    *((unsigned int *)t5) = t10;
    t11 = *((unsigned int *)t7);
    t12 = (t11 >> 7);
    t13 = (t12 & 1);
    *((unsigned int *)t6) = t13;
    t14 = (t0 + 2892);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    t17 = (t16 + 32U);
    t18 = *((char **)t17);
    memset(t18, 0, 8);
    t19 = 1U;
    t20 = t19;
    t21 = (t5 + 4);
    t22 = *((unsigned int *)t5);
    t19 = (t19 & t22);
    t23 = *((unsigned int *)t21);
    t20 = (t20 & t23);
    t24 = (t18 + 4);
    t25 = *((unsigned int *)t18);
    *((unsigned int *)t18) = (t25 | t19);
    t26 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t26 | t20);
    xsi_driver_vfirst_trans(t14, 0, 0);
    t27 = (t0 + 2848);
    *((int *)t27) = 1;

LAB1:    return;
}


extern void work_m_00000000002060445712_1335891369_init()
{
	static char *pe[] = {(void *)Always_107_0,(void *)Cont_148_1};
	xsi_register_didat("work_m_00000000002060445712_1335891369", "isim/amber-test.exe.sim/work/m_00000000002060445712_1335891369.didat");
	xsi_register_executes(pe);
}
