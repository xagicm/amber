/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/amber23/a23_multiply.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1U, 0U, 0U, 0U};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {2U, 0U};
static unsigned int ng5[] = {0U, 0U, 0U, 0U};
static unsigned int ng6[] = {33U, 0U};
static unsigned int ng7[] = {34U, 0U};
static unsigned int ng8[] = {35U, 0U};
static unsigned int ng9[] = {31U, 0U};



static void Cont_86_0(char *t0)
{
    char t4[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;

LAB0:    t1 = (t0 + 3088U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(86, ng0);
    t2 = (t0 + 968U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t4 + 4);
    t5 = (t3 + 4);
    t6 = *((unsigned int *)t3);
    t7 = (t6 >> 0);
    t8 = (t7 & 1);
    *((unsigned int *)t4) = t8;
    t9 = *((unsigned int *)t5);
    t10 = (t9 >> 0);
    t11 = (t10 & 1);
    *((unsigned int *)t2) = t11;
    t12 = (t0 + 4752);
    t13 = (t12 + 32U);
    t14 = *((char **)t13);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    memset(t16, 0, 8);
    t17 = 1U;
    t18 = t17;
    t19 = (t4 + 4);
    t20 = *((unsigned int *)t4);
    t17 = (t17 & t20);
    t21 = *((unsigned int *)t19);
    t18 = (t18 & t21);
    t22 = (t16 + 4);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 | t17);
    t24 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t24 | t18);
    xsi_driver_vfirst_trans(t12, 0, 0);
    t25 = (t0 + 4628);
    *((int *)t25) = 1;

LAB1:    return;
}

static void Cont_87_1(char *t0)
{
    char t4[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;

LAB0:    t1 = (t0 + 3224U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(87, ng0);
    t2 = (t0 + 968U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t4 + 4);
    t5 = (t3 + 4);
    t6 = *((unsigned int *)t3);
    t7 = (t6 >> 1);
    t8 = (t7 & 1);
    *((unsigned int *)t4) = t8;
    t9 = *((unsigned int *)t5);
    t10 = (t9 >> 1);
    t11 = (t10 & 1);
    *((unsigned int *)t2) = t11;
    t12 = (t0 + 4788);
    t13 = (t12 + 32U);
    t14 = *((char **)t13);
    t15 = (t14 + 32U);
    t16 = *((char **)t15);
    memset(t16, 0, 8);
    t17 = 1U;
    t18 = t17;
    t19 = (t4 + 4);
    t20 = *((unsigned int *)t4);
    t17 = (t17 & t20);
    t21 = *((unsigned int *)t19);
    t18 = (t18 & t21);
    t22 = (t16 + 4);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 | t17);
    t24 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t24 | t18);
    xsi_driver_vfirst_trans(t12, 0, 0);
    t25 = (t0 + 4636);
    *((int *)t25) = 1;

LAB1:    return;
}

static void Cont_89_2(char *t0)
{
    char t3[16];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    t1 = (t0 + 3360U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(89, ng0);
    t2 = (t0 + 784U);
    t4 = *((char **)t2);
    t2 = ((char*)((ng1)));
    xsi_vlogtype_concat(t3, 34, 34, 2U, t2, 2, t4, 32);
    t5 = (t0 + 4824);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    xsi_vlog_bit_copy(t9, 0, t3, 0, 34);
    xsi_driver_vfirst_trans(t5, 0, 33);
    t10 = (t0 + 4644);
    *((int *)t10) = 1;

LAB1:    return;
}

static void Cont_90_3(char *t0)
{
    char t3[16];
    char t4[16];
    char t7[16];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    t1 = (t0 + 3496U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(90, ng0);
    t2 = (t0 + 784U);
    t5 = *((char **)t2);
    t2 = ((char*)((ng1)));
    xsi_vlogtype_concat(t4, 34, 34, 2U, t2, 2, t5, 32);
    xsi_vlogtype_unsigned_bit_neg(t3, 34, t4, 34);
    t6 = ((char*)((ng2)));
    xsi_vlog_unsigned_add(t7, 34, t3, 34, t6, 34);
    t8 = (t0 + 4860);
    t9 = (t8 + 32U);
    t10 = *((char **)t9);
    t11 = (t10 + 32U);
    t12 = *((char **)t11);
    xsi_vlog_bit_copy(t12, 0, t7, 0, 34);
    xsi_driver_vfirst_trans(t8, 0, 33);
    t13 = (t0 + 4652);
    *((int *)t13) = 1;

LAB1:    return;
}

static void Cont_92_4(char *t0)
{
    char t3[16];
    char t4[8];
    char t5[8];
    char t17[8];
    char t50[16];
    char t51[8];
    char t52[8];
    char t64[8];
    char *t1;
    char *t2;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    char *t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    char *t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    char *t39;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    char *t44;
    char *t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    char *t65;
    char *t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    unsigned int t78;
    char *t79;
    char *t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    char *t86;
    char *t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    char *t91;
    char *t92;
    unsigned int t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    char *t97;
    char *t98;
    char *t99;
    char *t100;
    char *t101;
    char *t102;

LAB0:    t1 = (t0 + 3632U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(92, ng0);
    t2 = (t0 + 2392);
    t6 = (t2 + 36U);
    t7 = *((char **)t6);
    memset(t5, 0, 8);
    t8 = (t5 + 4);
    t9 = (t7 + 4);
    t10 = *((unsigned int *)t7);
    t11 = (t10 >> 0);
    *((unsigned int *)t5) = t11;
    t12 = *((unsigned int *)t9);
    t13 = (t12 >> 0);
    *((unsigned int *)t8) = t13;
    t14 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t14 & 3U);
    t15 = *((unsigned int *)t8);
    *((unsigned int *)t8) = (t15 & 3U);
    t16 = ((char*)((ng3)));
    memset(t17, 0, 8);
    t18 = (t5 + 4);
    t19 = (t16 + 4);
    t20 = *((unsigned int *)t5);
    t21 = *((unsigned int *)t16);
    t22 = (t20 ^ t21);
    t23 = *((unsigned int *)t18);
    t24 = *((unsigned int *)t19);
    t25 = (t23 ^ t24);
    t26 = (t22 | t25);
    t27 = *((unsigned int *)t18);
    t28 = *((unsigned int *)t19);
    t29 = (t27 | t28);
    t30 = (~(t29));
    t31 = (t26 & t30);
    if (t31 != 0)
        goto LAB7;

LAB4:    if (t29 != 0)
        goto LAB6;

LAB5:    *((unsigned int *)t17) = 1;

LAB7:    memset(t4, 0, 8);
    t33 = (t17 + 4);
    t34 = *((unsigned int *)t33);
    t35 = (~(t34));
    t36 = *((unsigned int *)t17);
    t37 = (t36 & t35);
    t38 = (t37 & 1U);
    if (t38 != 0)
        goto LAB8;

LAB9:    if (*((unsigned int *)t33) != 0)
        goto LAB10;

LAB11:    t40 = (t4 + 4);
    t41 = *((unsigned int *)t4);
    t42 = *((unsigned int *)t40);
    t43 = (t41 || t42);
    if (t43 > 0)
        goto LAB12;

LAB13:    t46 = *((unsigned int *)t4);
    t47 = (~(t46));
    t48 = *((unsigned int *)t40);
    t49 = (t47 || t48);
    if (t49 > 0)
        goto LAB14;

LAB15:    if (*((unsigned int *)t40) > 0)
        goto LAB16;

LAB17:    if (*((unsigned int *)t4) > 0)
        goto LAB18;

LAB19:    memcpy(t3, t50, 16);

LAB20:    t97 = (t0 + 4896);
    t98 = (t97 + 32U);
    t99 = *((char **)t98);
    t100 = (t99 + 32U);
    t101 = *((char **)t100);
    xsi_vlog_bit_copy(t101, 0, t3, 0, 34);
    xsi_driver_vfirst_trans(t97, 0, 33);
    t102 = (t0 + 4660);
    *((int *)t102) = 1;

LAB1:    return;
LAB6:    t32 = (t17 + 4);
    *((unsigned int *)t17) = 1;
    *((unsigned int *)t32) = 1;
    goto LAB7;

LAB8:    *((unsigned int *)t4) = 1;
    goto LAB11;

LAB10:    t39 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB11;

LAB12:    t44 = (t0 + 1520U);
    t45 = *((char **)t44);
    goto LAB13;

LAB14:    t44 = (t0 + 2392);
    t53 = (t44 + 36U);
    t54 = *((char **)t53);
    memset(t52, 0, 8);
    t55 = (t52 + 4);
    t56 = (t54 + 4);
    t57 = *((unsigned int *)t54);
    t58 = (t57 >> 0);
    *((unsigned int *)t52) = t58;
    t59 = *((unsigned int *)t56);
    t60 = (t59 >> 0);
    *((unsigned int *)t55) = t60;
    t61 = *((unsigned int *)t52);
    *((unsigned int *)t52) = (t61 & 3U);
    t62 = *((unsigned int *)t55);
    *((unsigned int *)t55) = (t62 & 3U);
    t63 = ((char*)((ng4)));
    memset(t64, 0, 8);
    t65 = (t52 + 4);
    t66 = (t63 + 4);
    t67 = *((unsigned int *)t52);
    t68 = *((unsigned int *)t63);
    t69 = (t67 ^ t68);
    t70 = *((unsigned int *)t65);
    t71 = *((unsigned int *)t66);
    t72 = (t70 ^ t71);
    t73 = (t69 | t72);
    t74 = *((unsigned int *)t65);
    t75 = *((unsigned int *)t66);
    t76 = (t74 | t75);
    t77 = (~(t76));
    t78 = (t73 & t77);
    if (t78 != 0)
        goto LAB24;

LAB21:    if (t76 != 0)
        goto LAB23;

LAB22:    *((unsigned int *)t64) = 1;

LAB24:    memset(t51, 0, 8);
    t80 = (t64 + 4);
    t81 = *((unsigned int *)t80);
    t82 = (~(t81));
    t83 = *((unsigned int *)t64);
    t84 = (t83 & t82);
    t85 = (t84 & 1U);
    if (t85 != 0)
        goto LAB25;

LAB26:    if (*((unsigned int *)t80) != 0)
        goto LAB27;

LAB28:    t87 = (t51 + 4);
    t88 = *((unsigned int *)t51);
    t89 = *((unsigned int *)t87);
    t90 = (t88 || t89);
    if (t90 > 0)
        goto LAB29;

LAB30:    t93 = *((unsigned int *)t51);
    t94 = (~(t93));
    t95 = *((unsigned int *)t87);
    t96 = (t94 || t95);
    if (t96 > 0)
        goto LAB31;

LAB32:    if (*((unsigned int *)t87) > 0)
        goto LAB33;

LAB34:    if (*((unsigned int *)t51) > 0)
        goto LAB35;

LAB36:    memcpy(t50, t91, 16);

LAB37:    goto LAB15;

LAB16:    xsi_vlog_unsigned_bit_combine(t3, 34, t45, 34, t50, 34);
    goto LAB20;

LAB18:    memcpy(t3, t45, 16);
    goto LAB20;

LAB23:    t79 = (t64 + 4);
    *((unsigned int *)t64) = 1;
    *((unsigned int *)t79) = 1;
    goto LAB24;

LAB25:    *((unsigned int *)t51) = 1;
    goto LAB28;

LAB27:    t86 = (t51 + 4);
    *((unsigned int *)t51) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB28;

LAB29:    t91 = (t0 + 1612U);
    t92 = *((char **)t91);
    goto LAB30;

LAB31:    t91 = ((char*)((ng5)));
    goto LAB32;

LAB33:    xsi_vlog_unsigned_bit_combine(t50, 34, t92, 34, t91, 34);
    goto LAB37;

LAB35:    memcpy(t50, t92, 16);
    goto LAB37;

}

static void Cont_141_5(char *t0)
{
    char t3[16];
    char t8[16];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    t1 = (t0 + 3768U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(141, ng0);
    t2 = (t0 + 2392);
    t4 = (t2 + 36U);
    t5 = *((char **)t4);
    xsi_vlog_get_part_select_value(t3, 34, t5, 67, 34);
    t6 = (t0 + 1796U);
    t7 = *((char **)t6);
    xsi_vlog_unsigned_add(t8, 34, t3, 34, t7, 34);
    t6 = (t0 + 4932);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t11 = (t10 + 32U);
    t12 = *((char **)t11);
    xsi_vlog_bit_copy(t12, 0, t8, 0, 34);
    xsi_driver_vfirst_trans(t6, 0, 33);
    t13 = (t0 + 4668);
    *((int *)t13) = 1;

LAB1:    return;
}

static void Cont_146_6(char *t0)
{
    char t3[16];
    char t4[8];
    char t24[16];
    char t27[16];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t25;
    char *t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;

LAB0:    t1 = (t0 + 3904U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(146, ng0);
    t2 = (t0 + 2392);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t4, 0, 8);
    t7 = (t4 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 1);
    *((unsigned int *)t4) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 1);
    *((unsigned int *)t7) = t12;
    t13 = (t6 + 8);
    t14 = (t6 + 12);
    t15 = *((unsigned int *)t13);
    t16 = (t15 << 31);
    t17 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t17 | t16);
    t18 = *((unsigned int *)t14);
    t19 = (t18 << 31);
    t20 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t20 | t19);
    t21 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t21 & 4294967295U);
    t22 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t22 & 4294967295U);
    t23 = ((char*)((ng1)));
    xsi_vlogtype_concat(t3, 33, 33, 2U, t23, 1, t4, 32);
    t25 = (t0 + 784U);
    t26 = *((char **)t25);
    t25 = ((char*)((ng1)));
    xsi_vlogtype_concat(t24, 33, 33, 2U, t25, 1, t26, 32);
    xsi_vlog_unsigned_add(t27, 33, t3, 33, t24, 33);
    t28 = (t0 + 4968);
    t29 = (t28 + 32U);
    t30 = *((char **)t29);
    t31 = (t30 + 32U);
    t32 = *((char **)t31);
    xsi_vlog_bit_copy(t32, 0, t27, 0, 33);
    xsi_driver_vfirst_trans(t28, 0, 32);
    t33 = (t0 + 4676);
    *((int *)t33) = 1;

LAB1:    return;
}

static void Always_151_7(char *t0)
{
    char t8[8];
    char t9[8];
    char t24[8];
    char t43[8];
    char t54[24];
    char t55[16];
    char t84[8];
    char t94[8];
    char t116[8];
    char t120[8];
    char t132[8];
    char t139[8];
    char t171[8];
    char t179[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    int t69;
    int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    char *t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    char *t83;
    char *t85;
    char *t86;
    char *t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    unsigned int t91;
    unsigned int t92;
    unsigned int t93;
    char *t95;
    char *t96;
    char *t97;
    char *t98;
    char *t99;
    char *t100;
    unsigned int t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    char *t105;
    char *t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    unsigned int t110;
    unsigned int t111;
    unsigned int t112;
    unsigned int t113;
    unsigned int t114;
    char *t115;
    unsigned int t117;
    unsigned int t118;
    unsigned int t119;
    unsigned int t121;
    unsigned int t122;
    unsigned int t123;
    unsigned int t124;
    unsigned int t125;
    char *t126;
    unsigned int t127;
    unsigned int t128;
    unsigned int t129;
    char *t130;
    char *t131;
    unsigned int t133;
    unsigned int t134;
    unsigned int t135;
    unsigned int t136;
    unsigned int t137;
    char *t138;
    unsigned int t140;
    unsigned int t141;
    unsigned int t142;
    char *t143;
    char *t144;
    char *t145;
    unsigned int t146;
    unsigned int t147;
    unsigned int t148;
    unsigned int t149;
    unsigned int t150;
    unsigned int t151;
    unsigned int t152;
    char *t153;
    char *t154;
    unsigned int t155;
    unsigned int t156;
    unsigned int t157;
    unsigned int t158;
    unsigned int t159;
    unsigned int t160;
    unsigned int t161;
    unsigned int t162;
    int t163;
    int t164;
    unsigned int t165;
    unsigned int t166;
    unsigned int t167;
    unsigned int t168;
    unsigned int t169;
    unsigned int t170;
    char *t172;
    unsigned int t173;
    unsigned int t174;
    unsigned int t175;
    unsigned int t176;
    unsigned int t177;
    char *t178;
    unsigned int t180;
    unsigned int t181;
    unsigned int t182;
    char *t183;
    char *t184;
    char *t185;
    unsigned int t186;
    unsigned int t187;
    unsigned int t188;
    unsigned int t189;
    unsigned int t190;
    unsigned int t191;
    unsigned int t192;
    char *t193;
    char *t194;
    unsigned int t195;
    unsigned int t196;
    unsigned int t197;
    int t198;
    unsigned int t199;
    unsigned int t200;
    unsigned int t201;
    int t202;
    unsigned int t203;
    unsigned int t204;
    unsigned int t205;
    unsigned int t206;
    char *t207;
    unsigned int t208;
    unsigned int t209;
    unsigned int t210;
    unsigned int t211;
    unsigned int t212;
    char *t213;
    char *t214;

LAB0:    t1 = (t0 + 4040U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(151, ng0);
    t2 = (t0 + 4684);
    *((int *)t2) = 1;
    t3 = (t0 + 4064);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(152, ng0);

LAB5:    xsi_set_current_line(154, ng0);
    t4 = (t0 + 2208);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    t7 = (t0 + 2300);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 6);
    xsi_set_current_line(155, ng0);
    t2 = (t0 + 2392);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 2484);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 68);
    xsi_set_current_line(161, ng0);
    t2 = (t0 + 2392);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    memset(t9, 0, 8);
    t5 = (t9 + 4);
    t6 = (t4 + 4);
    t10 = *((unsigned int *)t4);
    t11 = (t10 >> 1);
    *((unsigned int *)t9) = t11;
    t12 = *((unsigned int *)t6);
    t13 = (t12 >> 1);
    *((unsigned int *)t5) = t13;
    t7 = (t4 + 8);
    t14 = (t4 + 12);
    t15 = *((unsigned int *)t7);
    t16 = (t15 << 31);
    t17 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t17 | t16);
    t18 = *((unsigned int *)t14);
    t19 = (t18 << 31);
    t20 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t20 | t19);
    t21 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t21 & 4294967295U);
    t22 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t22 & 4294967295U);
    t23 = ((char*)((ng1)));
    memset(t24, 0, 8);
    t25 = (t9 + 4);
    t26 = (t23 + 4);
    t27 = *((unsigned int *)t9);
    t28 = *((unsigned int *)t23);
    t29 = (t27 ^ t28);
    t30 = *((unsigned int *)t25);
    t31 = *((unsigned int *)t26);
    t32 = (t30 ^ t31);
    t33 = (t29 | t32);
    t34 = *((unsigned int *)t25);
    t35 = *((unsigned int *)t26);
    t36 = (t34 | t35);
    t37 = (~(t36));
    t38 = (t33 & t37);
    if (t38 != 0)
        goto LAB9;

LAB6:    if (t36 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t24) = 1;

LAB9:    t40 = (t0 + 2392);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    memset(t43, 0, 8);
    t44 = (t43 + 4);
    t45 = (t42 + 8);
    t46 = (t42 + 12);
    t47 = *((unsigned int *)t45);
    t48 = (t47 >> 0);
    t49 = (t48 & 1);
    *((unsigned int *)t43) = t49;
    t50 = *((unsigned int *)t46);
    t51 = (t50 >> 0);
    t52 = (t51 & 1);
    *((unsigned int *)t44) = t52;
    xsi_vlogtype_concat(t8, 2, 2, 2U, t43, 1, t24, 1);
    t53 = (t0 + 2576);
    xsi_vlogvar_assign_value(t53, t8, 0, 0, 2);
    xsi_set_current_line(164, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng1)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t10 = *((unsigned int *)t4);
    t11 = *((unsigned int *)t5);
    t12 = (t10 ^ t11);
    t13 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t16 = (t13 ^ t15);
    t17 = (t12 | t16);
    t18 = *((unsigned int *)t6);
    t19 = *((unsigned int *)t7);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB13;

LAB10:    if (t20 != 0)
        goto LAB12;

LAB11:    *((unsigned int *)t8) = 1;

LAB13:    t23 = (t8 + 4);
    t27 = *((unsigned int *)t23);
    t28 = (~(t27));
    t29 = *((unsigned int *)t8);
    t30 = (t29 & t28);
    t31 = (t30 != 0);
    if (t31 > 0)
        goto LAB14;

LAB15:    xsi_set_current_line(166, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng6)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    if (*((unsigned int *)t6) != 0)
        goto LAB18;

LAB17:    t7 = (t5 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB18;

LAB21:    if (*((unsigned int *)t4) > *((unsigned int *)t5))
        goto LAB20;

LAB19:    *((unsigned int *)t8) = 1;

LAB20:    t23 = (t8 + 4);
    t10 = *((unsigned int *)t23);
    t11 = (~(t10));
    t12 = *((unsigned int *)t8);
    t13 = (t12 & t11);
    t15 = (t13 != 0);
    if (t15 > 0)
        goto LAB22;

LAB23:    xsi_set_current_line(168, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng7)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t10 = *((unsigned int *)t4);
    t11 = *((unsigned int *)t5);
    t12 = (t10 ^ t11);
    t13 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t16 = (t13 ^ t15);
    t17 = (t12 | t16);
    t18 = *((unsigned int *)t6);
    t19 = *((unsigned int *)t7);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB28;

LAB25:    if (t20 != 0)
        goto LAB27;

LAB26:    *((unsigned int *)t8) = 1;

LAB28:    memset(t9, 0, 8);
    t23 = (t8 + 4);
    t27 = *((unsigned int *)t23);
    t28 = (~(t27));
    t29 = *((unsigned int *)t8);
    t30 = (t29 & t28);
    t31 = (t30 & 1U);
    if (t31 != 0)
        goto LAB29;

LAB30:    if (*((unsigned int *)t23) != 0)
        goto LAB31;

LAB32:    t26 = (t9 + 4);
    t32 = *((unsigned int *)t9);
    t33 = *((unsigned int *)t26);
    t34 = (t32 || t33);
    if (t34 > 0)
        goto LAB33;

LAB34:    memcpy(t43, t9, 8);

LAB35:    t77 = (t43 + 4);
    t78 = *((unsigned int *)t77);
    t79 = (~(t78));
    t80 = *((unsigned int *)t43);
    t81 = (t80 & t79);
    t82 = (t81 != 0);
    if (t82 > 0)
        goto LAB43;

LAB44:
LAB45:
LAB24:
LAB16:    xsi_set_current_line(176, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng1)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t10 = *((unsigned int *)t4);
    t11 = *((unsigned int *)t5);
    t12 = (t10 ^ t11);
    t13 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t16 = (t13 ^ t15);
    t17 = (t12 | t16);
    t18 = *((unsigned int *)t6);
    t19 = *((unsigned int *)t7);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB50;

LAB47:    if (t20 != 0)
        goto LAB49;

LAB48:    *((unsigned int *)t8) = 1;

LAB50:    t23 = (t8 + 4);
    t27 = *((unsigned int *)t23);
    t28 = (~(t27));
    t29 = *((unsigned int *)t8);
    t30 = (t29 & t28);
    t31 = (t30 != 0);
    if (t31 > 0)
        goto LAB51;

LAB52:    xsi_set_current_line(178, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng7)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t10 = *((unsigned int *)t4);
    t11 = *((unsigned int *)t5);
    t12 = (t10 ^ t11);
    t13 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t16 = (t13 ^ t15);
    t17 = (t12 | t16);
    t18 = *((unsigned int *)t6);
    t19 = *((unsigned int *)t7);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB70;

LAB67:    if (t20 != 0)
        goto LAB69;

LAB68:    *((unsigned int *)t8) = 1;

LAB70:    memset(t9, 0, 8);
    t23 = (t8 + 4);
    t27 = *((unsigned int *)t23);
    t28 = (~(t27));
    t29 = *((unsigned int *)t8);
    t30 = (t29 & t28);
    t31 = (t30 & 1U);
    if (t31 != 0)
        goto LAB71;

LAB72:    if (*((unsigned int *)t23) != 0)
        goto LAB73;

LAB74:    t26 = (t9 + 4);
    t32 = *((unsigned int *)t9);
    t33 = *((unsigned int *)t26);
    t34 = (t32 || t33);
    if (t34 > 0)
        goto LAB75;

LAB76:    memcpy(t84, t9, 8);

LAB77:    memset(t94, 0, 8);
    t85 = (t84 + 4);
    t88 = *((unsigned int *)t85);
    t89 = (~(t88));
    t90 = *((unsigned int *)t84);
    t91 = (t90 & t89);
    t92 = (t91 & 1U);
    if (t92 != 0)
        goto LAB89;

LAB90:    if (*((unsigned int *)t85) != 0)
        goto LAB91;

LAB92:    t87 = (t94 + 4);
    t93 = *((unsigned int *)t94);
    t101 = (!(t93));
    t102 = *((unsigned int *)t87);
    t103 = (t101 || t102);
    if (t103 > 0)
        goto LAB93;

LAB94:    memcpy(t179, t94, 8);

LAB95:    t207 = (t179 + 4);
    t208 = *((unsigned int *)t207);
    t209 = (~(t208));
    t210 = *((unsigned int *)t179);
    t211 = (t210 & t209);
    t212 = (t211 != 0);
    if (t212 > 0)
        goto LAB121;

LAB122:    xsi_set_current_line(182, ng0);
    t2 = (t0 + 2208);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t8, 0, 8);
    xsi_vlog_unsigned_add(t8, 6, t4, 6, t5, 6);
    t6 = (t0 + 2300);
    xsi_vlogvar_assign_value(t6, t8, 0, 0, 6);

LAB123:
LAB53:    goto LAB2;

LAB8:    t39 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB9;

LAB12:    t14 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t14) = 1;
    goto LAB13;

LAB14:    xsi_set_current_line(165, ng0);
    t25 = ((char*)((ng1)));
    t26 = (t0 + 876U);
    t39 = *((char **)t26);
    t26 = ((char*)((ng1)));
    t40 = ((char*)((ng5)));
    xsi_vlogtype_concat(t54, 68, 67, 4U, t40, 33, t26, 1, t39, 32, t25, 1);
    t41 = (t0 + 2484);
    xsi_vlogvar_assign_value(t41, t54, 0, 0, 68);
    goto LAB16;

LAB18:    t14 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t14) = 1;
    goto LAB20;

LAB22:    xsi_set_current_line(167, ng0);
    t25 = (t0 + 2392);
    t26 = (t25 + 36U);
    t39 = *((char **)t26);
    xsi_vlog_get_part_select_value(t55, 33, t39, 33, 1);
    t40 = (t0 + 1704U);
    t41 = *((char **)t40);
    t40 = (t0 + 1704U);
    t42 = *((char **)t40);
    memset(t9, 0, 8);
    t40 = (t9 + 4);
    t44 = (t42 + 8);
    t45 = (t42 + 12);
    t16 = *((unsigned int *)t44);
    t17 = (t16 >> 1);
    t18 = (t17 & 1);
    *((unsigned int *)t9) = t18;
    t19 = *((unsigned int *)t45);
    t20 = (t19 >> 1);
    t21 = (t20 & 1);
    *((unsigned int *)t40) = t21;
    xsi_vlogtype_concat(t54, 68, 68, 3U, t9, 1, t41, 34, t55, 33);
    t46 = (t0 + 2484);
    xsi_vlogvar_assign_value(t46, t54, 0, 0, 68);
    goto LAB24;

LAB27:    t14 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t14) = 1;
    goto LAB28;

LAB29:    *((unsigned int *)t9) = 1;
    goto LAB32;

LAB31:    t25 = (t9 + 4);
    *((unsigned int *)t9) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB32;

LAB33:    t39 = (t0 + 1428U);
    t40 = *((char **)t39);
    memset(t24, 0, 8);
    t39 = (t40 + 4);
    t35 = *((unsigned int *)t39);
    t36 = (~(t35));
    t37 = *((unsigned int *)t40);
    t38 = (t37 & t36);
    t47 = (t38 & 1U);
    if (t47 != 0)
        goto LAB36;

LAB37:    if (*((unsigned int *)t39) != 0)
        goto LAB38;

LAB39:    t48 = *((unsigned int *)t9);
    t49 = *((unsigned int *)t24);
    t50 = (t48 & t49);
    *((unsigned int *)t43) = t50;
    t42 = (t9 + 4);
    t44 = (t24 + 4);
    t45 = (t43 + 4);
    t51 = *((unsigned int *)t42);
    t52 = *((unsigned int *)t44);
    t56 = (t51 | t52);
    *((unsigned int *)t45) = t56;
    t57 = *((unsigned int *)t45);
    t58 = (t57 != 0);
    if (t58 == 1)
        goto LAB40;

LAB41:
LAB42:    goto LAB35;

LAB36:    *((unsigned int *)t24) = 1;
    goto LAB39;

LAB38:    t41 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t41) = 1;
    goto LAB39;

LAB40:    t59 = *((unsigned int *)t43);
    t60 = *((unsigned int *)t45);
    *((unsigned int *)t43) = (t59 | t60);
    t46 = (t9 + 4);
    t53 = (t24 + 4);
    t61 = *((unsigned int *)t9);
    t62 = (~(t61));
    t63 = *((unsigned int *)t46);
    t64 = (~(t63));
    t65 = *((unsigned int *)t24);
    t66 = (~(t65));
    t67 = *((unsigned int *)t53);
    t68 = (~(t67));
    t69 = (t62 & t64);
    t70 = (t66 & t68);
    t71 = (~(t69));
    t72 = (~(t70));
    t73 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t73 & t71);
    t74 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t74 & t72);
    t75 = *((unsigned int *)t43);
    *((unsigned int *)t43) = (t75 & t71);
    t76 = *((unsigned int *)t43);
    *((unsigned int *)t43) = (t76 & t72);
    goto LAB42;

LAB43:    xsi_set_current_line(169, ng0);

LAB46:    xsi_set_current_line(172, ng0);
    t83 = ((char*)((ng1)));
    t85 = (t0 + 1888U);
    t86 = *((char **)t85);
    memset(t84, 0, 8);
    t85 = (t84 + 4);
    t87 = (t86 + 4);
    t88 = *((unsigned int *)t86);
    t89 = (t88 >> 0);
    *((unsigned int *)t84) = t89;
    t90 = *((unsigned int *)t87);
    t91 = (t90 >> 0);
    *((unsigned int *)t85) = t91;
    t92 = *((unsigned int *)t84);
    *((unsigned int *)t84) = (t92 & 4294967295U);
    t93 = *((unsigned int *)t85);
    *((unsigned int *)t85) = (t93 & 4294967295U);
    t95 = (t0 + 2392);
    t96 = (t95 + 36U);
    t97 = *((char **)t96);
    memset(t94, 0, 8);
    t98 = (t94 + 4);
    t99 = (t97 + 8);
    t100 = (t97 + 12);
    t101 = *((unsigned int *)t99);
    t102 = (t101 >> 1);
    *((unsigned int *)t94) = t102;
    t103 = *((unsigned int *)t100);
    t104 = (t103 >> 1);
    *((unsigned int *)t98) = t104;
    t105 = (t97 + 16);
    t106 = (t97 + 20);
    t107 = *((unsigned int *)t105);
    t108 = (t107 << 31);
    t109 = *((unsigned int *)t94);
    *((unsigned int *)t94) = (t109 | t108);
    t110 = *((unsigned int *)t106);
    t111 = (t110 << 31);
    t112 = *((unsigned int *)t98);
    *((unsigned int *)t98) = (t112 | t111);
    t113 = *((unsigned int *)t94);
    *((unsigned int *)t94) = (t113 & 4294967295U);
    t114 = *((unsigned int *)t98);
    *((unsigned int *)t98) = (t114 & 4294967295U);
    xsi_vlogtype_concat(t54, 68, 65, 3U, t94, 32, t84, 32, t83, 1);
    t115 = (t0 + 2484);
    xsi_vlogvar_assign_value(t115, t54, 0, 0, 68);
    goto LAB45;

LAB49:    t14 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t14) = 1;
    goto LAB50;

LAB51:    xsi_set_current_line(177, ng0);
    t25 = (t0 + 1336U);
    t26 = *((char **)t25);
    memset(t24, 0, 8);
    t25 = (t26 + 4);
    t32 = *((unsigned int *)t25);
    t33 = (~(t32));
    t34 = *((unsigned int *)t26);
    t35 = (t34 & t33);
    t36 = (t35 & 1U);
    if (t36 != 0)
        goto LAB54;

LAB55:    if (*((unsigned int *)t25) != 0)
        goto LAB56;

LAB57:    t40 = (t24 + 4);
    t37 = *((unsigned int *)t24);
    t38 = *((unsigned int *)t40);
    t47 = (t37 || t38);
    if (t47 > 0)
        goto LAB58;

LAB59:    t48 = *((unsigned int *)t24);
    t49 = (~(t48));
    t50 = *((unsigned int *)t40);
    t51 = (t49 || t50);
    if (t51 > 0)
        goto LAB60;

LAB61:    if (*((unsigned int *)t40) > 0)
        goto LAB62;

LAB63:    if (*((unsigned int *)t24) > 0)
        goto LAB64;

LAB65:    memcpy(t9, t42, 8);

LAB66:    t44 = (t0 + 2300);
    xsi_vlogvar_assign_value(t44, t9, 0, 0, 6);
    goto LAB53;

LAB54:    *((unsigned int *)t24) = 1;
    goto LAB57;

LAB56:    t39 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB57;

LAB58:    t41 = ((char*)((ng3)));
    goto LAB59;

LAB60:    t42 = ((char*)((ng1)));
    goto LAB61;

LAB62:    xsi_vlog_unsigned_bit_combine(t9, 6, t41, 6, t42, 6);
    goto LAB66;

LAB64:    memcpy(t9, t41, 8);
    goto LAB66;

LAB69:    t14 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t14) = 1;
    goto LAB70;

LAB71:    *((unsigned int *)t9) = 1;
    goto LAB74;

LAB73:    t25 = (t9 + 4);
    *((unsigned int *)t9) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB74;

LAB75:    t39 = (t0 + 1428U);
    t40 = *((char **)t39);
    memset(t24, 0, 8);
    t39 = (t40 + 4);
    t35 = *((unsigned int *)t39);
    t36 = (~(t35));
    t37 = *((unsigned int *)t40);
    t38 = (t37 & t36);
    t47 = (t38 & 1U);
    if (t47 != 0)
        goto LAB81;

LAB79:    if (*((unsigned int *)t39) == 0)
        goto LAB78;

LAB80:    t41 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t41) = 1;

LAB81:    memset(t43, 0, 8);
    t42 = (t24 + 4);
    t48 = *((unsigned int *)t42);
    t49 = (~(t48));
    t50 = *((unsigned int *)t24);
    t51 = (t50 & t49);
    t52 = (t51 & 1U);
    if (t52 != 0)
        goto LAB82;

LAB83:    if (*((unsigned int *)t42) != 0)
        goto LAB84;

LAB85:    t56 = *((unsigned int *)t9);
    t57 = *((unsigned int *)t43);
    t58 = (t56 & t57);
    *((unsigned int *)t84) = t58;
    t45 = (t9 + 4);
    t46 = (t43 + 4);
    t53 = (t84 + 4);
    t59 = *((unsigned int *)t45);
    t60 = *((unsigned int *)t46);
    t61 = (t59 | t60);
    *((unsigned int *)t53) = t61;
    t62 = *((unsigned int *)t53);
    t63 = (t62 != 0);
    if (t63 == 1)
        goto LAB86;

LAB87:
LAB88:    goto LAB77;

LAB78:    *((unsigned int *)t24) = 1;
    goto LAB81;

LAB82:    *((unsigned int *)t43) = 1;
    goto LAB85;

LAB84:    t44 = (t43 + 4);
    *((unsigned int *)t43) = 1;
    *((unsigned int *)t44) = 1;
    goto LAB85;

LAB86:    t64 = *((unsigned int *)t84);
    t65 = *((unsigned int *)t53);
    *((unsigned int *)t84) = (t64 | t65);
    t77 = (t9 + 4);
    t83 = (t43 + 4);
    t66 = *((unsigned int *)t9);
    t67 = (~(t66));
    t68 = *((unsigned int *)t77);
    t71 = (~(t68));
    t72 = *((unsigned int *)t43);
    t73 = (~(t72));
    t74 = *((unsigned int *)t83);
    t75 = (~(t74));
    t69 = (t67 & t71);
    t70 = (t73 & t75);
    t76 = (~(t69));
    t78 = (~(t70));
    t79 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t79 & t76);
    t80 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t80 & t78);
    t81 = *((unsigned int *)t84);
    *((unsigned int *)t84) = (t81 & t76);
    t82 = *((unsigned int *)t84);
    *((unsigned int *)t84) = (t82 & t78);
    goto LAB88;

LAB89:    *((unsigned int *)t94) = 1;
    goto LAB92;

LAB91:    t86 = (t94 + 4);
    *((unsigned int *)t94) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB92;

LAB93:    t95 = (t0 + 2208);
    t96 = (t95 + 36U);
    t97 = *((char **)t96);
    t98 = ((char*)((ng8)));
    memset(t116, 0, 8);
    t99 = (t97 + 4);
    t100 = (t98 + 4);
    t104 = *((unsigned int *)t97);
    t107 = *((unsigned int *)t98);
    t108 = (t104 ^ t107);
    t109 = *((unsigned int *)t99);
    t110 = *((unsigned int *)t100);
    t111 = (t109 ^ t110);
    t112 = (t108 | t111);
    t113 = *((unsigned int *)t99);
    t114 = *((unsigned int *)t100);
    t117 = (t113 | t114);
    t118 = (~(t117));
    t119 = (t112 & t118);
    if (t119 != 0)
        goto LAB99;

LAB96:    if (t117 != 0)
        goto LAB98;

LAB97:    *((unsigned int *)t116) = 1;

LAB99:    memset(t120, 0, 8);
    t106 = (t116 + 4);
    t121 = *((unsigned int *)t106);
    t122 = (~(t121));
    t123 = *((unsigned int *)t116);
    t124 = (t123 & t122);
    t125 = (t124 & 1U);
    if (t125 != 0)
        goto LAB100;

LAB101:    if (*((unsigned int *)t106) != 0)
        goto LAB102;

LAB103:    t126 = (t120 + 4);
    t127 = *((unsigned int *)t120);
    t128 = *((unsigned int *)t126);
    t129 = (t127 || t128);
    if (t129 > 0)
        goto LAB104;

LAB105:    memcpy(t139, t120, 8);

LAB106:    memset(t171, 0, 8);
    t172 = (t139 + 4);
    t173 = *((unsigned int *)t172);
    t174 = (~(t173));
    t175 = *((unsigned int *)t139);
    t176 = (t175 & t174);
    t177 = (t176 & 1U);
    if (t177 != 0)
        goto LAB114;

LAB115:    if (*((unsigned int *)t172) != 0)
        goto LAB116;

LAB117:    t180 = *((unsigned int *)t94);
    t181 = *((unsigned int *)t171);
    t182 = (t180 | t181);
    *((unsigned int *)t179) = t182;
    t183 = (t94 + 4);
    t184 = (t171 + 4);
    t185 = (t179 + 4);
    t186 = *((unsigned int *)t183);
    t187 = *((unsigned int *)t184);
    t188 = (t186 | t187);
    *((unsigned int *)t185) = t188;
    t189 = *((unsigned int *)t185);
    t190 = (t189 != 0);
    if (t190 == 1)
        goto LAB118;

LAB119:
LAB120:    goto LAB95;

LAB98:    t105 = (t116 + 4);
    *((unsigned int *)t116) = 1;
    *((unsigned int *)t105) = 1;
    goto LAB99;

LAB100:    *((unsigned int *)t120) = 1;
    goto LAB103;

LAB102:    t115 = (t120 + 4);
    *((unsigned int *)t120) = 1;
    *((unsigned int *)t115) = 1;
    goto LAB103;

LAB104:    t130 = (t0 + 1428U);
    t131 = *((char **)t130);
    memset(t132, 0, 8);
    t130 = (t131 + 4);
    t133 = *((unsigned int *)t130);
    t134 = (~(t133));
    t135 = *((unsigned int *)t131);
    t136 = (t135 & t134);
    t137 = (t136 & 1U);
    if (t137 != 0)
        goto LAB107;

LAB108:    if (*((unsigned int *)t130) != 0)
        goto LAB109;

LAB110:    t140 = *((unsigned int *)t120);
    t141 = *((unsigned int *)t132);
    t142 = (t140 & t141);
    *((unsigned int *)t139) = t142;
    t143 = (t120 + 4);
    t144 = (t132 + 4);
    t145 = (t139 + 4);
    t146 = *((unsigned int *)t143);
    t147 = *((unsigned int *)t144);
    t148 = (t146 | t147);
    *((unsigned int *)t145) = t148;
    t149 = *((unsigned int *)t145);
    t150 = (t149 != 0);
    if (t150 == 1)
        goto LAB111;

LAB112:
LAB113:    goto LAB106;

LAB107:    *((unsigned int *)t132) = 1;
    goto LAB110;

LAB109:    t138 = (t132 + 4);
    *((unsigned int *)t132) = 1;
    *((unsigned int *)t138) = 1;
    goto LAB110;

LAB111:    t151 = *((unsigned int *)t139);
    t152 = *((unsigned int *)t145);
    *((unsigned int *)t139) = (t151 | t152);
    t153 = (t120 + 4);
    t154 = (t132 + 4);
    t155 = *((unsigned int *)t120);
    t156 = (~(t155));
    t157 = *((unsigned int *)t153);
    t158 = (~(t157));
    t159 = *((unsigned int *)t132);
    t160 = (~(t159));
    t161 = *((unsigned int *)t154);
    t162 = (~(t161));
    t163 = (t156 & t158);
    t164 = (t160 & t162);
    t165 = (~(t163));
    t166 = (~(t164));
    t167 = *((unsigned int *)t145);
    *((unsigned int *)t145) = (t167 & t165);
    t168 = *((unsigned int *)t145);
    *((unsigned int *)t145) = (t168 & t166);
    t169 = *((unsigned int *)t139);
    *((unsigned int *)t139) = (t169 & t165);
    t170 = *((unsigned int *)t139);
    *((unsigned int *)t139) = (t170 & t166);
    goto LAB113;

LAB114:    *((unsigned int *)t171) = 1;
    goto LAB117;

LAB116:    t178 = (t171 + 4);
    *((unsigned int *)t171) = 1;
    *((unsigned int *)t178) = 1;
    goto LAB117;

LAB118:    t191 = *((unsigned int *)t179);
    t192 = *((unsigned int *)t185);
    *((unsigned int *)t179) = (t191 | t192);
    t193 = (t94 + 4);
    t194 = (t171 + 4);
    t195 = *((unsigned int *)t193);
    t196 = (~(t195));
    t197 = *((unsigned int *)t94);
    t198 = (t197 & t196);
    t199 = *((unsigned int *)t194);
    t200 = (~(t199));
    t201 = *((unsigned int *)t171);
    t202 = (t201 & t200);
    t203 = (~(t198));
    t204 = (~(t202));
    t205 = *((unsigned int *)t185);
    *((unsigned int *)t185) = (t205 & t203);
    t206 = *((unsigned int *)t185);
    *((unsigned int *)t185) = (t206 & t204);
    goto LAB120;

LAB121:    xsi_set_current_line(180, ng0);
    t213 = ((char*)((ng1)));
    t214 = (t0 + 2300);
    xsi_vlogvar_assign_value(t214, t213, 0, 0, 6);
    goto LAB123;

}

static void Always_186_8(char *t0)
{
    char t4[8];
    char t19[8];
    char t20[8];
    char t44[24];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    unsigned int t45;
    unsigned int t46;

LAB0:    t1 = (t0 + 4176U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(186, ng0);
    t2 = (t0 + 4692);
    *((int *)t2) = 1;
    t3 = (t0 + 4200);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(187, ng0);
    t5 = (t0 + 692U);
    t6 = *((char **)t5);
    memset(t4, 0, 8);
    t5 = (t6 + 4);
    t7 = *((unsigned int *)t5);
    t8 = (~(t7));
    t9 = *((unsigned int *)t6);
    t10 = (t9 & t8);
    t11 = (t10 & 1U);
    if (t11 != 0)
        goto LAB8;

LAB6:    if (*((unsigned int *)t5) == 0)
        goto LAB5;

LAB7:    t12 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t12) = 1;

LAB8:    t13 = (t4 + 4);
    t14 = *((unsigned int *)t13);
    t15 = (~(t14));
    t16 = *((unsigned int *)t4);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB9;

LAB10:
LAB11:    goto LAB2;

LAB5:    *((unsigned int *)t4) = 1;
    goto LAB8;

LAB9:    xsi_set_current_line(188, ng0);

LAB12:    xsi_set_current_line(189, ng0);
    t21 = (t0 + 1060U);
    t22 = *((char **)t21);
    memset(t20, 0, 8);
    t21 = (t22 + 4);
    t23 = *((unsigned int *)t21);
    t24 = (~(t23));
    t25 = *((unsigned int *)t22);
    t26 = (t25 & t24);
    t27 = (t26 & 1U);
    if (t27 != 0)
        goto LAB13;

LAB14:    if (*((unsigned int *)t21) != 0)
        goto LAB15;

LAB16:    t29 = (t20 + 4);
    t30 = *((unsigned int *)t20);
    t31 = *((unsigned int *)t29);
    t32 = (t30 || t31);
    if (t32 > 0)
        goto LAB17;

LAB18:    t36 = *((unsigned int *)t20);
    t37 = (~(t36));
    t38 = *((unsigned int *)t29);
    t39 = (t37 || t38);
    if (t39 > 0)
        goto LAB19;

LAB20:    if (*((unsigned int *)t29) > 0)
        goto LAB21;

LAB22:    if (*((unsigned int *)t20) > 0)
        goto LAB23;

LAB24:    memcpy(t19, t42, 8);

LAB25:    t43 = (t0 + 2208);
    xsi_vlogvar_wait_assign_value(t43, t19, 0, 0, 6, 0LL);
    xsi_set_current_line(190, ng0);
    t2 = (t0 + 1060U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t3 + 4);
    t7 = *((unsigned int *)t2);
    t8 = (~(t7));
    t9 = *((unsigned int *)t3);
    t10 = (t9 & t8);
    t11 = (t10 & 1U);
    if (t11 != 0)
        goto LAB26;

LAB27:    if (*((unsigned int *)t2) != 0)
        goto LAB28;

LAB29:    t6 = (t4 + 4);
    t14 = *((unsigned int *)t4);
    t15 = *((unsigned int *)t6);
    t16 = (t14 || t15);
    if (t16 > 0)
        goto LAB30;

LAB31:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t23 = *((unsigned int *)t6);
    t24 = (t18 || t23);
    if (t24 > 0)
        goto LAB32;

LAB33:    if (*((unsigned int *)t6) > 0)
        goto LAB34;

LAB35:    if (*((unsigned int *)t4) > 0)
        goto LAB36;

LAB37:    memcpy(t44, t29, 24);

LAB38:    t33 = (t0 + 2392);
    xsi_vlogvar_wait_assign_value(t33, t44, 0, 0, 68, 0LL);
    xsi_set_current_line(191, ng0);
    t2 = (t0 + 1060U);
    t3 = *((char **)t2);
    memset(t19, 0, 8);
    t2 = (t3 + 4);
    t7 = *((unsigned int *)t2);
    t8 = (~(t7));
    t9 = *((unsigned int *)t3);
    t10 = (t9 & t8);
    t11 = (t10 & 1U);
    if (t11 != 0)
        goto LAB39;

LAB40:    if (*((unsigned int *)t2) != 0)
        goto LAB41;

LAB42:    t6 = (t19 + 4);
    t14 = *((unsigned int *)t19);
    t15 = *((unsigned int *)t6);
    t16 = (t14 || t15);
    if (t16 > 0)
        goto LAB43;

LAB44:    t38 = *((unsigned int *)t19);
    t39 = (~(t38));
    t45 = *((unsigned int *)t6);
    t46 = (t39 || t45);
    if (t46 > 0)
        goto LAB45;

LAB46:    if (*((unsigned int *)t6) > 0)
        goto LAB47;

LAB48:    if (*((unsigned int *)t19) > 0)
        goto LAB49;

LAB50:    memcpy(t4, t40, 8);

LAB51:    t41 = (t0 + 2116);
    xsi_vlogvar_wait_assign_value(t41, t4, 0, 0, 1, 0LL);
    goto LAB11;

LAB13:    *((unsigned int *)t20) = 1;
    goto LAB16;

LAB15:    t28 = (t20 + 4);
    *((unsigned int *)t20) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB16;

LAB17:    t33 = (t0 + 2300);
    t34 = (t33 + 36U);
    t35 = *((char **)t34);
    goto LAB18;

LAB19:    t40 = (t0 + 2208);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    goto LAB20;

LAB21:    xsi_vlog_unsigned_bit_combine(t19, 6, t35, 6, t42, 6);
    goto LAB25;

LAB23:    memcpy(t19, t35, 8);
    goto LAB25;

LAB26:    *((unsigned int *)t4) = 1;
    goto LAB29;

LAB28:    t5 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t5) = 1;
    goto LAB29;

LAB30:    t12 = (t0 + 2484);
    t13 = (t12 + 36U);
    t21 = *((char **)t13);
    goto LAB31;

LAB32:    t22 = (t0 + 2392);
    t28 = (t22 + 36U);
    t29 = *((char **)t28);
    goto LAB33;

LAB34:    xsi_vlog_unsigned_bit_combine(t44, 68, t21, 68, t29, 68);
    goto LAB38;

LAB36:    memcpy(t44, t21, 24);
    goto LAB38;

LAB39:    *((unsigned int *)t19) = 1;
    goto LAB42;

LAB41:    t5 = (t19 + 4);
    *((unsigned int *)t19) = 1;
    *((unsigned int *)t5) = 1;
    goto LAB42;

LAB43:    t12 = (t0 + 2208);
    t13 = (t12 + 36U);
    t21 = *((char **)t13);
    t22 = ((char*)((ng9)));
    memset(t20, 0, 8);
    t28 = (t21 + 4);
    t29 = (t22 + 4);
    t17 = *((unsigned int *)t21);
    t18 = *((unsigned int *)t22);
    t23 = (t17 ^ t18);
    t24 = *((unsigned int *)t28);
    t25 = *((unsigned int *)t29);
    t26 = (t24 ^ t25);
    t27 = (t23 | t26);
    t30 = *((unsigned int *)t28);
    t31 = *((unsigned int *)t29);
    t32 = (t30 | t31);
    t36 = (~(t32));
    t37 = (t27 & t36);
    if (t37 != 0)
        goto LAB55;

LAB52:    if (t32 != 0)
        goto LAB54;

LAB53:    *((unsigned int *)t20) = 1;

LAB55:    goto LAB44;

LAB45:    t34 = (t0 + 2116);
    t35 = (t34 + 36U);
    t40 = *((char **)t35);
    goto LAB46;

LAB47:    xsi_vlog_unsigned_bit_combine(t4, 1, t20, 1, t40, 1);
    goto LAB51;

LAB49:    memcpy(t4, t20, 8);
    goto LAB51;

LAB54:    t33 = (t20 + 4);
    *((unsigned int *)t20) = 1;
    *((unsigned int *)t33) = 1;
    goto LAB55;

}

static void Cont_195_9(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    t1 = (t0 + 4312U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(195, ng0);
    t2 = (t0 + 2392);
    t4 = (t2 + 36U);
    t5 = *((char **)t4);
    memset(t3, 0, 8);
    t6 = (t3 + 4);
    t7 = (t5 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 1);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t7);
    t11 = (t10 >> 1);
    *((unsigned int *)t6) = t11;
    t12 = (t5 + 8);
    t13 = (t5 + 12);
    t14 = *((unsigned int *)t12);
    t15 = (t14 << 31);
    t16 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t16 | t15);
    t17 = *((unsigned int *)t13);
    t18 = (t17 << 31);
    t19 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t19 | t18);
    t20 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t20 & 4294967295U);
    t21 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t21 & 4294967295U);
    t22 = (t0 + 5004);
    t23 = (t22 + 32U);
    t24 = *((char **)t23);
    t25 = (t24 + 32U);
    t26 = *((char **)t25);
    memcpy(t26, t3, 8);
    xsi_driver_vfirst_trans(t22, 0, 31);
    t27 = (t0 + 4700);
    *((int *)t27) = 1;

LAB1:    return;
}

static void Cont_196_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;

LAB0:    t1 = (t0 + 4448U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(196, ng0);
    t2 = (t0 + 2576);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 5040);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    memset(t9, 0, 8);
    t10 = 3U;
    t11 = t10;
    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t10 = (t10 & t13);
    t14 = *((unsigned int *)t12);
    t11 = (t11 & t14);
    t15 = (t9 + 4);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 | t10);
    t17 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t17 | t11);
    xsi_driver_vfirst_trans(t5, 0, 1);
    t18 = (t0 + 4708);
    *((int *)t18) = 1;

LAB1:    return;
}


extern void work_m_00000000002832526842_2621494917_init()
{
	static char *pe[] = {(void *)Cont_86_0,(void *)Cont_87_1,(void *)Cont_89_2,(void *)Cont_90_3,(void *)Cont_92_4,(void *)Cont_141_5,(void *)Cont_146_6,(void *)Always_151_7,(void *)Always_186_8,(void *)Cont_195_9,(void *)Cont_196_10};
	xsi_register_didat("work_m_00000000002832526842_2621494917", "isim/amber-test.exe.sim/work/m_00000000002832526842_2621494917.didat");
	xsi_register_executes(pe);
}
