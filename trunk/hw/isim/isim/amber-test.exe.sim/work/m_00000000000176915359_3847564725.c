/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leeth/amber/trunk/hw/vlog/system/memory_configuration.vh";
static unsigned int ng1[] = {0U, 0U};
static int ng2[] = {2, 0};
static int ng3[] = {1, 0};
static unsigned int ng4[] = {40960U, 0U};
static unsigned int ng5[] = {5632U, 0U};
static unsigned int ng6[] = {5888U, 0U};
static unsigned int ng7[] = {5120U, 0U};
static unsigned int ng8[] = {4864U, 0U};
static unsigned int ng9[] = {61440U, 0U};
static unsigned int ng10[] = {8192U, 0U};
static const char *ng11 = "/home/leeth/amber/trunk/hw/vlog/system/main_mem.v";
static unsigned int ng12[] = {4095U, 0U};
static unsigned int ng13[] = {1U, 0U};
static unsigned int ng14[] = {15U, 0U};
static unsigned int ng15[] = {255U, 0U};
static unsigned int ng16[] = {2U, 0U};
static int ng17[] = {4, 0};



static int sp_in_loboot_mem(char *t1, char *t2)
{
    char t7[8];
    char t11[8];
    char t30[8];
    char t31[8];
    char t32[8];
    char t34[8];
    char t35[8];
    char t39[8];
    char t47[8];
    int t0;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t33;
    char *t36;
    char *t37;
    char *t38;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    char *t46;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    char *t51;
    char *t52;
    char *t53;
    unsigned int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    char *t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    int t71;
    int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    unsigned int t78;
    char *t79;

LAB0:    t0 = 1;
    xsi_set_current_line(63, ng0);

LAB2:    xsi_set_current_line(64, ng0);
    t3 = (t1 + 6892);
    t4 = (t3 + 36U);
    t5 = *((char **)t4);
    t6 = ((char*)((ng1)));
    memset(t7, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB4;

LAB3:    t9 = (t6 + 4);
    if (*((unsigned int *)t9) != 0)
        goto LAB4;

LAB7:    if (*((unsigned int *)t5) < *((unsigned int *)t6))
        goto LAB6;

LAB5:    *((unsigned int *)t7) = 1;

LAB6:    memset(t11, 0, 8);
    t12 = (t7 + 4);
    t13 = *((unsigned int *)t12);
    t14 = (~(t13));
    t15 = *((unsigned int *)t7);
    t16 = (t15 & t14);
    t17 = (t16 & 1U);
    if (t17 != 0)
        goto LAB8;

LAB9:    if (*((unsigned int *)t12) != 0)
        goto LAB10;

LAB11:    t19 = (t11 + 4);
    t20 = *((unsigned int *)t11);
    t21 = *((unsigned int *)t19);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB12;

LAB13:    memcpy(t47, t11, 8);

LAB14:    t79 = (t1 + 6800);
    xsi_vlogvar_assign_value(t79, t47, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB4:    t10 = (t7 + 4);
    *((unsigned int *)t7) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB6;

LAB8:    *((unsigned int *)t11) = 1;
    goto LAB11;

LAB10:    t18 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t18) = 1;
    goto LAB11;

LAB12:    t23 = (t1 + 6892);
    t24 = (t23 + 36U);
    t25 = *((char **)t24);
    t26 = ((char*)((ng1)));
    t27 = ((char*)((ng2)));
    t28 = (t1 + 504);
    t29 = *((char **)t28);
    t28 = ((char*)((ng3)));
    memset(t30, 0, 8);
    xsi_vlog_signed_add(t30, 32, t29, 32, t28, 32);
    memset(t31, 0, 8);
    xsi_vlog_unsigned_power(t31, 32, t27, 32, t30, 32, 1);
    memset(t32, 0, 8);
    xsi_vlog_unsigned_add(t32, 32, t26, 32, t31, 32);
    t33 = ((char*)((ng3)));
    memset(t34, 0, 8);
    xsi_vlog_unsigned_minus(t34, 32, t32, 32, t33, 32);
    memset(t35, 0, 8);
    t36 = (t25 + 4);
    if (*((unsigned int *)t36) != 0)
        goto LAB16;

LAB15:    t37 = (t34 + 4);
    if (*((unsigned int *)t37) != 0)
        goto LAB16;

LAB19:    if (*((unsigned int *)t25) < *((unsigned int *)t34))
        goto LAB17;

LAB18:    memset(t39, 0, 8);
    t40 = (t35 + 4);
    t41 = *((unsigned int *)t40);
    t42 = (~(t41));
    t43 = *((unsigned int *)t35);
    t44 = (t43 & t42);
    t45 = (t44 & 1U);
    if (t45 != 0)
        goto LAB20;

LAB21:    if (*((unsigned int *)t40) != 0)
        goto LAB22;

LAB23:    t48 = *((unsigned int *)t11);
    t49 = *((unsigned int *)t39);
    t50 = (t48 & t49);
    *((unsigned int *)t47) = t50;
    t51 = (t11 + 4);
    t52 = (t39 + 4);
    t53 = (t47 + 4);
    t54 = *((unsigned int *)t51);
    t55 = *((unsigned int *)t52);
    t56 = (t54 | t55);
    *((unsigned int *)t53) = t56;
    t57 = *((unsigned int *)t53);
    t58 = (t57 != 0);
    if (t58 == 1)
        goto LAB24;

LAB25:
LAB26:    goto LAB14;

LAB16:    t38 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t38) = 1;
    goto LAB18;

LAB17:    *((unsigned int *)t35) = 1;
    goto LAB18;

LAB20:    *((unsigned int *)t39) = 1;
    goto LAB23;

LAB22:    t46 = (t39 + 4);
    *((unsigned int *)t39) = 1;
    *((unsigned int *)t46) = 1;
    goto LAB23;

LAB24:    t59 = *((unsigned int *)t47);
    t60 = *((unsigned int *)t53);
    *((unsigned int *)t47) = (t59 | t60);
    t61 = (t11 + 4);
    t62 = (t39 + 4);
    t63 = *((unsigned int *)t11);
    t64 = (~(t63));
    t65 = *((unsigned int *)t61);
    t66 = (~(t65));
    t67 = *((unsigned int *)t39);
    t68 = (~(t67));
    t69 = *((unsigned int *)t62);
    t70 = (~(t69));
    t71 = (t64 & t66);
    t72 = (t68 & t70);
    t73 = (~(t71));
    t74 = (~(t72));
    t75 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t75 & t73);
    t76 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t76 & t74);
    t77 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t77 & t73);
    t78 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t78 & t74);
    goto LAB26;

}

static int sp_in_hiboot_mem(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(72, ng0);

LAB2:    xsi_set_current_line(73, ng0);
    t4 = (t1 + 7076);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 14);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 14);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 262143U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 262143U);
    t15 = ((char*)((ng4)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 6984);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_boot_mem(char *t1, char *t2)
{
    char t23[8];
    char t27[8];
    char t60[8];
    char t64[8];
    char t72[8];
    int t0;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int t19;
    char *t20;
    char *t21;
    char *t22;
    char *t24;
    char *t25;
    char *t26;
    char *t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    char *t54;
    char *t55;
    int t56;
    char *t57;
    char *t58;
    char *t59;
    char *t61;
    char *t62;
    char *t63;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    char *t71;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    char *t76;
    char *t77;
    char *t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    char *t86;
    char *t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    int t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    char *t100;

LAB0:    t0 = 1;
    xsi_set_current_line(80, ng0);

LAB2:    xsi_set_current_line(81, ng0);
    t3 = (t1 + 7260);
    t4 = (t3 + 36U);
    t5 = *((char **)t4);
    t6 = (t2 + 32U);
    t7 = *((char **)t6);
    t8 = (t1 + 1524);
    t9 = xsi_create_subprogram_invocation(t7, 0, t1, t8, 0, t2);
    t10 = (t1 + 6892);
    xsi_vlogvar_assign_value(t10, t5, 0, 0, 32);

LAB3:    t11 = (t2 + 36U);
    t12 = *((char **)t11);
    t13 = (t12 + 44U);
    t14 = *((char **)t13);
    t15 = (t14 + 148U);
    t16 = *((char **)t15);
    t17 = (t16 + 0U);
    t18 = *((char **)t17);
    t19 = ((int  (*)(char *, char *))t18)(t1, t12);
    if (t19 != 0)
        goto LAB5;

LAB4:    t12 = (t2 + 36U);
    t20 = *((char **)t12);
    t12 = (t1 + 6800);
    t21 = (t12 + 36U);
    t22 = *((char **)t21);
    memcpy(t23, t22, 8);
    t24 = (t1 + 1524);
    t25 = (t2 + 32U);
    t26 = *((char **)t25);
    xsi_delete_subprogram_invocation(t24, t20, t1, t26, t2);
    memset(t27, 0, 8);
    t28 = (t23 + 4);
    t29 = *((unsigned int *)t28);
    t30 = (~(t29));
    t31 = *((unsigned int *)t23);
    t32 = (t31 & t30);
    t33 = (t32 & 1U);
    if (t33 != 0)
        goto LAB6;

LAB7:    if (*((unsigned int *)t28) != 0)
        goto LAB8;

LAB9:    t35 = (t27 + 4);
    t36 = *((unsigned int *)t27);
    t37 = (!(t36));
    t38 = *((unsigned int *)t35);
    t39 = (t37 || t38);
    if (t39 > 0)
        goto LAB10;

LAB11:    memcpy(t72, t27, 8);

LAB12:    t100 = (t1 + 7168);
    xsi_vlogvar_assign_value(t100, t72, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t11 = (t2 + 28U);
    *((char **)t11) = &&LAB3;
    goto LAB1;

LAB6:    *((unsigned int *)t27) = 1;
    goto LAB9;

LAB8:    t34 = (t27 + 4);
    *((unsigned int *)t27) = 1;
    *((unsigned int *)t34) = 1;
    goto LAB9;

LAB10:    t40 = (t1 + 7260);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t2 + 32U);
    t44 = *((char **)t43);
    t45 = (t1 + 1780);
    t46 = xsi_create_subprogram_invocation(t44, 0, t1, t45, 0, t2);
    t47 = (t1 + 7076);
    xsi_vlogvar_assign_value(t47, t42, 0, 0, 32);

LAB13:    t48 = (t2 + 36U);
    t49 = *((char **)t48);
    t50 = (t49 + 44U);
    t51 = *((char **)t50);
    t52 = (t51 + 148U);
    t53 = *((char **)t52);
    t54 = (t53 + 0U);
    t55 = *((char **)t54);
    t56 = ((int  (*)(char *, char *))t55)(t1, t49);
    if (t56 != 0)
        goto LAB15;

LAB14:    t49 = (t2 + 36U);
    t57 = *((char **)t49);
    t49 = (t1 + 6984);
    t58 = (t49 + 36U);
    t59 = *((char **)t58);
    memcpy(t60, t59, 8);
    t61 = (t1 + 1780);
    t62 = (t2 + 32U);
    t63 = *((char **)t62);
    xsi_delete_subprogram_invocation(t61, t57, t1, t63, t2);
    memset(t64, 0, 8);
    t65 = (t60 + 4);
    t66 = *((unsigned int *)t65);
    t67 = (~(t66));
    t68 = *((unsigned int *)t60);
    t69 = (t68 & t67);
    t70 = (t69 & 1U);
    if (t70 != 0)
        goto LAB16;

LAB17:    if (*((unsigned int *)t65) != 0)
        goto LAB18;

LAB19:    t73 = *((unsigned int *)t27);
    t74 = *((unsigned int *)t64);
    t75 = (t73 | t74);
    *((unsigned int *)t72) = t75;
    t76 = (t27 + 4);
    t77 = (t64 + 4);
    t78 = (t72 + 4);
    t79 = *((unsigned int *)t76);
    t80 = *((unsigned int *)t77);
    t81 = (t79 | t80);
    *((unsigned int *)t78) = t81;
    t82 = *((unsigned int *)t78);
    t83 = (t82 != 0);
    if (t83 == 1)
        goto LAB20;

LAB21:
LAB22:    goto LAB12;

LAB15:    t48 = (t2 + 28U);
    *((char **)t48) = &&LAB13;
    goto LAB1;

LAB16:    *((unsigned int *)t64) = 1;
    goto LAB19;

LAB18:    t71 = (t64 + 4);
    *((unsigned int *)t64) = 1;
    *((unsigned int *)t71) = 1;
    goto LAB19;

LAB20:    t84 = *((unsigned int *)t72);
    t85 = *((unsigned int *)t78);
    *((unsigned int *)t72) = (t84 | t85);
    t86 = (t27 + 4);
    t87 = (t64 + 4);
    t88 = *((unsigned int *)t86);
    t89 = (~(t88));
    t90 = *((unsigned int *)t27);
    t91 = (t90 & t89);
    t92 = *((unsigned int *)t87);
    t93 = (~(t92));
    t94 = *((unsigned int *)t64);
    t95 = (t94 & t93);
    t96 = (~(t91));
    t97 = (~(t95));
    t98 = *((unsigned int *)t78);
    *((unsigned int *)t78) = (t98 & t96);
    t99 = *((unsigned int *)t78);
    *((unsigned int *)t78) = (t99 & t97);
    goto LAB22;

}

static int sp_in_main_mem(char *t1, char *t2)
{
    char t7[8];
    char t11[8];
    char t30[8];
    char t31[8];
    char t32[8];
    char t34[8];
    char t35[8];
    char t39[8];
    char t47[8];
    char t79[8];
    char t91[8];
    char t112[8];
    char t123[8];
    char t131[8];
    int t0;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t33;
    char *t36;
    char *t37;
    char *t38;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    char *t46;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    char *t51;
    char *t52;
    char *t53;
    unsigned int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    char *t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    int t71;
    int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    unsigned int t78;
    char *t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    char *t86;
    char *t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    char *t92;
    char *t93;
    char *t94;
    char *t95;
    char *t96;
    char *t97;
    char *t98;
    char *t99;
    char *t100;
    char *t101;
    char *t102;
    char *t103;
    char *t104;
    char *t105;
    char *t106;
    char *t107;
    int t108;
    char *t109;
    char *t110;
    char *t111;
    char *t113;
    char *t114;
    char *t115;
    char *t116;
    unsigned int t117;
    unsigned int t118;
    unsigned int t119;
    unsigned int t120;
    unsigned int t121;
    char *t122;
    char *t124;
    unsigned int t125;
    unsigned int t126;
    unsigned int t127;
    unsigned int t128;
    unsigned int t129;
    char *t130;
    unsigned int t132;
    unsigned int t133;
    unsigned int t134;
    char *t135;
    char *t136;
    char *t137;
    unsigned int t138;
    unsigned int t139;
    unsigned int t140;
    unsigned int t141;
    unsigned int t142;
    unsigned int t143;
    unsigned int t144;
    char *t145;
    char *t146;
    unsigned int t147;
    unsigned int t148;
    unsigned int t149;
    unsigned int t150;
    unsigned int t151;
    unsigned int t152;
    unsigned int t153;
    unsigned int t154;
    int t155;
    int t156;
    unsigned int t157;
    unsigned int t158;
    unsigned int t159;
    unsigned int t160;
    unsigned int t161;
    unsigned int t162;
    char *t163;

LAB0:    t0 = 1;
    xsi_set_current_line(88, ng0);

LAB2:    xsi_set_current_line(89, ng0);
    t3 = (t1 + 7444);
    t4 = (t3 + 36U);
    t5 = *((char **)t4);
    t6 = ((char*)((ng1)));
    memset(t7, 0, 8);
    t8 = (t5 + 4);
    if (*((unsigned int *)t8) != 0)
        goto LAB4;

LAB3:    t9 = (t6 + 4);
    if (*((unsigned int *)t9) != 0)
        goto LAB4;

LAB7:    if (*((unsigned int *)t5) < *((unsigned int *)t6))
        goto LAB6;

LAB5:    *((unsigned int *)t7) = 1;

LAB6:    memset(t11, 0, 8);
    t12 = (t7 + 4);
    t13 = *((unsigned int *)t12);
    t14 = (~(t13));
    t15 = *((unsigned int *)t7);
    t16 = (t15 & t14);
    t17 = (t16 & 1U);
    if (t17 != 0)
        goto LAB8;

LAB9:    if (*((unsigned int *)t12) != 0)
        goto LAB10;

LAB11:    t19 = (t11 + 4);
    t20 = *((unsigned int *)t11);
    t21 = *((unsigned int *)t19);
    t22 = (t20 || t21);
    if (t22 > 0)
        goto LAB12;

LAB13:    memcpy(t47, t11, 8);

LAB14:    memset(t79, 0, 8);
    t80 = (t47 + 4);
    t81 = *((unsigned int *)t80);
    t82 = (~(t81));
    t83 = *((unsigned int *)t47);
    t84 = (t83 & t82);
    t85 = (t84 & 1U);
    if (t85 != 0)
        goto LAB27;

LAB28:    if (*((unsigned int *)t80) != 0)
        goto LAB29;

LAB30:    t87 = (t79 + 4);
    t88 = *((unsigned int *)t79);
    t89 = *((unsigned int *)t87);
    t90 = (t88 || t89);
    if (t90 > 0)
        goto LAB31;

LAB32:    memcpy(t131, t79, 8);

LAB33:    t163 = (t1 + 7352);
    xsi_vlogvar_assign_value(t163, t131, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB4:    t10 = (t7 + 4);
    *((unsigned int *)t7) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB6;

LAB8:    *((unsigned int *)t11) = 1;
    goto LAB11;

LAB10:    t18 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t18) = 1;
    goto LAB11;

LAB12:    t23 = (t1 + 7444);
    t24 = (t23 + 36U);
    t25 = *((char **)t24);
    t26 = ((char*)((ng1)));
    t27 = ((char*)((ng2)));
    t28 = (t1 + 424);
    t29 = *((char **)t28);
    t28 = ((char*)((ng3)));
    memset(t30, 0, 8);
    xsi_vlog_signed_add(t30, 32, t29, 32, t28, 32);
    memset(t31, 0, 8);
    xsi_vlog_unsigned_power(t31, 32, t27, 32, t30, 32, 1);
    memset(t32, 0, 8);
    xsi_vlog_unsigned_add(t32, 32, t26, 32, t31, 32);
    t33 = ((char*)((ng3)));
    memset(t34, 0, 8);
    xsi_vlog_unsigned_minus(t34, 32, t32, 32, t33, 32);
    memset(t35, 0, 8);
    t36 = (t25 + 4);
    if (*((unsigned int *)t36) != 0)
        goto LAB16;

LAB15:    t37 = (t34 + 4);
    if (*((unsigned int *)t37) != 0)
        goto LAB16;

LAB19:    if (*((unsigned int *)t25) < *((unsigned int *)t34))
        goto LAB17;

LAB18:    memset(t39, 0, 8);
    t40 = (t35 + 4);
    t41 = *((unsigned int *)t40);
    t42 = (~(t41));
    t43 = *((unsigned int *)t35);
    t44 = (t43 & t42);
    t45 = (t44 & 1U);
    if (t45 != 0)
        goto LAB20;

LAB21:    if (*((unsigned int *)t40) != 0)
        goto LAB22;

LAB23:    t48 = *((unsigned int *)t11);
    t49 = *((unsigned int *)t39);
    t50 = (t48 & t49);
    *((unsigned int *)t47) = t50;
    t51 = (t11 + 4);
    t52 = (t39 + 4);
    t53 = (t47 + 4);
    t54 = *((unsigned int *)t51);
    t55 = *((unsigned int *)t52);
    t56 = (t54 | t55);
    *((unsigned int *)t53) = t56;
    t57 = *((unsigned int *)t53);
    t58 = (t57 != 0);
    if (t58 == 1)
        goto LAB24;

LAB25:
LAB26:    goto LAB14;

LAB16:    t38 = (t35 + 4);
    *((unsigned int *)t35) = 1;
    *((unsigned int *)t38) = 1;
    goto LAB18;

LAB17:    *((unsigned int *)t35) = 1;
    goto LAB18;

LAB20:    *((unsigned int *)t39) = 1;
    goto LAB23;

LAB22:    t46 = (t39 + 4);
    *((unsigned int *)t39) = 1;
    *((unsigned int *)t46) = 1;
    goto LAB23;

LAB24:    t59 = *((unsigned int *)t47);
    t60 = *((unsigned int *)t53);
    *((unsigned int *)t47) = (t59 | t60);
    t61 = (t11 + 4);
    t62 = (t39 + 4);
    t63 = *((unsigned int *)t11);
    t64 = (~(t63));
    t65 = *((unsigned int *)t61);
    t66 = (~(t65));
    t67 = *((unsigned int *)t39);
    t68 = (~(t67));
    t69 = *((unsigned int *)t62);
    t70 = (~(t69));
    t71 = (t64 & t66);
    t72 = (t68 & t70);
    t73 = (~(t71));
    t74 = (~(t72));
    t75 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t75 & t73);
    t76 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t76 & t74);
    t77 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t77 & t73);
    t78 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t78 & t74);
    goto LAB26;

LAB27:    *((unsigned int *)t79) = 1;
    goto LAB30;

LAB29:    t86 = (t79 + 4);
    *((unsigned int *)t79) = 1;
    *((unsigned int *)t86) = 1;
    goto LAB30;

LAB31:    t92 = (t1 + 7444);
    t93 = (t92 + 36U);
    t94 = *((char **)t93);
    t95 = (t2 + 32U);
    t96 = *((char **)t95);
    t97 = (t1 + 2036);
    t98 = xsi_create_subprogram_invocation(t96, 0, t1, t97, 0, t2);
    t99 = (t1 + 7260);
    xsi_vlogvar_assign_value(t99, t94, 0, 0, 32);

LAB34:    t100 = (t2 + 36U);
    t101 = *((char **)t100);
    t102 = (t101 + 44U);
    t103 = *((char **)t102);
    t104 = (t103 + 148U);
    t105 = *((char **)t104);
    t106 = (t105 + 0U);
    t107 = *((char **)t106);
    t108 = ((int  (*)(char *, char *))t107)(t1, t101);
    if (t108 != 0)
        goto LAB36;

LAB35:    t101 = (t2 + 36U);
    t109 = *((char **)t101);
    t101 = (t1 + 7168);
    t110 = (t101 + 36U);
    t111 = *((char **)t110);
    memcpy(t112, t111, 8);
    t113 = (t1 + 2036);
    t114 = (t2 + 32U);
    t115 = *((char **)t114);
    xsi_delete_subprogram_invocation(t113, t109, t1, t115, t2);
    memset(t91, 0, 8);
    t116 = (t112 + 4);
    t117 = *((unsigned int *)t116);
    t118 = (~(t117));
    t119 = *((unsigned int *)t112);
    t120 = (t119 & t118);
    t121 = (t120 & 1U);
    if (t121 != 0)
        goto LAB40;

LAB38:    if (*((unsigned int *)t116) == 0)
        goto LAB37;

LAB39:    t122 = (t91 + 4);
    *((unsigned int *)t91) = 1;
    *((unsigned int *)t122) = 1;

LAB40:    memset(t123, 0, 8);
    t124 = (t91 + 4);
    t125 = *((unsigned int *)t124);
    t126 = (~(t125));
    t127 = *((unsigned int *)t91);
    t128 = (t127 & t126);
    t129 = (t128 & 1U);
    if (t129 != 0)
        goto LAB41;

LAB42:    if (*((unsigned int *)t124) != 0)
        goto LAB43;

LAB44:    t132 = *((unsigned int *)t79);
    t133 = *((unsigned int *)t123);
    t134 = (t132 & t133);
    *((unsigned int *)t131) = t134;
    t135 = (t79 + 4);
    t136 = (t123 + 4);
    t137 = (t131 + 4);
    t138 = *((unsigned int *)t135);
    t139 = *((unsigned int *)t136);
    t140 = (t138 | t139);
    *((unsigned int *)t137) = t140;
    t141 = *((unsigned int *)t137);
    t142 = (t141 != 0);
    if (t142 == 1)
        goto LAB45;

LAB46:
LAB47:    goto LAB33;

LAB36:    t100 = (t2 + 28U);
    *((char **)t100) = &&LAB34;
    goto LAB1;

LAB37:    *((unsigned int *)t91) = 1;
    goto LAB40;

LAB41:    *((unsigned int *)t123) = 1;
    goto LAB44;

LAB43:    t130 = (t123 + 4);
    *((unsigned int *)t123) = 1;
    *((unsigned int *)t130) = 1;
    goto LAB44;

LAB45:    t143 = *((unsigned int *)t131);
    t144 = *((unsigned int *)t137);
    *((unsigned int *)t131) = (t143 | t144);
    t145 = (t79 + 4);
    t146 = (t123 + 4);
    t147 = *((unsigned int *)t79);
    t148 = (~(t147));
    t149 = *((unsigned int *)t145);
    t150 = (~(t149));
    t151 = *((unsigned int *)t123);
    t152 = (~(t151));
    t153 = *((unsigned int *)t146);
    t154 = (~(t153));
    t155 = (t148 & t150);
    t156 = (t152 & t154);
    t157 = (~(t155));
    t158 = (~(t156));
    t159 = *((unsigned int *)t137);
    *((unsigned int *)t137) = (t159 & t157);
    t160 = *((unsigned int *)t137);
    *((unsigned int *)t137) = (t160 & t158);
    t161 = *((unsigned int *)t131);
    *((unsigned int *)t131) = (t161 & t157);
    t162 = *((unsigned int *)t131);
    *((unsigned int *)t131) = (t162 & t158);
    goto LAB47;

}

static int sp_in_uart0(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(99, ng0);

LAB2:    xsi_set_current_line(100, ng0);
    t4 = (t1 + 7628);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng5)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 7536);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_uart1(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(108, ng0);

LAB2:    xsi_set_current_line(109, ng0);
    t4 = (t1 + 7812);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng6)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 7720);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_ic(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(117, ng0);

LAB2:    xsi_set_current_line(118, ng0);
    t4 = (t1 + 7996);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng7)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 7904);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_tm(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(126, ng0);

LAB2:    xsi_set_current_line(127, ng0);
    t4 = (t1 + 8180);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng8)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 8088);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_test(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(135, ng0);

LAB2:    xsi_set_current_line(136, ng0);
    t4 = (t1 + 8364);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng9)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 8272);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_ethmac(char *t1, char *t2)
{
    char t3[8];
    char t16[8];
    int t0;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    char *t32;

LAB0:    t0 = 1;
    xsi_set_current_line(144, ng0);

LAB2:    xsi_set_current_line(145, ng0);
    t4 = (t1 + 8548);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t3 + 4);
    t8 = (t6 + 4);
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 16);
    *((unsigned int *)t3) = t10;
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 16);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t13 & 65535U);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 & 65535U);
    t15 = ((char*)((ng10)));
    memset(t16, 0, 8);
    t17 = (t3 + 4);
    t18 = (t15 + 4);
    t19 = *((unsigned int *)t3);
    t20 = *((unsigned int *)t15);
    t21 = (t19 ^ t20);
    t22 = *((unsigned int *)t17);
    t23 = *((unsigned int *)t18);
    t24 = (t22 ^ t23);
    t25 = (t21 | t24);
    t26 = *((unsigned int *)t17);
    t27 = *((unsigned int *)t18);
    t28 = (t26 | t27);
    t29 = (~(t28));
    t30 = (t25 & t29);
    if (t30 != 0)
        goto LAB6;

LAB3:    if (t28 != 0)
        goto LAB5;

LAB4:    *((unsigned int *)t16) = 1;

LAB6:    t32 = (t1 + 8456);
    xsi_vlogvar_assign_value(t32, t16, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t31 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB6;

}

static int sp_in_cachable_mem(char *t1, char *t2)
{
    char t23[8];
    char t27[8];
    char t60[8];
    char t64[8];
    char t72[8];
    int t0;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int t19;
    char *t20;
    char *t21;
    char *t22;
    char *t24;
    char *t25;
    char *t26;
    char *t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    char *t54;
    char *t55;
    int t56;
    char *t57;
    char *t58;
    char *t59;
    char *t61;
    char *t62;
    char *t63;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    char *t71;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    char *t76;
    char *t77;
    char *t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    char *t86;
    char *t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    int t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    char *t100;

LAB0:    t0 = 1;
    xsi_set_current_line(154, ng0);

LAB2:    xsi_set_current_line(155, ng0);
    t3 = (t1 + 8732);
    t4 = (t3 + 36U);
    t5 = *((char **)t4);
    t6 = (t2 + 32U);
    t7 = *((char **)t6);
    t8 = (t1 + 1524);
    t9 = xsi_create_subprogram_invocation(t7, 0, t1, t8, 0, t2);
    t10 = (t1 + 6892);
    xsi_vlogvar_assign_value(t10, t5, 0, 0, 32);

LAB3:    t11 = (t2 + 36U);
    t12 = *((char **)t11);
    t13 = (t12 + 44U);
    t14 = *((char **)t13);
    t15 = (t14 + 148U);
    t16 = *((char **)t15);
    t17 = (t16 + 0U);
    t18 = *((char **)t17);
    t19 = ((int  (*)(char *, char *))t18)(t1, t12);
    if (t19 != 0)
        goto LAB5;

LAB4:    t12 = (t2 + 36U);
    t20 = *((char **)t12);
    t12 = (t1 + 6800);
    t21 = (t12 + 36U);
    t22 = *((char **)t21);
    memcpy(t23, t22, 8);
    t24 = (t1 + 1524);
    t25 = (t2 + 32U);
    t26 = *((char **)t25);
    xsi_delete_subprogram_invocation(t24, t20, t1, t26, t2);
    memset(t27, 0, 8);
    t28 = (t23 + 4);
    t29 = *((unsigned int *)t28);
    t30 = (~(t29));
    t31 = *((unsigned int *)t23);
    t32 = (t31 & t30);
    t33 = (t32 & 1U);
    if (t33 != 0)
        goto LAB6;

LAB7:    if (*((unsigned int *)t28) != 0)
        goto LAB8;

LAB9:    t35 = (t27 + 4);
    t36 = *((unsigned int *)t27);
    t37 = (!(t36));
    t38 = *((unsigned int *)t35);
    t39 = (t37 || t38);
    if (t39 > 0)
        goto LAB10;

LAB11:    memcpy(t72, t27, 8);

LAB12:    t100 = (t1 + 8640);
    xsi_vlogvar_assign_value(t100, t72, 0, 0, 1);
    t0 = 0;

LAB1:    return t0;
LAB5:    t11 = (t2 + 28U);
    *((char **)t11) = &&LAB3;
    goto LAB1;

LAB6:    *((unsigned int *)t27) = 1;
    goto LAB9;

LAB8:    t34 = (t27 + 4);
    *((unsigned int *)t27) = 1;
    *((unsigned int *)t34) = 1;
    goto LAB9;

LAB10:    t40 = (t1 + 8732);
    t41 = (t40 + 36U);
    t42 = *((char **)t41);
    t43 = (t2 + 32U);
    t44 = *((char **)t43);
    t45 = (t1 + 2292);
    t46 = xsi_create_subprogram_invocation(t44, 0, t1, t45, 0, t2);
    t47 = (t1 + 7444);
    xsi_vlogvar_assign_value(t47, t42, 0, 0, 32);

LAB13:    t48 = (t2 + 36U);
    t49 = *((char **)t48);
    t50 = (t49 + 44U);
    t51 = *((char **)t50);
    t52 = (t51 + 148U);
    t53 = *((char **)t52);
    t54 = (t53 + 0U);
    t55 = *((char **)t54);
    t56 = ((int  (*)(char *, char *))t55)(t1, t49);
    if (t56 != 0)
        goto LAB15;

LAB14:    t49 = (t2 + 36U);
    t57 = *((char **)t49);
    t49 = (t1 + 7352);
    t58 = (t49 + 36U);
    t59 = *((char **)t58);
    memcpy(t60, t59, 8);
    t61 = (t1 + 2292);
    t62 = (t2 + 32U);
    t63 = *((char **)t62);
    xsi_delete_subprogram_invocation(t61, t57, t1, t63, t2);
    memset(t64, 0, 8);
    t65 = (t60 + 4);
    t66 = *((unsigned int *)t65);
    t67 = (~(t66));
    t68 = *((unsigned int *)t60);
    t69 = (t68 & t67);
    t70 = (t69 & 1U);
    if (t70 != 0)
        goto LAB16;

LAB17:    if (*((unsigned int *)t65) != 0)
        goto LAB18;

LAB19:    t73 = *((unsigned int *)t27);
    t74 = *((unsigned int *)t64);
    t75 = (t73 | t74);
    *((unsigned int *)t72) = t75;
    t76 = (t27 + 4);
    t77 = (t64 + 4);
    t78 = (t72 + 4);
    t79 = *((unsigned int *)t76);
    t80 = *((unsigned int *)t77);
    t81 = (t79 | t80);
    *((unsigned int *)t78) = t81;
    t82 = *((unsigned int *)t78);
    t83 = (t82 != 0);
    if (t83 == 1)
        goto LAB20;

LAB21:
LAB22:    goto LAB12;

LAB15:    t48 = (t2 + 28U);
    *((char **)t48) = &&LAB13;
    goto LAB1;

LAB16:    *((unsigned int *)t64) = 1;
    goto LAB19;

LAB18:    t71 = (t64 + 4);
    *((unsigned int *)t64) = 1;
    *((unsigned int *)t71) = 1;
    goto LAB19;

LAB20:    t84 = *((unsigned int *)t72);
    t85 = *((unsigned int *)t78);
    *((unsigned int *)t72) = (t84 | t85);
    t86 = (t27 + 4);
    t87 = (t64 + 4);
    t88 = *((unsigned int *)t86);
    t89 = (~(t88));
    t90 = *((unsigned int *)t27);
    t91 = (t90 & t89);
    t92 = *((unsigned int *)t87);
    t93 = (~(t92));
    t94 = *((unsigned int *)t64);
    t95 = (t94 & t93);
    t96 = (~(t91));
    t97 = (~(t95));
    t98 = *((unsigned int *)t78);
    *((unsigned int *)t78) = (t98 & t96);
    t99 = *((unsigned int *)t78);
    *((unsigned int *)t78) = (t99 & t97);
    goto LAB22;

}

static void Cont_83_0(char *t0)
{
    char t4[8];
    char t17[8];
    char t24[8];
    char t56[8];
    char t68[8];
    char t77[8];
    char t85[8];
    char *t1;
    char *t2;
    char *t3;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t16;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    char *t38;
    char *t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    int t48;
    int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    char *t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    char *t69;
    char *t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    char *t76;
    char *t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    char *t84;
    unsigned int t86;
    unsigned int t87;
    unsigned int t88;
    char *t89;
    char *t90;
    char *t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    char *t99;
    char *t100;
    unsigned int t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    unsigned int t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    int t109;
    int t110;
    unsigned int t111;
    unsigned int t112;
    unsigned int t113;
    unsigned int t114;
    unsigned int t115;
    unsigned int t116;
    char *t117;
    char *t118;
    char *t119;
    char *t120;
    char *t121;
    unsigned int t122;
    unsigned int t123;
    char *t124;
    unsigned int t125;
    unsigned int t126;
    char *t127;
    unsigned int t128;
    unsigned int t129;
    char *t130;

LAB0:    t1 = (t0 + 9244U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(83, ng11);
    t2 = (t0 + 5192U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t3 + 4);
    t5 = *((unsigned int *)t2);
    t6 = (~(t5));
    t7 = *((unsigned int *)t3);
    t8 = (t7 & t6);
    t9 = (t8 & 1U);
    if (t9 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t11 = (t4 + 4);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t11);
    t14 = (t12 || t13);
    if (t14 > 0)
        goto LAB8;

LAB9:    memcpy(t24, t4, 8);

LAB10:    memset(t56, 0, 8);
    t57 = (t24 + 4);
    t58 = *((unsigned int *)t57);
    t59 = (~(t58));
    t60 = *((unsigned int *)t24);
    t61 = (t60 & t59);
    t62 = (t61 & 1U);
    if (t62 != 0)
        goto LAB18;

LAB19:    if (*((unsigned int *)t57) != 0)
        goto LAB20;

LAB21:    t64 = (t56 + 4);
    t65 = *((unsigned int *)t56);
    t66 = *((unsigned int *)t64);
    t67 = (t65 || t66);
    if (t67 > 0)
        goto LAB22;

LAB23:    memcpy(t85, t56, 8);

LAB24:    t117 = (t0 + 12916);
    t118 = (t117 + 32U);
    t119 = *((char **)t118);
    t120 = (t119 + 32U);
    t121 = *((char **)t120);
    memset(t121, 0, 8);
    t122 = 1U;
    t123 = t122;
    t124 = (t85 + 4);
    t125 = *((unsigned int *)t85);
    t122 = (t122 & t125);
    t126 = *((unsigned int *)t124);
    t123 = (t123 & t126);
    t127 = (t121 + 4);
    t128 = *((unsigned int *)t121);
    *((unsigned int *)t121) = (t128 | t122);
    t129 = *((unsigned int *)t127);
    *((unsigned int *)t127) = (t129 | t123);
    xsi_driver_vfirst_trans(t117, 0, 0);
    t130 = (t0 + 12688);
    *((int *)t130) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t10 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB7;

LAB8:    t15 = (t0 + 4824U);
    t16 = *((char **)t15);
    memset(t17, 0, 8);
    t15 = (t16 + 4);
    t18 = *((unsigned int *)t15);
    t19 = (~(t18));
    t20 = *((unsigned int *)t16);
    t21 = (t20 & t19);
    t22 = (t21 & 1U);
    if (t22 != 0)
        goto LAB11;

LAB12:    if (*((unsigned int *)t15) != 0)
        goto LAB13;

LAB14:    t25 = *((unsigned int *)t4);
    t26 = *((unsigned int *)t17);
    t27 = (t25 & t26);
    *((unsigned int *)t24) = t27;
    t28 = (t4 + 4);
    t29 = (t17 + 4);
    t30 = (t24 + 4);
    t31 = *((unsigned int *)t28);
    t32 = *((unsigned int *)t29);
    t33 = (t31 | t32);
    *((unsigned int *)t30) = t33;
    t34 = *((unsigned int *)t30);
    t35 = (t34 != 0);
    if (t35 == 1)
        goto LAB15;

LAB16:
LAB17:    goto LAB10;

LAB11:    *((unsigned int *)t17) = 1;
    goto LAB14;

LAB13:    t23 = (t17 + 4);
    *((unsigned int *)t17) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB14;

LAB15:    t36 = *((unsigned int *)t24);
    t37 = *((unsigned int *)t30);
    *((unsigned int *)t24) = (t36 | t37);
    t38 = (t4 + 4);
    t39 = (t17 + 4);
    t40 = *((unsigned int *)t4);
    t41 = (~(t40));
    t42 = *((unsigned int *)t38);
    t43 = (~(t42));
    t44 = *((unsigned int *)t17);
    t45 = (~(t44));
    t46 = *((unsigned int *)t39);
    t47 = (~(t46));
    t48 = (t41 & t43);
    t49 = (t45 & t47);
    t50 = (~(t48));
    t51 = (~(t49));
    t52 = *((unsigned int *)t30);
    *((unsigned int *)t30) = (t52 & t50);
    t53 = *((unsigned int *)t30);
    *((unsigned int *)t30) = (t53 & t51);
    t54 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t54 & t50);
    t55 = *((unsigned int *)t24);
    *((unsigned int *)t24) = (t55 & t51);
    goto LAB17;

LAB18:    *((unsigned int *)t56) = 1;
    goto LAB21;

LAB20:    t63 = (t56 + 4);
    *((unsigned int *)t56) = 1;
    *((unsigned int *)t63) = 1;
    goto LAB21;

LAB22:    t69 = (t0 + 5836U);
    t70 = *((char **)t69);
    memset(t68, 0, 8);
    t69 = (t70 + 4);
    t71 = *((unsigned int *)t69);
    t72 = (~(t71));
    t73 = *((unsigned int *)t70);
    t74 = (t73 & t72);
    t75 = (t74 & 1U);
    if (t75 != 0)
        goto LAB28;

LAB26:    if (*((unsigned int *)t69) == 0)
        goto LAB25;

LAB27:    t76 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t76) = 1;

LAB28:    memset(t77, 0, 8);
    t78 = (t68 + 4);
    t79 = *((unsigned int *)t78);
    t80 = (~(t79));
    t81 = *((unsigned int *)t68);
    t82 = (t81 & t80);
    t83 = (t82 & 1U);
    if (t83 != 0)
        goto LAB29;

LAB30:    if (*((unsigned int *)t78) != 0)
        goto LAB31;

LAB32:    t86 = *((unsigned int *)t56);
    t87 = *((unsigned int *)t77);
    t88 = (t86 & t87);
    *((unsigned int *)t85) = t88;
    t89 = (t56 + 4);
    t90 = (t77 + 4);
    t91 = (t85 + 4);
    t92 = *((unsigned int *)t89);
    t93 = *((unsigned int *)t90);
    t94 = (t92 | t93);
    *((unsigned int *)t91) = t94;
    t95 = *((unsigned int *)t91);
    t96 = (t95 != 0);
    if (t96 == 1)
        goto LAB33;

LAB34:
LAB35:    goto LAB24;

LAB25:    *((unsigned int *)t68) = 1;
    goto LAB28;

LAB29:    *((unsigned int *)t77) = 1;
    goto LAB32;

LAB31:    t84 = (t77 + 4);
    *((unsigned int *)t77) = 1;
    *((unsigned int *)t84) = 1;
    goto LAB32;

LAB33:    t97 = *((unsigned int *)t85);
    t98 = *((unsigned int *)t91);
    *((unsigned int *)t85) = (t97 | t98);
    t99 = (t56 + 4);
    t100 = (t77 + 4);
    t101 = *((unsigned int *)t56);
    t102 = (~(t101));
    t103 = *((unsigned int *)t99);
    t104 = (~(t103));
    t105 = *((unsigned int *)t77);
    t106 = (~(t105));
    t107 = *((unsigned int *)t100);
    t108 = (~(t107));
    t109 = (t102 & t104);
    t110 = (t106 & t108);
    t111 = (~(t109));
    t112 = (~(t110));
    t113 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t113 & t111);
    t114 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t114 & t112);
    t115 = *((unsigned int *)t85);
    *((unsigned int *)t85) = (t115 & t111);
    t116 = *((unsigned int *)t85);
    *((unsigned int *)t85) = (t116 & t112);
    goto LAB35;

}

static void Cont_84_1(char *t0)
{
    char t4[8];
    char t15[8];
    char t24[8];
    char t32[8];
    char t64[8];
    char t76[8];
    char t85[8];
    char t93[8];
    char *t1;
    char *t2;
    char *t3;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    char *t31;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    char *t36;
    char *t37;
    char *t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    char *t46;
    char *t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    int t56;
    int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    char *t71;
    char *t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    char *t77;
    char *t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    char *t84;
    char *t86;
    unsigned int t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    unsigned int t91;
    char *t92;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    char *t97;
    char *t98;
    char *t99;
    unsigned int t100;
    unsigned int t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    unsigned int t105;
    unsigned int t106;
    char *t107;
    char *t108;
    unsigned int t109;
    unsigned int t110;
    unsigned int t111;
    unsigned int t112;
    unsigned int t113;
    unsigned int t114;
    unsigned int t115;
    unsigned int t116;
    int t117;
    int t118;
    unsigned int t119;
    unsigned int t120;
    unsigned int t121;
    unsigned int t122;
    unsigned int t123;
    unsigned int t124;
    char *t125;
    char *t126;
    char *t127;
    char *t128;
    char *t129;
    unsigned int t130;
    unsigned int t131;
    char *t132;
    unsigned int t133;
    unsigned int t134;
    char *t135;
    unsigned int t136;
    unsigned int t137;
    char *t138;

LAB0:    t1 = (t0 + 9380U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(84, ng11);
    t2 = (t0 + 5192U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t3 + 4);
    t5 = *((unsigned int *)t2);
    t6 = (~(t5));
    t7 = *((unsigned int *)t3);
    t8 = (t7 & t6);
    t9 = (t8 & 1U);
    if (t9 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t11 = (t4 + 4);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t11);
    t14 = (t12 || t13);
    if (t14 > 0)
        goto LAB8;

LAB9:    memcpy(t32, t4, 8);

LAB10:    memset(t64, 0, 8);
    t65 = (t32 + 4);
    t66 = *((unsigned int *)t65);
    t67 = (~(t66));
    t68 = *((unsigned int *)t32);
    t69 = (t68 & t67);
    t70 = (t69 & 1U);
    if (t70 != 0)
        goto LAB22;

LAB23:    if (*((unsigned int *)t65) != 0)
        goto LAB24;

LAB25:    t72 = (t64 + 4);
    t73 = *((unsigned int *)t64);
    t74 = *((unsigned int *)t72);
    t75 = (t73 || t74);
    if (t75 > 0)
        goto LAB26;

LAB27:    memcpy(t93, t64, 8);

LAB28:    t125 = (t0 + 12952);
    t126 = (t125 + 32U);
    t127 = *((char **)t126);
    t128 = (t127 + 32U);
    t129 = *((char **)t128);
    memset(t129, 0, 8);
    t130 = 1U;
    t131 = t130;
    t132 = (t93 + 4);
    t133 = *((unsigned int *)t93);
    t130 = (t130 & t133);
    t134 = *((unsigned int *)t132);
    t131 = (t131 & t134);
    t135 = (t129 + 4);
    t136 = *((unsigned int *)t129);
    *((unsigned int *)t129) = (t136 | t130);
    t137 = *((unsigned int *)t135);
    *((unsigned int *)t135) = (t137 | t131);
    xsi_driver_vfirst_trans(t125, 0, 0);
    t138 = (t0 + 12696);
    *((int *)t138) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t10 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB7;

LAB8:    t16 = (t0 + 4824U);
    t17 = *((char **)t16);
    memset(t15, 0, 8);
    t16 = (t17 + 4);
    t18 = *((unsigned int *)t16);
    t19 = (~(t18));
    t20 = *((unsigned int *)t17);
    t21 = (t20 & t19);
    t22 = (t21 & 1U);
    if (t22 != 0)
        goto LAB14;

LAB12:    if (*((unsigned int *)t16) == 0)
        goto LAB11;

LAB13:    t23 = (t15 + 4);
    *((unsigned int *)t15) = 1;
    *((unsigned int *)t23) = 1;

LAB14:    memset(t24, 0, 8);
    t25 = (t15 + 4);
    t26 = *((unsigned int *)t25);
    t27 = (~(t26));
    t28 = *((unsigned int *)t15);
    t29 = (t28 & t27);
    t30 = (t29 & 1U);
    if (t30 != 0)
        goto LAB15;

LAB16:    if (*((unsigned int *)t25) != 0)
        goto LAB17;

LAB18:    t33 = *((unsigned int *)t4);
    t34 = *((unsigned int *)t24);
    t35 = (t33 & t34);
    *((unsigned int *)t32) = t35;
    t36 = (t4 + 4);
    t37 = (t24 + 4);
    t38 = (t32 + 4);
    t39 = *((unsigned int *)t36);
    t40 = *((unsigned int *)t37);
    t41 = (t39 | t40);
    *((unsigned int *)t38) = t41;
    t42 = *((unsigned int *)t38);
    t43 = (t42 != 0);
    if (t43 == 1)
        goto LAB19;

LAB20:
LAB21:    goto LAB10;

LAB11:    *((unsigned int *)t15) = 1;
    goto LAB14;

LAB15:    *((unsigned int *)t24) = 1;
    goto LAB18;

LAB17:    t31 = (t24 + 4);
    *((unsigned int *)t24) = 1;
    *((unsigned int *)t31) = 1;
    goto LAB18;

LAB19:    t44 = *((unsigned int *)t32);
    t45 = *((unsigned int *)t38);
    *((unsigned int *)t32) = (t44 | t45);
    t46 = (t4 + 4);
    t47 = (t24 + 4);
    t48 = *((unsigned int *)t4);
    t49 = (~(t48));
    t50 = *((unsigned int *)t46);
    t51 = (~(t50));
    t52 = *((unsigned int *)t24);
    t53 = (~(t52));
    t54 = *((unsigned int *)t47);
    t55 = (~(t54));
    t56 = (t49 & t51);
    t57 = (t53 & t55);
    t58 = (~(t56));
    t59 = (~(t57));
    t60 = *((unsigned int *)t38);
    *((unsigned int *)t38) = (t60 & t58);
    t61 = *((unsigned int *)t38);
    *((unsigned int *)t38) = (t61 & t59);
    t62 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t62 & t58);
    t63 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t63 & t59);
    goto LAB21;

LAB22:    *((unsigned int *)t64) = 1;
    goto LAB25;

LAB24:    t71 = (t64 + 4);
    *((unsigned int *)t64) = 1;
    *((unsigned int *)t71) = 1;
    goto LAB25;

LAB26:    t77 = (t0 + 5836U);
    t78 = *((char **)t77);
    memset(t76, 0, 8);
    t77 = (t78 + 4);
    t79 = *((unsigned int *)t77);
    t80 = (~(t79));
    t81 = *((unsigned int *)t78);
    t82 = (t81 & t80);
    t83 = (t82 & 1U);
    if (t83 != 0)
        goto LAB32;

LAB30:    if (*((unsigned int *)t77) == 0)
        goto LAB29;

LAB31:    t84 = (t76 + 4);
    *((unsigned int *)t76) = 1;
    *((unsigned int *)t84) = 1;

LAB32:    memset(t85, 0, 8);
    t86 = (t76 + 4);
    t87 = *((unsigned int *)t86);
    t88 = (~(t87));
    t89 = *((unsigned int *)t76);
    t90 = (t89 & t88);
    t91 = (t90 & 1U);
    if (t91 != 0)
        goto LAB33;

LAB34:    if (*((unsigned int *)t86) != 0)
        goto LAB35;

LAB36:    t94 = *((unsigned int *)t64);
    t95 = *((unsigned int *)t85);
    t96 = (t94 & t95);
    *((unsigned int *)t93) = t96;
    t97 = (t64 + 4);
    t98 = (t85 + 4);
    t99 = (t93 + 4);
    t100 = *((unsigned int *)t97);
    t101 = *((unsigned int *)t98);
    t102 = (t100 | t101);
    *((unsigned int *)t99) = t102;
    t103 = *((unsigned int *)t99);
    t104 = (t103 != 0);
    if (t104 == 1)
        goto LAB37;

LAB38:
LAB39:    goto LAB28;

LAB29:    *((unsigned int *)t76) = 1;
    goto LAB32;

LAB33:    *((unsigned int *)t85) = 1;
    goto LAB36;

LAB35:    t92 = (t85 + 4);
    *((unsigned int *)t85) = 1;
    *((unsigned int *)t92) = 1;
    goto LAB36;

LAB37:    t105 = *((unsigned int *)t93);
    t106 = *((unsigned int *)t99);
    *((unsigned int *)t93) = (t105 | t106);
    t107 = (t64 + 4);
    t108 = (t85 + 4);
    t109 = *((unsigned int *)t64);
    t110 = (~(t109));
    t111 = *((unsigned int *)t107);
    t112 = (~(t111));
    t113 = *((unsigned int *)t85);
    t114 = (~(t113));
    t115 = *((unsigned int *)t108);
    t116 = (~(t115));
    t117 = (t110 & t112);
    t118 = (t114 & t116);
    t119 = (~(t117));
    t120 = (~(t118));
    t121 = *((unsigned int *)t99);
    *((unsigned int *)t99) = (t121 & t119);
    t122 = *((unsigned int *)t99);
    *((unsigned int *)t99) = (t122 & t120);
    t123 = *((unsigned int *)t93);
    *((unsigned int *)t93) = (t123 & t119);
    t124 = *((unsigned int *)t93);
    *((unsigned int *)t93) = (t124 & t120);
    goto LAB39;

}

static void Cont_85_2(char *t0)
{
    char t5[8];
    char t21[8];
    char t29[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    char *t20;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    char *t43;
    char *t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    unsigned int t56;
    char *t57;
    char *t58;
    char *t59;
    char *t60;
    char *t61;
    unsigned int t62;
    unsigned int t63;
    char *t64;
    unsigned int t65;
    unsigned int t66;
    char *t67;
    unsigned int t68;
    unsigned int t69;
    char *t70;

LAB0:    t1 = (t0 + 9516U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(85, ng11);
    t2 = (t0 + 6156);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    memset(t5, 0, 8);
    t6 = (t4 + 4);
    t7 = *((unsigned int *)t6);
    t8 = (~(t7));
    t9 = *((unsigned int *)t4);
    t10 = (t9 & t8);
    t11 = (t10 & 1U);
    if (t11 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t6) != 0)
        goto LAB6;

LAB7:    t13 = (t5 + 4);
    t14 = *((unsigned int *)t5);
    t15 = (!(t14));
    t16 = *((unsigned int *)t13);
    t17 = (t15 || t16);
    if (t17 > 0)
        goto LAB8;

LAB9:    memcpy(t29, t5, 8);

LAB10:    t57 = (t0 + 12988);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    t60 = (t59 + 32U);
    t61 = *((char **)t60);
    memset(t61, 0, 8);
    t62 = 1U;
    t63 = t62;
    t64 = (t29 + 4);
    t65 = *((unsigned int *)t29);
    t62 = (t62 & t65);
    t66 = *((unsigned int *)t64);
    t63 = (t63 & t66);
    t67 = (t61 + 4);
    t68 = *((unsigned int *)t61);
    *((unsigned int *)t61) = (t68 | t62);
    t69 = *((unsigned int *)t67);
    *((unsigned int *)t67) = (t69 | t63);
    xsi_driver_vfirst_trans(t57, 0, 0);
    t70 = (t0 + 12704);
    *((int *)t70) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t5) = 1;
    goto LAB7;

LAB6:    t12 = (t5 + 4);
    *((unsigned int *)t5) = 1;
    *((unsigned int *)t12) = 1;
    goto LAB7;

LAB8:    t18 = (t0 + 6248);
    t19 = (t18 + 36U);
    t20 = *((char **)t19);
    memset(t21, 0, 8);
    t22 = (t20 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t20);
    t26 = (t25 & t24);
    t27 = (t26 & 1U);
    if (t27 != 0)
        goto LAB11;

LAB12:    if (*((unsigned int *)t22) != 0)
        goto LAB13;

LAB14:    t30 = *((unsigned int *)t5);
    t31 = *((unsigned int *)t21);
    t32 = (t30 | t31);
    *((unsigned int *)t29) = t32;
    t33 = (t5 + 4);
    t34 = (t21 + 4);
    t35 = (t29 + 4);
    t36 = *((unsigned int *)t33);
    t37 = *((unsigned int *)t34);
    t38 = (t36 | t37);
    *((unsigned int *)t35) = t38;
    t39 = *((unsigned int *)t35);
    t40 = (t39 != 0);
    if (t40 == 1)
        goto LAB15;

LAB16:
LAB17:    goto LAB10;

LAB11:    *((unsigned int *)t21) = 1;
    goto LAB14;

LAB13:    t28 = (t21 + 4);
    *((unsigned int *)t21) = 1;
    *((unsigned int *)t28) = 1;
    goto LAB14;

LAB15:    t41 = *((unsigned int *)t29);
    t42 = *((unsigned int *)t35);
    *((unsigned int *)t29) = (t41 | t42);
    t43 = (t5 + 4);
    t44 = (t21 + 4);
    t45 = *((unsigned int *)t43);
    t46 = (~(t45));
    t47 = *((unsigned int *)t5);
    t48 = (t47 & t46);
    t49 = *((unsigned int *)t44);
    t50 = (~(t49));
    t51 = *((unsigned int *)t21);
    t52 = (t51 & t50);
    t53 = (~(t48));
    t54 = (~(t52));
    t55 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t55 & t53);
    t56 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t56 & t54);
    goto LAB17;

}

static void Cont_87_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;

LAB0:    t1 = (t0 + 9652U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(87, ng11);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 13024);
    t4 = (t3 + 32U);
    t5 = *((char **)t4);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 1U;
    t9 = t8;
    t10 = (t2 + 4);
    t11 = *((unsigned int *)t2);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t3, 0, 0);

LAB1:    return;
}

static void Always_142_4(char *t0)
{
    char t6[8];
    char t7[8];
    char t8[8];
    char t15[8];
    char t42[8];
    char t43[8];
    char t60[8];
    char t61[8];
    char t62[8];
    char t73[8];
    char t100[8];
    char t102[8];
    char t119[8];
    char t120[8];
    char t121[8];
    char t132[8];
    char t159[8];
    char t161[8];
    char t178[8];
    char t180[8];
    char t193[32];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    char *t37;
    char *t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    char *t63;
    char *t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    unsigned int t71;
    char *t72;
    char *t74;
    char *t75;
    unsigned int t76;
    unsigned int t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    unsigned int t86;
    unsigned int t87;
    char *t88;
    char *t89;
    unsigned int t90;
    unsigned int t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    char *t95;
    char *t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    char *t101;
    char *t103;
    char *t104;
    char *t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    unsigned int t110;
    unsigned int t111;
    unsigned int t112;
    unsigned int t113;
    char *t114;
    unsigned int t115;
    unsigned int t116;
    unsigned int t117;
    unsigned int t118;
    char *t122;
    char *t123;
    char *t124;
    unsigned int t125;
    unsigned int t126;
    unsigned int t127;
    unsigned int t128;
    unsigned int t129;
    unsigned int t130;
    char *t131;
    char *t133;
    char *t134;
    unsigned int t135;
    unsigned int t136;
    unsigned int t137;
    unsigned int t138;
    unsigned int t139;
    unsigned int t140;
    unsigned int t141;
    unsigned int t142;
    unsigned int t143;
    unsigned int t144;
    unsigned int t145;
    unsigned int t146;
    char *t147;
    char *t148;
    unsigned int t149;
    unsigned int t150;
    unsigned int t151;
    unsigned int t152;
    unsigned int t153;
    char *t154;
    char *t155;
    unsigned int t156;
    unsigned int t157;
    unsigned int t158;
    char *t160;
    char *t162;
    char *t163;
    char *t164;
    unsigned int t165;
    unsigned int t166;
    unsigned int t167;
    unsigned int t168;
    unsigned int t169;
    unsigned int t170;
    unsigned int t171;
    unsigned int t172;
    char *t173;
    unsigned int t174;
    unsigned int t175;
    unsigned int t176;
    unsigned int t177;
    char *t179;
    char *t181;
    char *t182;
    char *t183;
    unsigned int t184;
    unsigned int t185;
    unsigned int t186;
    unsigned int t187;
    unsigned int t188;
    unsigned int t189;
    unsigned int t190;
    unsigned int t191;
    char *t192;
    int t194;
    int t195;
    int t196;
    int t197;
    int t198;

LAB0:    t1 = (t0 + 9788U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(142, ng11);
    t2 = (t0 + 12712);
    *((int *)t2) = 1;
    t3 = (t0 + 9812);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(143, ng11);

LAB5:    xsi_set_current_line(144, ng11);
    t4 = (t0 + 5468U);
    t5 = *((char **)t4);
    t4 = (t0 + 6340);
    xsi_vlogvar_wait_assign_value(t4, t5, 0, 0, 1, 0LL);
    xsi_set_current_line(145, ng11);
    t2 = (t0 + 4640U);
    t3 = *((char **)t2);
    memset(t8, 0, 8);
    t2 = (t8 + 4);
    t4 = (t3 + 4);
    t9 = *((unsigned int *)t3);
    t10 = (t9 >> 2);
    *((unsigned int *)t8) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 2);
    *((unsigned int *)t2) = t12;
    t13 = *((unsigned int *)t8);
    *((unsigned int *)t8) = (t13 & 3U);
    t14 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t14 & 3U);
    t5 = ((char*)((ng1)));
    memset(t15, 0, 8);
    t16 = (t8 + 4);
    t17 = (t5 + 4);
    t18 = *((unsigned int *)t8);
    t19 = *((unsigned int *)t5);
    t20 = (t18 ^ t19);
    t21 = *((unsigned int *)t16);
    t22 = *((unsigned int *)t17);
    t23 = (t21 ^ t22);
    t24 = (t20 | t23);
    t25 = *((unsigned int *)t16);
    t26 = *((unsigned int *)t17);
    t27 = (t25 | t26);
    t28 = (~(t27));
    t29 = (t24 & t28);
    if (t29 != 0)
        goto LAB9;

LAB6:    if (t27 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t15) = 1;

LAB9:    memset(t7, 0, 8);
    t31 = (t15 + 4);
    t32 = *((unsigned int *)t31);
    t33 = (~(t32));
    t34 = *((unsigned int *)t15);
    t35 = (t34 & t33);
    t36 = (t35 & 1U);
    if (t36 != 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t31) != 0)
        goto LAB12;

LAB13:    t38 = (t7 + 4);
    t39 = *((unsigned int *)t7);
    t40 = *((unsigned int *)t38);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB14;

LAB15:    t56 = *((unsigned int *)t7);
    t57 = (~(t56));
    t58 = *((unsigned int *)t38);
    t59 = (t57 || t58);
    if (t59 > 0)
        goto LAB16;

LAB17:    if (*((unsigned int *)t38) > 0)
        goto LAB18;

LAB19:    if (*((unsigned int *)t7) > 0)
        goto LAB20;

LAB21:    memcpy(t6, t60, 8);

LAB22:    t192 = (t0 + 6432);
    xsi_vlogvar_wait_assign_value(t192, t6, 0, 0, 16, 0LL);
    xsi_set_current_line(149, ng11);
    t2 = ((char*)((ng17)));
    t3 = (t0 + 5008U);
    t4 = *((char **)t3);
    xsi_vlog_mul_concat(t193, 128, 32, t2, 1U, t4, 32);
    t3 = (t0 + 6524);
    xsi_vlogvar_wait_assign_value(t3, t193, 0, 0, 128, 0LL);
    xsi_set_current_line(152, ng11);
    t2 = (t0 + 4548U);
    t3 = *((char **)t2);
    memset(t7, 0, 8);
    t2 = (t3 + 4);
    t9 = *((unsigned int *)t2);
    t10 = (~(t9));
    t11 = *((unsigned int *)t3);
    t12 = (t11 & t10);
    t13 = (t12 & 1U);
    if (t13 != 0)
        goto LAB65;

LAB66:    if (*((unsigned int *)t2) != 0)
        goto LAB67;

LAB68:    t5 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    t18 = *((unsigned int *)t5);
    t19 = (t14 || t18);
    if (t19 > 0)
        goto LAB69;

LAB70:    t26 = *((unsigned int *)t7);
    t27 = (~(t26));
    t28 = *((unsigned int *)t5);
    t29 = (t27 || t28);
    if (t29 > 0)
        goto LAB71;

LAB72:    if (*((unsigned int *)t5) > 0)
        goto LAB73;

LAB74:    if (*((unsigned int *)t7) > 0)
        goto LAB75;

LAB76:    memcpy(t6, t42, 8);

LAB77:    t45 = (t0 + 6616);
    xsi_vlogvar_wait_assign_value(t45, t6, 0, 0, 28, 0LL);
    xsi_set_current_line(154, ng11);
    t2 = (t0 + 6340);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (~(t9));
    t11 = *((unsigned int *)t4);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB78;

LAB79:
LAB80:    goto LAB2;

LAB8:    t30 = (t15 + 4);
    *((unsigned int *)t15) = 1;
    *((unsigned int *)t30) = 1;
    goto LAB9;

LAB10:    *((unsigned int *)t7) = 1;
    goto LAB13;

LAB12:    t37 = (t7 + 4);
    *((unsigned int *)t7) = 1;
    *((unsigned int *)t37) = 1;
    goto LAB13;

LAB14:    t44 = (t0 + 4732U);
    t45 = *((char **)t44);
    memset(t43, 0, 8);
    t44 = (t43 + 4);
    t46 = (t45 + 4);
    t47 = *((unsigned int *)t45);
    t48 = (~(t47));
    *((unsigned int *)t43) = t48;
    *((unsigned int *)t44) = 0;
    if (*((unsigned int *)t46) != 0)
        goto LAB24;

LAB23:    t53 = *((unsigned int *)t43);
    *((unsigned int *)t43) = (t53 & 15U);
    t54 = *((unsigned int *)t44);
    *((unsigned int *)t44) = (t54 & 15U);
    t55 = ((char*)((ng12)));
    xsi_vlogtype_concat(t42, 16, 16, 2U, t55, 12, t43, 4);
    goto LAB15;

LAB16:    t63 = (t0 + 4640U);
    t64 = *((char **)t63);
    memset(t62, 0, 8);
    t63 = (t62 + 4);
    t65 = (t64 + 4);
    t66 = *((unsigned int *)t64);
    t67 = (t66 >> 2);
    *((unsigned int *)t62) = t67;
    t68 = *((unsigned int *)t65);
    t69 = (t68 >> 2);
    *((unsigned int *)t63) = t69;
    t70 = *((unsigned int *)t62);
    *((unsigned int *)t62) = (t70 & 3U);
    t71 = *((unsigned int *)t63);
    *((unsigned int *)t63) = (t71 & 3U);
    t72 = ((char*)((ng13)));
    memset(t73, 0, 8);
    t74 = (t62 + 4);
    t75 = (t72 + 4);
    t76 = *((unsigned int *)t62);
    t77 = *((unsigned int *)t72);
    t78 = (t76 ^ t77);
    t79 = *((unsigned int *)t74);
    t80 = *((unsigned int *)t75);
    t81 = (t79 ^ t80);
    t82 = (t78 | t81);
    t83 = *((unsigned int *)t74);
    t84 = *((unsigned int *)t75);
    t85 = (t83 | t84);
    t86 = (~(t85));
    t87 = (t82 & t86);
    if (t87 != 0)
        goto LAB28;

LAB25:    if (t85 != 0)
        goto LAB27;

LAB26:    *((unsigned int *)t73) = 1;

LAB28:    memset(t61, 0, 8);
    t89 = (t73 + 4);
    t90 = *((unsigned int *)t89);
    t91 = (~(t90));
    t92 = *((unsigned int *)t73);
    t93 = (t92 & t91);
    t94 = (t93 & 1U);
    if (t94 != 0)
        goto LAB29;

LAB30:    if (*((unsigned int *)t89) != 0)
        goto LAB31;

LAB32:    t96 = (t61 + 4);
    t97 = *((unsigned int *)t61);
    t98 = *((unsigned int *)t96);
    t99 = (t97 || t98);
    if (t99 > 0)
        goto LAB33;

LAB34:    t115 = *((unsigned int *)t61);
    t116 = (~(t115));
    t117 = *((unsigned int *)t96);
    t118 = (t116 || t117);
    if (t118 > 0)
        goto LAB35;

LAB36:    if (*((unsigned int *)t96) > 0)
        goto LAB37;

LAB38:    if (*((unsigned int *)t61) > 0)
        goto LAB39;

LAB40:    memcpy(t60, t119, 8);

LAB41:    goto LAB17;

LAB18:    xsi_vlog_unsigned_bit_combine(t6, 16, t42, 16, t60, 16);
    goto LAB22;

LAB20:    memcpy(t6, t42, 8);
    goto LAB22;

LAB24:    t49 = *((unsigned int *)t43);
    t50 = *((unsigned int *)t46);
    *((unsigned int *)t43) = (t49 | t50);
    t51 = *((unsigned int *)t44);
    t52 = *((unsigned int *)t46);
    *((unsigned int *)t44) = (t51 | t52);
    goto LAB23;

LAB27:    t88 = (t73 + 4);
    *((unsigned int *)t73) = 1;
    *((unsigned int *)t88) = 1;
    goto LAB28;

LAB29:    *((unsigned int *)t61) = 1;
    goto LAB32;

LAB31:    t95 = (t61 + 4);
    *((unsigned int *)t61) = 1;
    *((unsigned int *)t95) = 1;
    goto LAB32;

LAB33:    t101 = ((char*)((ng14)));
    t103 = (t0 + 4732U);
    t104 = *((char **)t103);
    memset(t102, 0, 8);
    t103 = (t102 + 4);
    t105 = (t104 + 4);
    t106 = *((unsigned int *)t104);
    t107 = (~(t106));
    *((unsigned int *)t102) = t107;
    *((unsigned int *)t103) = 0;
    if (*((unsigned int *)t105) != 0)
        goto LAB43;

LAB42:    t112 = *((unsigned int *)t102);
    *((unsigned int *)t102) = (t112 & 15U);
    t113 = *((unsigned int *)t103);
    *((unsigned int *)t103) = (t113 & 15U);
    t114 = ((char*)((ng15)));
    xsi_vlogtype_concat(t100, 16, 16, 3U, t114, 8, t102, 4, t101, 4);
    goto LAB34;

LAB35:    t122 = (t0 + 4640U);
    t123 = *((char **)t122);
    memset(t121, 0, 8);
    t122 = (t121 + 4);
    t124 = (t123 + 4);
    t125 = *((unsigned int *)t123);
    t126 = (t125 >> 2);
    *((unsigned int *)t121) = t126;
    t127 = *((unsigned int *)t124);
    t128 = (t127 >> 2);
    *((unsigned int *)t122) = t128;
    t129 = *((unsigned int *)t121);
    *((unsigned int *)t121) = (t129 & 3U);
    t130 = *((unsigned int *)t122);
    *((unsigned int *)t122) = (t130 & 3U);
    t131 = ((char*)((ng16)));
    memset(t132, 0, 8);
    t133 = (t121 + 4);
    t134 = (t131 + 4);
    t135 = *((unsigned int *)t121);
    t136 = *((unsigned int *)t131);
    t137 = (t135 ^ t136);
    t138 = *((unsigned int *)t133);
    t139 = *((unsigned int *)t134);
    t140 = (t138 ^ t139);
    t141 = (t137 | t140);
    t142 = *((unsigned int *)t133);
    t143 = *((unsigned int *)t134);
    t144 = (t142 | t143);
    t145 = (~(t144));
    t146 = (t141 & t145);
    if (t146 != 0)
        goto LAB47;

LAB44:    if (t144 != 0)
        goto LAB46;

LAB45:    *((unsigned int *)t132) = 1;

LAB47:    memset(t120, 0, 8);
    t148 = (t132 + 4);
    t149 = *((unsigned int *)t148);
    t150 = (~(t149));
    t151 = *((unsigned int *)t132);
    t152 = (t151 & t150);
    t153 = (t152 & 1U);
    if (t153 != 0)
        goto LAB48;

LAB49:    if (*((unsigned int *)t148) != 0)
        goto LAB50;

LAB51:    t155 = (t120 + 4);
    t156 = *((unsigned int *)t120);
    t157 = *((unsigned int *)t155);
    t158 = (t156 || t157);
    if (t158 > 0)
        goto LAB52;

LAB53:    t174 = *((unsigned int *)t120);
    t175 = (~(t174));
    t176 = *((unsigned int *)t155);
    t177 = (t175 || t176);
    if (t177 > 0)
        goto LAB54;

LAB55:    if (*((unsigned int *)t155) > 0)
        goto LAB56;

LAB57:    if (*((unsigned int *)t120) > 0)
        goto LAB58;

LAB59:    memcpy(t119, t178, 8);

LAB60:    goto LAB36;

LAB37:    xsi_vlog_unsigned_bit_combine(t60, 16, t100, 16, t119, 16);
    goto LAB41;

LAB39:    memcpy(t60, t100, 8);
    goto LAB41;

LAB43:    t108 = *((unsigned int *)t102);
    t109 = *((unsigned int *)t105);
    *((unsigned int *)t102) = (t108 | t109);
    t110 = *((unsigned int *)t103);
    t111 = *((unsigned int *)t105);
    *((unsigned int *)t103) = (t110 | t111);
    goto LAB42;

LAB46:    t147 = (t132 + 4);
    *((unsigned int *)t132) = 1;
    *((unsigned int *)t147) = 1;
    goto LAB47;

LAB48:    *((unsigned int *)t120) = 1;
    goto LAB51;

LAB50:    t154 = (t120 + 4);
    *((unsigned int *)t120) = 1;
    *((unsigned int *)t154) = 1;
    goto LAB51;

LAB52:    t160 = ((char*)((ng15)));
    t162 = (t0 + 4732U);
    t163 = *((char **)t162);
    memset(t161, 0, 8);
    t162 = (t161 + 4);
    t164 = (t163 + 4);
    t165 = *((unsigned int *)t163);
    t166 = (~(t165));
    *((unsigned int *)t161) = t166;
    *((unsigned int *)t162) = 0;
    if (*((unsigned int *)t164) != 0)
        goto LAB62;

LAB61:    t171 = *((unsigned int *)t161);
    *((unsigned int *)t161) = (t171 & 15U);
    t172 = *((unsigned int *)t162);
    *((unsigned int *)t162) = (t172 & 15U);
    t173 = ((char*)((ng14)));
    xsi_vlogtype_concat(t159, 16, 16, 3U, t173, 4, t161, 4, t160, 8);
    goto LAB53;

LAB54:    t179 = ((char*)((ng12)));
    t181 = (t0 + 4732U);
    t182 = *((char **)t181);
    memset(t180, 0, 8);
    t181 = (t180 + 4);
    t183 = (t182 + 4);
    t184 = *((unsigned int *)t182);
    t185 = (~(t184));
    *((unsigned int *)t180) = t185;
    *((unsigned int *)t181) = 0;
    if (*((unsigned int *)t183) != 0)
        goto LAB64;

LAB63:    t190 = *((unsigned int *)t180);
    *((unsigned int *)t180) = (t190 & 15U);
    t191 = *((unsigned int *)t181);
    *((unsigned int *)t181) = (t191 & 15U);
    xsi_vlogtype_concat(t178, 16, 16, 2U, t180, 4, t179, 12);
    goto LAB55;

LAB56:    xsi_vlog_unsigned_bit_combine(t119, 16, t159, 16, t178, 16);
    goto LAB60;

LAB58:    memcpy(t119, t159, 8);
    goto LAB60;

LAB62:    t167 = *((unsigned int *)t161);
    t168 = *((unsigned int *)t164);
    *((unsigned int *)t161) = (t167 | t168);
    t169 = *((unsigned int *)t162);
    t170 = *((unsigned int *)t164);
    *((unsigned int *)t162) = (t169 | t170);
    goto LAB61;

LAB64:    t186 = *((unsigned int *)t180);
    t187 = *((unsigned int *)t183);
    *((unsigned int *)t180) = (t186 | t187);
    t188 = *((unsigned int *)t181);
    t189 = *((unsigned int *)t183);
    *((unsigned int *)t181) = (t188 | t189);
    goto LAB63;

LAB65:    *((unsigned int *)t7) = 1;
    goto LAB68;

LAB67:    t4 = (t7 + 4);
    *((unsigned int *)t7) = 1;
    *((unsigned int *)t4) = 1;
    goto LAB68;

LAB69:    t16 = (t0 + 4640U);
    t17 = *((char **)t16);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t30 = (t17 + 4);
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 2);
    *((unsigned int *)t15) = t21;
    t22 = *((unsigned int *)t30);
    t23 = (t22 >> 2);
    *((unsigned int *)t16) = t23;
    t24 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t24 & 8388607U);
    t25 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t25 & 8388607U);
    t31 = ((char*)((ng1)));
    xsi_vlogtype_concat(t8, 28, 28, 2U, t31, 5, t15, 23);
    goto LAB70;

LAB71:    t37 = (t0 + 4640U);
    t38 = *((char **)t37);
    memset(t42, 0, 8);
    t37 = (t42 + 4);
    t44 = (t38 + 4);
    t32 = *((unsigned int *)t38);
    t33 = (t32 >> 2);
    *((unsigned int *)t42) = t33;
    t34 = *((unsigned int *)t44);
    t35 = (t34 >> 2);
    *((unsigned int *)t37) = t35;
    t36 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t36 & 268435455U);
    t39 = *((unsigned int *)t37);
    *((unsigned int *)t37) = (t39 & 268435455U);
    goto LAB72;

LAB73:    xsi_vlog_unsigned_bit_combine(t6, 28, t8, 28, t42, 28);
    goto LAB77;

LAB75:    memcpy(t6, t8, 8);
    goto LAB77;

LAB78:    xsi_set_current_line(155, ng11);

LAB81:    xsi_set_current_line(156, ng11);
    t16 = (t0 + 5744U);
    t17 = *((char **)t16);
    t16 = (t0 + 6064);
    t30 = (t0 + 6064);
    t31 = (t30 + 44U);
    t37 = *((char **)t31);
    t38 = (t0 + 6064);
    t44 = (t38 + 40U);
    t45 = *((char **)t44);
    t46 = (t0 + 6616);
    t55 = (t46 + 36U);
    t63 = *((char **)t55);
    memset(t8, 0, 8);
    t64 = (t8 + 4);
    t65 = (t63 + 4);
    t14 = *((unsigned int *)t63);
    t18 = (t14 >> 2);
    *((unsigned int *)t8) = t18;
    t19 = *((unsigned int *)t65);
    t20 = (t19 >> 2);
    *((unsigned int *)t64) = t20;
    t21 = *((unsigned int *)t8);
    *((unsigned int *)t8) = (t21 & 67108863U);
    t22 = *((unsigned int *)t64);
    *((unsigned int *)t64) = (t22 & 67108863U);
    xsi_vlog_generic_convert_array_indices(t6, t7, t37, t45, 2, 1, t8, 26, 2);
    t72 = (t6 + 4);
    t23 = *((unsigned int *)t72);
    t194 = (!(t23));
    t74 = (t7 + 4);
    t24 = *((unsigned int *)t74);
    t195 = (!(t24));
    t196 = (t194 && t195);
    if (t196 == 1)
        goto LAB82;

LAB83:    goto LAB80;

LAB82:    t25 = *((unsigned int *)t6);
    t26 = *((unsigned int *)t7);
    t197 = (t25 - t26);
    t198 = (t197 + 1);
    xsi_vlogvar_wait_assign_value(t16, t17, 0, *((unsigned int *)t7), t198, 0LL);
    goto LAB83;

}

static void Cont_171_5(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t41[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    char *t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;

LAB0:    t1 = (t0 + 9924U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 0);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 0);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t37 = *((unsigned int *)t4);
    t38 = (~(t37));
    t39 = *((unsigned int *)t23);
    t40 = (t38 || t39);
    if (t40 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t41, 8);

LAB16:    t53 = (t0 + 13060);
    t54 = (t53 + 32U);
    t55 = *((char **)t54);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    memset(t57, 0, 8);
    t58 = 255U;
    t59 = t58;
    t60 = (t3 + 4);
    t61 = *((unsigned int *)t3);
    t58 = (t58 & t61);
    t62 = *((unsigned int *)t60);
    t59 = (t59 & t62);
    t63 = (t57 + 4);
    t64 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t64 | t58);
    t65 = *((unsigned int *)t63);
    *((unsigned int *)t63) = (t65 | t59);
    xsi_driver_vfirst_trans(t53, 0, 7);
    t66 = (t0 + 12720);
    *((int *)t66) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 4);
    t31 = *((unsigned int *)t29);
    t32 = (t31 >> 0);
    *((unsigned int *)t27) = t32;
    t33 = *((unsigned int *)t30);
    t34 = (t33 >> 0);
    *((unsigned int *)t28) = t34;
    t35 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t35 & 255U);
    t36 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t36 & 255U);
    goto LAB9;

LAB10:    t42 = (t0 + 6524);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    memset(t41, 0, 8);
    t45 = (t41 + 4);
    t46 = (t44 + 4);
    t47 = *((unsigned int *)t44);
    t48 = (t47 >> 0);
    *((unsigned int *)t41) = t48;
    t49 = *((unsigned int *)t46);
    t50 = (t49 >> 0);
    *((unsigned int *)t45) = t50;
    t51 = *((unsigned int *)t41);
    *((unsigned int *)t41) = (t51 & 255U);
    t52 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t52 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t41, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_6(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t41[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    char *t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;

LAB0:    t1 = (t0 + 10060U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 1);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 1);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t37 = *((unsigned int *)t4);
    t38 = (~(t37));
    t39 = *((unsigned int *)t23);
    t40 = (t38 || t39);
    if (t40 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t41, 8);

LAB16:    t53 = (t0 + 13096);
    t54 = (t53 + 32U);
    t55 = *((char **)t54);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    memset(t57, 0, 8);
    t58 = 255U;
    t59 = t58;
    t60 = (t3 + 4);
    t61 = *((unsigned int *)t3);
    t58 = (t58 & t61);
    t62 = *((unsigned int *)t60);
    t59 = (t59 & t62);
    t63 = (t57 + 4);
    t64 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t64 | t58);
    t65 = *((unsigned int *)t63);
    *((unsigned int *)t63) = (t65 | t59);
    xsi_driver_vfirst_trans(t53, 8, 15);
    t66 = (t0 + 12728);
    *((int *)t66) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 4);
    t31 = *((unsigned int *)t29);
    t32 = (t31 >> 8);
    *((unsigned int *)t27) = t32;
    t33 = *((unsigned int *)t30);
    t34 = (t33 >> 8);
    *((unsigned int *)t28) = t34;
    t35 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t35 & 255U);
    t36 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t36 & 255U);
    goto LAB9;

LAB10:    t42 = (t0 + 6524);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    memset(t41, 0, 8);
    t45 = (t41 + 4);
    t46 = (t44 + 4);
    t47 = *((unsigned int *)t44);
    t48 = (t47 >> 8);
    *((unsigned int *)t41) = t48;
    t49 = *((unsigned int *)t46);
    t50 = (t49 >> 8);
    *((unsigned int *)t45) = t50;
    t51 = *((unsigned int *)t41);
    *((unsigned int *)t41) = (t51 & 255U);
    t52 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t52 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t41, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_7(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t41[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    char *t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;

LAB0:    t1 = (t0 + 10196U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 2);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 2);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t37 = *((unsigned int *)t4);
    t38 = (~(t37));
    t39 = *((unsigned int *)t23);
    t40 = (t38 || t39);
    if (t40 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t41, 8);

LAB16:    t53 = (t0 + 13132);
    t54 = (t53 + 32U);
    t55 = *((char **)t54);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    memset(t57, 0, 8);
    t58 = 255U;
    t59 = t58;
    t60 = (t3 + 4);
    t61 = *((unsigned int *)t3);
    t58 = (t58 & t61);
    t62 = *((unsigned int *)t60);
    t59 = (t59 & t62);
    t63 = (t57 + 4);
    t64 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t64 | t58);
    t65 = *((unsigned int *)t63);
    *((unsigned int *)t63) = (t65 | t59);
    xsi_driver_vfirst_trans(t53, 16, 23);
    t66 = (t0 + 12736);
    *((int *)t66) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 4);
    t31 = *((unsigned int *)t29);
    t32 = (t31 >> 16);
    *((unsigned int *)t27) = t32;
    t33 = *((unsigned int *)t30);
    t34 = (t33 >> 16);
    *((unsigned int *)t28) = t34;
    t35 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t35 & 255U);
    t36 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t36 & 255U);
    goto LAB9;

LAB10:    t42 = (t0 + 6524);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    memset(t41, 0, 8);
    t45 = (t41 + 4);
    t46 = (t44 + 4);
    t47 = *((unsigned int *)t44);
    t48 = (t47 >> 16);
    *((unsigned int *)t41) = t48;
    t49 = *((unsigned int *)t46);
    t50 = (t49 >> 16);
    *((unsigned int *)t45) = t50;
    t51 = *((unsigned int *)t41);
    *((unsigned int *)t41) = (t51 & 255U);
    t52 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t52 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t41, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_8(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t41[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    unsigned int t58;
    unsigned int t59;
    char *t60;
    unsigned int t61;
    unsigned int t62;
    char *t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;

LAB0:    t1 = (t0 + 10332U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 3);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 3);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t37 = *((unsigned int *)t4);
    t38 = (~(t37));
    t39 = *((unsigned int *)t23);
    t40 = (t38 || t39);
    if (t40 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t41, 8);

LAB16:    t53 = (t0 + 13168);
    t54 = (t53 + 32U);
    t55 = *((char **)t54);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    memset(t57, 0, 8);
    t58 = 255U;
    t59 = t58;
    t60 = (t3 + 4);
    t61 = *((unsigned int *)t3);
    t58 = (t58 & t61);
    t62 = *((unsigned int *)t60);
    t59 = (t59 & t62);
    t63 = (t57 + 4);
    t64 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t64 | t58);
    t65 = *((unsigned int *)t63);
    *((unsigned int *)t63) = (t65 | t59);
    xsi_driver_vfirst_trans(t53, 24, 31);
    t66 = (t0 + 12744);
    *((int *)t66) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 4);
    t31 = *((unsigned int *)t29);
    t32 = (t31 >> 24);
    *((unsigned int *)t27) = t32;
    t33 = *((unsigned int *)t30);
    t34 = (t33 >> 24);
    *((unsigned int *)t28) = t34;
    t35 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t35 & 255U);
    t36 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t36 & 255U);
    goto LAB9;

LAB10:    t42 = (t0 + 6524);
    t43 = (t42 + 36U);
    t44 = *((char **)t43);
    memset(t41, 0, 8);
    t45 = (t41 + 4);
    t46 = (t44 + 4);
    t47 = *((unsigned int *)t44);
    t48 = (t47 >> 24);
    *((unsigned int *)t41) = t48;
    t49 = *((unsigned int *)t46);
    t50 = (t49 >> 24);
    *((unsigned int *)t45) = t50;
    t51 = *((unsigned int *)t41);
    *((unsigned int *)t41) = (t51 & 255U);
    t52 = *((unsigned int *)t45);
    *((unsigned int *)t45) = (t52 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t41, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_9(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 10468U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 4);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 4);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13204);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 32, 39);
    t68 = (t0 + 12752);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 8);
    t31 = (t29 + 12);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 0);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 0);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 8);
    t48 = (t45 + 12);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 0);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 0);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_10(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 10604U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 5);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 5);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13240);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 40, 47);
    t68 = (t0 + 12760);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 8);
    t31 = (t29 + 12);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 8);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 8);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 8);
    t48 = (t45 + 12);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 8);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 8);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_11(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 10740U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 6);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 6);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13276);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 48, 55);
    t68 = (t0 + 12768);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 8);
    t31 = (t29 + 12);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 16);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 16);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 8);
    t48 = (t45 + 12);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 16);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 16);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_12(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 10876U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 7);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 7);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13312);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 56, 63);
    t68 = (t0 + 12776);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 8);
    t31 = (t29 + 12);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 24);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 24);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 8);
    t48 = (t45 + 12);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 24);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 24);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_13(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11012U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 8);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 8);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13348);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 64, 71);
    t68 = (t0 + 12784);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 16);
    t31 = (t29 + 20);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 0);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 0);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 16);
    t48 = (t45 + 20);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 0);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 0);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_14(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11148U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 9);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 9);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13384);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 72, 79);
    t68 = (t0 + 12792);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 16);
    t31 = (t29 + 20);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 8);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 8);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 16);
    t48 = (t45 + 20);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 8);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 8);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_15(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11284U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 10);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 10);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13420);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 80, 87);
    t68 = (t0 + 12800);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 16);
    t31 = (t29 + 20);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 16);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 16);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 16);
    t48 = (t45 + 20);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 16);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 16);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_16(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11420U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 11);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 11);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13456);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 88, 95);
    t68 = (t0 + 12808);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 16);
    t31 = (t29 + 20);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 24);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 24);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 16);
    t48 = (t45 + 20);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 24);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 24);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_17(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11556U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 12);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 12);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13492);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 96, 103);
    t68 = (t0 + 12816);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 24);
    t31 = (t29 + 28);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 0);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 0);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 24);
    t48 = (t45 + 28);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 0);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 0);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_18(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11692U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 13);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 13);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13528);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 104, 111);
    t68 = (t0 + 12824);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 24);
    t31 = (t29 + 28);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 8);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 8);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 24);
    t48 = (t45 + 28);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 8);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 8);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_19(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11828U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 14);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 14);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13564);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 112, 119);
    t68 = (t0 + 12832);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 24);
    t31 = (t29 + 28);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 16);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 16);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 24);
    t48 = (t45 + 28);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 16);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 16);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_171_20(char *t0)
{
    char t3[8];
    char t4[8];
    char t7[8];
    char t27[8];
    char t42[8];
    char *t1;
    char *t2;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t63;
    unsigned int t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    char *t68;

LAB0:    t1 = (t0 + 11964U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(171, ng11);
    t2 = (t0 + 6432);
    t5 = (t2 + 36U);
    t6 = *((char **)t5);
    memset(t7, 0, 8);
    t8 = (t7 + 4);
    t9 = (t6 + 4);
    t10 = *((unsigned int *)t6);
    t11 = (t10 >> 15);
    t12 = (t11 & 1);
    *((unsigned int *)t7) = t12;
    t13 = *((unsigned int *)t9);
    t14 = (t13 >> 15);
    t15 = (t14 & 1);
    *((unsigned int *)t8) = t15;
    memset(t4, 0, 8);
    t16 = (t7 + 4);
    t17 = *((unsigned int *)t16);
    t18 = (~(t17));
    t19 = *((unsigned int *)t7);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t16) != 0)
        goto LAB6;

LAB7:    t23 = (t4 + 4);
    t24 = *((unsigned int *)t4);
    t25 = *((unsigned int *)t23);
    t26 = (t24 || t25);
    if (t26 > 0)
        goto LAB8;

LAB9:    t38 = *((unsigned int *)t4);
    t39 = (~(t38));
    t40 = *((unsigned int *)t23);
    t41 = (t39 || t40);
    if (t41 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t42, 8);

LAB16:    t55 = (t0 + 13600);
    t56 = (t55 + 32U);
    t57 = *((char **)t56);
    t58 = (t57 + 32U);
    t59 = *((char **)t58);
    memset(t59, 0, 8);
    t60 = 255U;
    t61 = t60;
    t62 = (t3 + 4);
    t63 = *((unsigned int *)t3);
    t60 = (t60 & t63);
    t64 = *((unsigned int *)t62);
    t61 = (t61 & t64);
    t65 = (t59 + 4);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 | t60);
    t67 = *((unsigned int *)t65);
    *((unsigned int *)t65) = (t67 | t61);
    xsi_driver_vfirst_trans(t55, 120, 127);
    t68 = (t0 + 12840);
    *((int *)t68) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t22 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB7;

LAB8:    t28 = (t0 + 5652U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 24);
    t31 = (t29 + 28);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 24);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 24);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 255U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 255U);
    goto LAB9;

LAB10:    t43 = (t0 + 6524);
    t44 = (t43 + 36U);
    t45 = *((char **)t44);
    memset(t42, 0, 8);
    t46 = (t42 + 4);
    t47 = (t45 + 24);
    t48 = (t45 + 28);
    t49 = *((unsigned int *)t47);
    t50 = (t49 >> 24);
    *((unsigned int *)t42) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 24);
    *((unsigned int *)t46) = t52;
    t53 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t53 & 255U);
    t54 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t54 & 255U);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t27, 8, t42, 8);
    goto LAB16;

LAB14:    memcpy(t3, t27, 8);
    goto LAB16;

}

static void Cont_177_21(char *t0)
{
    char t5[32];
    char t12[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;

LAB0:    t1 = (t0 + 12100U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(177, ng11);
    t2 = (t0 + 6064);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = (t0 + 6064);
    t7 = (t6 + 44U);
    t8 = *((char **)t7);
    t9 = (t0 + 6064);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    t13 = (t0 + 6616);
    t14 = (t13 + 36U);
    t15 = *((char **)t14);
    memset(t12, 0, 8);
    t16 = (t12 + 4);
    t17 = (t15 + 4);
    t18 = *((unsigned int *)t15);
    t19 = (t18 >> 2);
    *((unsigned int *)t12) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 2);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t12);
    *((unsigned int *)t12) = (t22 & 67108863U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 67108863U);
    xsi_vlog_generic_get_array_select_value(t5, 128, t4, t8, t11, 2, 1, t12, 26, 2);
    t24 = (t0 + 13636);
    t25 = (t24 + 32U);
    t26 = *((char **)t25);
    t27 = (t26 + 32U);
    t28 = *((char **)t27);
    xsi_vlog_bit_copy(t28, 0, t5, 0, 128);
    xsi_driver_vfirst_trans(t24, 0, 127);
    t29 = (t0 + 12848);
    *((int *)t29) = 1;

LAB1:    return;
}

static void Always_179_22(char *t0)
{
    char t11[8];
    char t12[8];
    char t13[8];
    char t26[8];
    char t53[8];
    char t67[8];
    char t68[8];
    char t69[8];
    char t82[8];
    char t109[8];
    char t124[8];
    char t125[8];
    char t126[8];
    char t139[8];
    char t166[8];
    char t181[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    char *t25;
    char *t27;
    char *t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t41;
    char *t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    char *t48;
    char *t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t54;
    char *t55;
    char *t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    char *t70;
    char *t71;
    char *t72;
    char *t73;
    char *t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    char *t81;
    char *t83;
    char *t84;
    unsigned int t85;
    unsigned int t86;
    unsigned int t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    unsigned int t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    char *t97;
    char *t98;
    unsigned int t99;
    unsigned int t100;
    unsigned int t101;
    unsigned int t102;
    unsigned int t103;
    char *t104;
    char *t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    char *t110;
    char *t111;
    char *t112;
    char *t113;
    unsigned int t114;
    unsigned int t115;
    unsigned int t116;
    unsigned int t117;
    unsigned int t118;
    unsigned int t119;
    unsigned int t120;
    unsigned int t121;
    unsigned int t122;
    unsigned int t123;
    char *t127;
    char *t128;
    char *t129;
    char *t130;
    char *t131;
    unsigned int t132;
    unsigned int t133;
    unsigned int t134;
    unsigned int t135;
    unsigned int t136;
    unsigned int t137;
    char *t138;
    char *t140;
    char *t141;
    unsigned int t142;
    unsigned int t143;
    unsigned int t144;
    unsigned int t145;
    unsigned int t146;
    unsigned int t147;
    unsigned int t148;
    unsigned int t149;
    unsigned int t150;
    unsigned int t151;
    unsigned int t152;
    unsigned int t153;
    char *t154;
    char *t155;
    unsigned int t156;
    unsigned int t157;
    unsigned int t158;
    unsigned int t159;
    unsigned int t160;
    char *t161;
    char *t162;
    unsigned int t163;
    unsigned int t164;
    unsigned int t165;
    char *t167;
    char *t168;
    char *t169;
    char *t170;
    unsigned int t171;
    unsigned int t172;
    unsigned int t173;
    unsigned int t174;
    unsigned int t175;
    unsigned int t176;
    unsigned int t177;
    unsigned int t178;
    unsigned int t179;
    unsigned int t180;
    char *t182;
    char *t183;
    char *t184;
    char *t185;
    unsigned int t186;
    unsigned int t187;
    unsigned int t188;
    unsigned int t189;
    unsigned int t190;
    unsigned int t191;
    char *t192;

LAB0:    t1 = (t0 + 12236U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(179, ng11);
    t2 = (t0 + 12856);
    *((int *)t2) = 1;
    t3 = (t0 + 12260);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(180, ng11);

LAB5:    xsi_set_current_line(181, ng11);
    t4 = (t0 + 5560U);
    t5 = *((char **)t4);
    t4 = (t0 + 6156);
    xsi_vlogvar_wait_assign_value(t4, t5, 0, 0, 1, 0LL);
    xsi_set_current_line(182, ng11);
    t2 = (t0 + 6156);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 6248);
    xsi_vlogvar_wait_assign_value(t5, t4, 0, 0, 1, 0LL);
    xsi_set_current_line(183, ng11);
    t2 = (t0 + 6156);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (~(t6));
    t8 = *((unsigned int *)t4);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(184, ng11);

LAB9:    xsi_set_current_line(185, ng11);
    t14 = (t0 + 6616);
    t15 = (t14 + 36U);
    t16 = *((char **)t15);
    memset(t13, 0, 8);
    t17 = (t13 + 4);
    t18 = (t16 + 4);
    t19 = *((unsigned int *)t16);
    t20 = (t19 >> 0);
    *((unsigned int *)t13) = t20;
    t21 = *((unsigned int *)t18);
    t22 = (t21 >> 0);
    *((unsigned int *)t17) = t22;
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 3U);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 & 3U);
    t25 = ((char*)((ng1)));
    memset(t26, 0, 8);
    t27 = (t13 + 4);
    t28 = (t25 + 4);
    t29 = *((unsigned int *)t13);
    t30 = *((unsigned int *)t25);
    t31 = (t29 ^ t30);
    t32 = *((unsigned int *)t27);
    t33 = *((unsigned int *)t28);
    t34 = (t32 ^ t33);
    t35 = (t31 | t34);
    t36 = *((unsigned int *)t27);
    t37 = *((unsigned int *)t28);
    t38 = (t36 | t37);
    t39 = (~(t38));
    t40 = (t35 & t39);
    if (t40 != 0)
        goto LAB13;

LAB10:    if (t38 != 0)
        goto LAB12;

LAB11:    *((unsigned int *)t26) = 1;

LAB13:    memset(t12, 0, 8);
    t42 = (t26 + 4);
    t43 = *((unsigned int *)t42);
    t44 = (~(t43));
    t45 = *((unsigned int *)t26);
    t46 = (t45 & t44);
    t47 = (t46 & 1U);
    if (t47 != 0)
        goto LAB14;

LAB15:    if (*((unsigned int *)t42) != 0)
        goto LAB16;

LAB17:    t49 = (t12 + 4);
    t50 = *((unsigned int *)t12);
    t51 = *((unsigned int *)t49);
    t52 = (t50 || t51);
    if (t52 > 0)
        goto LAB18;

LAB19:    t63 = *((unsigned int *)t12);
    t64 = (~(t63));
    t65 = *((unsigned int *)t49);
    t66 = (t64 || t65);
    if (t66 > 0)
        goto LAB20;

LAB21:    if (*((unsigned int *)t49) > 0)
        goto LAB22;

LAB23:    if (*((unsigned int *)t12) > 0)
        goto LAB24;

LAB25:    memcpy(t11, t67, 8);

LAB26:    t192 = (t0 + 6708);
    xsi_vlogvar_wait_assign_value(t192, t11, 0, 0, 32, 0LL);
    goto LAB8;

LAB12:    t41 = (t26 + 4);
    *((unsigned int *)t26) = 1;
    *((unsigned int *)t41) = 1;
    goto LAB13;

LAB14:    *((unsigned int *)t12) = 1;
    goto LAB17;

LAB16:    t48 = (t12 + 4);
    *((unsigned int *)t12) = 1;
    *((unsigned int *)t48) = 1;
    goto LAB17;

LAB18:    t54 = (t0 + 5652U);
    t55 = *((char **)t54);
    memset(t53, 0, 8);
    t54 = (t53 + 4);
    t56 = (t55 + 4);
    t57 = *((unsigned int *)t55);
    t58 = (t57 >> 0);
    *((unsigned int *)t53) = t58;
    t59 = *((unsigned int *)t56);
    t60 = (t59 >> 0);
    *((unsigned int *)t54) = t60;
    t61 = *((unsigned int *)t53);
    *((unsigned int *)t53) = (t61 & 4294967295U);
    t62 = *((unsigned int *)t54);
    *((unsigned int *)t54) = (t62 & 4294967295U);
    goto LAB19;

LAB20:    t70 = (t0 + 6616);
    t71 = (t70 + 36U);
    t72 = *((char **)t71);
    memset(t69, 0, 8);
    t73 = (t69 + 4);
    t74 = (t72 + 4);
    t75 = *((unsigned int *)t72);
    t76 = (t75 >> 0);
    *((unsigned int *)t69) = t76;
    t77 = *((unsigned int *)t74);
    t78 = (t77 >> 0);
    *((unsigned int *)t73) = t78;
    t79 = *((unsigned int *)t69);
    *((unsigned int *)t69) = (t79 & 3U);
    t80 = *((unsigned int *)t73);
    *((unsigned int *)t73) = (t80 & 3U);
    t81 = ((char*)((ng13)));
    memset(t82, 0, 8);
    t83 = (t69 + 4);
    t84 = (t81 + 4);
    t85 = *((unsigned int *)t69);
    t86 = *((unsigned int *)t81);
    t87 = (t85 ^ t86);
    t88 = *((unsigned int *)t83);
    t89 = *((unsigned int *)t84);
    t90 = (t88 ^ t89);
    t91 = (t87 | t90);
    t92 = *((unsigned int *)t83);
    t93 = *((unsigned int *)t84);
    t94 = (t92 | t93);
    t95 = (~(t94));
    t96 = (t91 & t95);
    if (t96 != 0)
        goto LAB30;

LAB27:    if (t94 != 0)
        goto LAB29;

LAB28:    *((unsigned int *)t82) = 1;

LAB30:    memset(t68, 0, 8);
    t98 = (t82 + 4);
    t99 = *((unsigned int *)t98);
    t100 = (~(t99));
    t101 = *((unsigned int *)t82);
    t102 = (t101 & t100);
    t103 = (t102 & 1U);
    if (t103 != 0)
        goto LAB31;

LAB32:    if (*((unsigned int *)t98) != 0)
        goto LAB33;

LAB34:    t105 = (t68 + 4);
    t106 = *((unsigned int *)t68);
    t107 = *((unsigned int *)t105);
    t108 = (t106 || t107);
    if (t108 > 0)
        goto LAB35;

LAB36:    t120 = *((unsigned int *)t68);
    t121 = (~(t120));
    t122 = *((unsigned int *)t105);
    t123 = (t121 || t122);
    if (t123 > 0)
        goto LAB37;

LAB38:    if (*((unsigned int *)t105) > 0)
        goto LAB39;

LAB40:    if (*((unsigned int *)t68) > 0)
        goto LAB41;

LAB42:    memcpy(t67, t124, 8);

LAB43:    goto LAB21;

LAB22:    xsi_vlog_unsigned_bit_combine(t11, 32, t53, 32, t67, 32);
    goto LAB26;

LAB24:    memcpy(t11, t53, 8);
    goto LAB26;

LAB29:    t97 = (t82 + 4);
    *((unsigned int *)t82) = 1;
    *((unsigned int *)t97) = 1;
    goto LAB30;

LAB31:    *((unsigned int *)t68) = 1;
    goto LAB34;

LAB33:    t104 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t104) = 1;
    goto LAB34;

LAB35:    t110 = (t0 + 5652U);
    t111 = *((char **)t110);
    memset(t109, 0, 8);
    t110 = (t109 + 4);
    t112 = (t111 + 8);
    t113 = (t111 + 12);
    t114 = *((unsigned int *)t112);
    t115 = (t114 >> 0);
    *((unsigned int *)t109) = t115;
    t116 = *((unsigned int *)t113);
    t117 = (t116 >> 0);
    *((unsigned int *)t110) = t117;
    t118 = *((unsigned int *)t109);
    *((unsigned int *)t109) = (t118 & 4294967295U);
    t119 = *((unsigned int *)t110);
    *((unsigned int *)t110) = (t119 & 4294967295U);
    goto LAB36;

LAB37:    t127 = (t0 + 6616);
    t128 = (t127 + 36U);
    t129 = *((char **)t128);
    memset(t126, 0, 8);
    t130 = (t126 + 4);
    t131 = (t129 + 4);
    t132 = *((unsigned int *)t129);
    t133 = (t132 >> 0);
    *((unsigned int *)t126) = t133;
    t134 = *((unsigned int *)t131);
    t135 = (t134 >> 0);
    *((unsigned int *)t130) = t135;
    t136 = *((unsigned int *)t126);
    *((unsigned int *)t126) = (t136 & 3U);
    t137 = *((unsigned int *)t130);
    *((unsigned int *)t130) = (t137 & 3U);
    t138 = ((char*)((ng16)));
    memset(t139, 0, 8);
    t140 = (t126 + 4);
    t141 = (t138 + 4);
    t142 = *((unsigned int *)t126);
    t143 = *((unsigned int *)t138);
    t144 = (t142 ^ t143);
    t145 = *((unsigned int *)t140);
    t146 = *((unsigned int *)t141);
    t147 = (t145 ^ t146);
    t148 = (t144 | t147);
    t149 = *((unsigned int *)t140);
    t150 = *((unsigned int *)t141);
    t151 = (t149 | t150);
    t152 = (~(t151));
    t153 = (t148 & t152);
    if (t153 != 0)
        goto LAB47;

LAB44:    if (t151 != 0)
        goto LAB46;

LAB45:    *((unsigned int *)t139) = 1;

LAB47:    memset(t125, 0, 8);
    t155 = (t139 + 4);
    t156 = *((unsigned int *)t155);
    t157 = (~(t156));
    t158 = *((unsigned int *)t139);
    t159 = (t158 & t157);
    t160 = (t159 & 1U);
    if (t160 != 0)
        goto LAB48;

LAB49:    if (*((unsigned int *)t155) != 0)
        goto LAB50;

LAB51:    t162 = (t125 + 4);
    t163 = *((unsigned int *)t125);
    t164 = *((unsigned int *)t162);
    t165 = (t163 || t164);
    if (t165 > 0)
        goto LAB52;

LAB53:    t177 = *((unsigned int *)t125);
    t178 = (~(t177));
    t179 = *((unsigned int *)t162);
    t180 = (t178 || t179);
    if (t180 > 0)
        goto LAB54;

LAB55:    if (*((unsigned int *)t162) > 0)
        goto LAB56;

LAB57:    if (*((unsigned int *)t125) > 0)
        goto LAB58;

LAB59:    memcpy(t124, t181, 8);

LAB60:    goto LAB38;

LAB39:    xsi_vlog_unsigned_bit_combine(t67, 32, t109, 32, t124, 32);
    goto LAB43;

LAB41:    memcpy(t67, t109, 8);
    goto LAB43;

LAB46:    t154 = (t139 + 4);
    *((unsigned int *)t139) = 1;
    *((unsigned int *)t154) = 1;
    goto LAB47;

LAB48:    *((unsigned int *)t125) = 1;
    goto LAB51;

LAB50:    t161 = (t125 + 4);
    *((unsigned int *)t125) = 1;
    *((unsigned int *)t161) = 1;
    goto LAB51;

LAB52:    t167 = (t0 + 5652U);
    t168 = *((char **)t167);
    memset(t166, 0, 8);
    t167 = (t166 + 4);
    t169 = (t168 + 16);
    t170 = (t168 + 20);
    t171 = *((unsigned int *)t169);
    t172 = (t171 >> 0);
    *((unsigned int *)t166) = t172;
    t173 = *((unsigned int *)t170);
    t174 = (t173 >> 0);
    *((unsigned int *)t167) = t174;
    t175 = *((unsigned int *)t166);
    *((unsigned int *)t166) = (t175 & 4294967295U);
    t176 = *((unsigned int *)t167);
    *((unsigned int *)t167) = (t176 & 4294967295U);
    goto LAB53;

LAB54:    t182 = (t0 + 5652U);
    t183 = *((char **)t182);
    memset(t181, 0, 8);
    t182 = (t181 + 4);
    t184 = (t183 + 24);
    t185 = (t183 + 28);
    t186 = *((unsigned int *)t184);
    t187 = (t186 >> 0);
    *((unsigned int *)t181) = t187;
    t188 = *((unsigned int *)t185);
    t189 = (t188 >> 0);
    *((unsigned int *)t182) = t189;
    t190 = *((unsigned int *)t181);
    *((unsigned int *)t181) = (t190 & 4294967295U);
    t191 = *((unsigned int *)t182);
    *((unsigned int *)t182) = (t191 & 4294967295U);
    goto LAB55;

LAB56:    xsi_vlog_unsigned_bit_combine(t124, 32, t166, 32, t181, 32);
    goto LAB60;

LAB58:    memcpy(t124, t166, 8);
    goto LAB60;

}

static void Cont_191_23(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    t1 = (t0 + 12372U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(191, ng11);
    t2 = (t0 + 6708);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 13672);
    t6 = (t5 + 32U);
    t7 = *((char **)t6);
    t8 = (t7 + 32U);
    t9 = *((char **)t8);
    memcpy(t9, t4, 8);
    xsi_driver_vfirst_trans(t5, 0, 31);
    t10 = (t0 + 12864);
    *((int *)t10) = 1;

LAB1:    return;
}

static void Cont_192_24(char *t0)
{
    char t4[8];
    char t17[8];
    char t32[8];
    char t40[8];
    char t68[8];
    char t76[8];
    char *t1;
    char *t2;
    char *t3;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    char *t16;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    char *t30;
    char *t31;
    char *t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    char *t39;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    char *t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    char *t54;
    char *t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    int t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    unsigned int t67;
    char *t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    char *t75;
    unsigned int t77;
    unsigned int t78;
    unsigned int t79;
    char *t80;
    char *t81;
    char *t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    unsigned int t86;
    unsigned int t87;
    unsigned int t88;
    unsigned int t89;
    char *t90;
    char *t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    int t100;
    int t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    unsigned int t105;
    unsigned int t106;
    unsigned int t107;
    char *t108;
    char *t109;
    char *t110;
    char *t111;
    char *t112;
    unsigned int t113;
    unsigned int t114;
    char *t115;
    unsigned int t116;
    unsigned int t117;
    char *t118;
    unsigned int t119;
    unsigned int t120;
    char *t121;

LAB0:    t1 = (t0 + 12508U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(192, ng11);
    t2 = (t0 + 5192U);
    t3 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t3 + 4);
    t5 = *((unsigned int *)t2);
    t6 = (~(t5));
    t7 = *((unsigned int *)t3);
    t8 = (t7 & t6);
    t9 = (t8 & 1U);
    if (t9 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t11 = (t4 + 4);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t11);
    t14 = (t12 || t13);
    if (t14 > 0)
        goto LAB8;

LAB9:    memcpy(t76, t4, 8);

LAB10:    t108 = (t0 + 13708);
    t109 = (t108 + 32U);
    t110 = *((char **)t109);
    t111 = (t110 + 32U);
    t112 = *((char **)t111);
    memset(t112, 0, 8);
    t113 = 1U;
    t114 = t113;
    t115 = (t76 + 4);
    t116 = *((unsigned int *)t76);
    t113 = (t113 & t116);
    t117 = *((unsigned int *)t115);
    t114 = (t114 & t117);
    t118 = (t112 + 4);
    t119 = *((unsigned int *)t112);
    *((unsigned int *)t112) = (t119 | t113);
    t120 = *((unsigned int *)t118);
    *((unsigned int *)t118) = (t120 | t114);
    xsi_driver_vfirst_trans(t108, 0, 0);
    t121 = (t0 + 12872);
    *((int *)t121) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t10 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB7;

LAB8:    t15 = (t0 + 5468U);
    t16 = *((char **)t15);
    memset(t17, 0, 8);
    t15 = (t16 + 4);
    t18 = *((unsigned int *)t15);
    t19 = (~(t18));
    t20 = *((unsigned int *)t16);
    t21 = (t20 & t19);
    t22 = (t21 & 1U);
    if (t22 != 0)
        goto LAB11;

LAB12:    if (*((unsigned int *)t15) != 0)
        goto LAB13;

LAB14:    t24 = (t17 + 4);
    t25 = *((unsigned int *)t17);
    t26 = (!(t25));
    t27 = *((unsigned int *)t24);
    t28 = (t26 || t27);
    if (t28 > 0)
        goto LAB15;

LAB16:    memcpy(t40, t17, 8);

LAB17:    memset(t68, 0, 8);
    t69 = (t40 + 4);
    t70 = *((unsigned int *)t69);
    t71 = (~(t70));
    t72 = *((unsigned int *)t40);
    t73 = (t72 & t71);
    t74 = (t73 & 1U);
    if (t74 != 0)
        goto LAB25;

LAB26:    if (*((unsigned int *)t69) != 0)
        goto LAB27;

LAB28:    t77 = *((unsigned int *)t4);
    t78 = *((unsigned int *)t68);
    t79 = (t77 & t78);
    *((unsigned int *)t76) = t79;
    t80 = (t4 + 4);
    t81 = (t68 + 4);
    t82 = (t76 + 4);
    t83 = *((unsigned int *)t80);
    t84 = *((unsigned int *)t81);
    t85 = (t83 | t84);
    *((unsigned int *)t82) = t85;
    t86 = *((unsigned int *)t82);
    t87 = (t86 != 0);
    if (t87 == 1)
        goto LAB29;

LAB30:
LAB31:    goto LAB10;

LAB11:    *((unsigned int *)t17) = 1;
    goto LAB14;

LAB13:    t23 = (t17 + 4);
    *((unsigned int *)t17) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB14;

LAB15:    t29 = (t0 + 6248);
    t30 = (t29 + 36U);
    t31 = *((char **)t30);
    memset(t32, 0, 8);
    t33 = (t31 + 4);
    t34 = *((unsigned int *)t33);
    t35 = (~(t34));
    t36 = *((unsigned int *)t31);
    t37 = (t36 & t35);
    t38 = (t37 & 1U);
    if (t38 != 0)
        goto LAB18;

LAB19:    if (*((unsigned int *)t33) != 0)
        goto LAB20;

LAB21:    t41 = *((unsigned int *)t17);
    t42 = *((unsigned int *)t32);
    t43 = (t41 | t42);
    *((unsigned int *)t40) = t43;
    t44 = (t17 + 4);
    t45 = (t32 + 4);
    t46 = (t40 + 4);
    t47 = *((unsigned int *)t44);
    t48 = *((unsigned int *)t45);
    t49 = (t47 | t48);
    *((unsigned int *)t46) = t49;
    t50 = *((unsigned int *)t46);
    t51 = (t50 != 0);
    if (t51 == 1)
        goto LAB22;

LAB23:
LAB24:    goto LAB17;

LAB18:    *((unsigned int *)t32) = 1;
    goto LAB21;

LAB20:    t39 = (t32 + 4);
    *((unsigned int *)t32) = 1;
    *((unsigned int *)t39) = 1;
    goto LAB21;

LAB22:    t52 = *((unsigned int *)t40);
    t53 = *((unsigned int *)t46);
    *((unsigned int *)t40) = (t52 | t53);
    t54 = (t17 + 4);
    t55 = (t32 + 4);
    t56 = *((unsigned int *)t54);
    t57 = (~(t56));
    t58 = *((unsigned int *)t17);
    t59 = (t58 & t57);
    t60 = *((unsigned int *)t55);
    t61 = (~(t60));
    t62 = *((unsigned int *)t32);
    t63 = (t62 & t61);
    t64 = (~(t59));
    t65 = (~(t63));
    t66 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t66 & t64);
    t67 = *((unsigned int *)t46);
    *((unsigned int *)t46) = (t67 & t65);
    goto LAB24;

LAB25:    *((unsigned int *)t68) = 1;
    goto LAB28;

LAB27:    t75 = (t68 + 4);
    *((unsigned int *)t68) = 1;
    *((unsigned int *)t75) = 1;
    goto LAB28;

LAB29:    t88 = *((unsigned int *)t76);
    t89 = *((unsigned int *)t82);
    *((unsigned int *)t76) = (t88 | t89);
    t90 = (t4 + 4);
    t91 = (t68 + 4);
    t92 = *((unsigned int *)t4);
    t93 = (~(t92));
    t94 = *((unsigned int *)t90);
    t95 = (~(t94));
    t96 = *((unsigned int *)t68);
    t97 = (~(t96));
    t98 = *((unsigned int *)t91);
    t99 = (~(t98));
    t100 = (t93 & t95);
    t101 = (t97 & t99);
    t102 = (~(t100));
    t103 = (~(t101));
    t104 = *((unsigned int *)t82);
    *((unsigned int *)t82) = (t104 & t102);
    t105 = *((unsigned int *)t82);
    *((unsigned int *)t82) = (t105 & t103);
    t106 = *((unsigned int *)t76);
    *((unsigned int *)t76) = (t106 & t102);
    t107 = *((unsigned int *)t76);
    *((unsigned int *)t76) = (t107 & t103);
    goto LAB31;

}


extern void work_m_00000000000176915359_3847564725_init()
{
	static char *pe[] = {(void *)Cont_83_0,(void *)Cont_84_1,(void *)Cont_85_2,(void *)Cont_87_3,(void *)Always_142_4,(void *)Cont_171_5,(void *)Cont_171_6,(void *)Cont_171_7,(void *)Cont_171_8,(void *)Cont_171_9,(void *)Cont_171_10,(void *)Cont_171_11,(void *)Cont_171_12,(void *)Cont_171_13,(void *)Cont_171_14,(void *)Cont_171_15,(void *)Cont_171_16,(void *)Cont_171_17,(void *)Cont_171_18,(void *)Cont_171_19,(void *)Cont_171_20,(void *)Cont_177_21,(void *)Always_179_22,(void *)Cont_191_23,(void *)Cont_192_24};
	static char *se[] = {(void *)sp_in_loboot_mem,(void *)sp_in_hiboot_mem,(void *)sp_in_boot_mem,(void *)sp_in_main_mem,(void *)sp_in_uart0,(void *)sp_in_uart1,(void *)sp_in_ic,(void *)sp_in_tm,(void *)sp_in_test,(void *)sp_in_ethmac,(void *)sp_in_cachable_mem};
	xsi_register_didat("work_m_00000000000176915359_3847564725", "isim/amber-test.exe.sim/work/m_00000000000176915359_3847564725.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
