module a23_delay_buffer

(
input		[31:0]		i_wb_adr,
input		[3:0]		i_wb_sel,
input					i_wb_we,
output		[31:0]		o_wb_dat_w,			//_w : wishbone to cache
input		[31:0]		i_wb_dat_c,			//_c : cache to wishbone
input					i_wb_cyc,
input					i_wb_stb,
output					o_wb_ack,
output					o_wb_err,

output		[31:0]		o_wb_adr,
output		[3:0]		o_wb_sel,
output					o_wb_we,
input		[31:0]		i_wb_dat_w,
output		[31:0]		o_wb_dat_c,
output					o_wb_cyc,
output					o_wb_stb,
input					i_wb_ack,
input					i_wb_err

);

/*
reg [31:0] 				adr;
reg [3:0]				sel;
reg						we;
reg [31:0]				w_c_dat;		//wishbone to core
reg [31:0]				c_w_dat;		//core to wishbone
reg 						cyc;
reg						stb;
reg						ack;
reg						err;


always @ (posedge i_clk) begin
*/

assign o_wb_adr	= i_wb_adr;
assign o_wb_sel	= i_wb_sel;
assign o_wb_we	= i_wb_we;
assign o_wb_dat_w = i_wb_dat_w;
assign o_wb_dat_c = i_wb_dat_c;
assign o_wb_cyc = i_wb_cyc;
assign o_wb_stb = i_wb_stb;
assign o_wb_ack = i_wb_ack;
assign o_wb_err = i_wb_err;
	
endmodule